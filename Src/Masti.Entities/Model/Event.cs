﻿using System;

namespace Masti.Entities.Model
{
    public class Event : BaseEntity
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string Description { get; set; }
        
        public string MainMessage { get; set; }

        public string HomePageURL { get; set; }

        public DateTime EventDate { get; set; }
        
        public DateTime RegistrationStartDate { get; set; }
        
        public DateTime RegistrationEndDate { get; set; }
        
        public string VenueName { get; set; }
        
        public string VenueAddress { get; set; }
        
        public string EventTypeCode { get; set; }
        
        public bool EarlyBirdOn { get; set; }
        
        public virtual EventType EventType { get; set; }

        public bool IsMastiSpellEvent() { return EventType.IsMastiSpellEvent(); }

        public bool IsMastiFestEvent() { return EventType.IsMastiFestEvent(); }

    }
}
