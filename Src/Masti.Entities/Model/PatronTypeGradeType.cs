﻿namespace Masti.Entities.Model
{
    public class PatronTypeGradeType : BaseEntity
    {
        public int Id { get; set; }
        public string PatronTypeCode { get; set; }
        public string GradeTypeCode { get; set; }
        public int DisplayOrder { get; set; }

        public virtual PatronType PatronType { get; set; }
        public virtual GradeType GradeType { get; set; }
    
    }
}
