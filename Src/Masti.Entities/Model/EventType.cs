﻿namespace Masti.Entities.Model
{
    public class EventType
    {
        public string EventTypeCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsMastiSpellEvent() { return EventTypeCode == "MASTISPELL";  }
        public bool IsMastiFestEvent() { return EventTypeCode == "MASTIFEST"; }
        //public IList<PatronType> PatronTypes;
    }
}
