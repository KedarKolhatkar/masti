﻿namespace Masti.Entities.Model
{
    public class County : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual State State { get; set; }
    }
}
