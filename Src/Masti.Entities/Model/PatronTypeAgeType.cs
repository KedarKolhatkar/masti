﻿namespace Masti.Entities.Model
{
    public class PatronTypeAgeType : BaseEntity
    {
        public int Id { get; set; }
        public string PatronTypeCode { get; set; }
        public string AgeTypeCode { get; set; }
        public int DisplayOrder { get; set; }

        public virtual PatronType PatronType { get; set; }
        public virtual AgeType AgeType { get; set; }
    
    }
}
