﻿namespace Masti.Entities.Model
{
    public class School : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual County County { get; set; }
    }
}
