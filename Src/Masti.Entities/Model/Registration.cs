﻿using System;
using System.Collections.Generic;

namespace Masti.Entities.Model
{
    public class Registration : BaseEntity
    {
        public int Id { get; set; }

        public Guid InvoiceNumber { get; set; }

        public decimal Amount { get; set; }
        
        public int MastiPaymentId { get; set; }

        public string PmtVendorOrderId { get; set; }
        
        public string PmtVendorCode { get; set; }

        public DateTime PaymentInitiationDate { get; set; }

        public DateTime? PaymentCompleteDate { get; set; }

        public DateTime? PaymentCancelledDate { get; set; }

        public DateTime RegisteredDate { get; set; }
        
        public DateTime? RefundedDate { get; set; }
        
        public int EventId { get; set; }
        
        public string StatusCode { get; set; }
        
        public bool AttendingSpellingBootcamp { get; set; }
        
        public string ReferredBy { get; set; }

        public virtual RegistrationStatus Status { get; set; }
        
        public virtual Event Event { get; set; }
        
        public IList<Patron> Patrons { get; set; }
    }
}
