namespace Masti.Entities.Model
{
    public class EventTypePatronType : BaseEntity
    {
        public int Id { get; set; }
        public string EventTypeCode { get; set; }
        public string PatronTypeCode { get; set; }
        public int DisplayOrder { get; set; }

        public virtual EventType EventType { get; set; }
        public virtual PatronType PatronType { get; set; }
    }
}
