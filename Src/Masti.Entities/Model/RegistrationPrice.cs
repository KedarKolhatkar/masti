﻿using System;

namespace Masti.Entities.Model
{
    public class RegistrationPrice : BaseEntity
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public decimal DiscountedAmount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int EventId { get; set; }
        public virtual Event Event { get; set; }
        public virtual PatronType PatronType { get; set; }
    }
}
