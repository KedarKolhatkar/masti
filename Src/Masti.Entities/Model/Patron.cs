﻿namespace Masti.Entities.Model
{
    public abstract class Patron : BaseEntity
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public decimal Amount { get; set; }
        public int RegistrationId { get; set; }
        public string AgeTypeCode { get; set; }

        public virtual Registration Registration { get; set; }
        public virtual AgeType Age { get; set; }
        public int Id { get; set; }
    }

    public abstract class Speller : Patron
    {
        public string GradeTypeCode { get; set; }
        public int? SchoolId { get; set; }
        public string OtherSchool { get; set; }

        public virtual GradeType Grade { get; set; }
        public virtual School School { get; set; }
    }

    public class JrBeeSpeller : Speller
    {

    }

    public class OlympiadSpeller : Speller
    {
        
    }

    public class Parent : Patron
    {
        
    }

    public class Infant : Patron
    {
        
    }

    public class KidSpectator : Patron
    {
        
    }

    public class OtherSpectator : Patron
    {
        
    }

    public class Adult : Patron
    {

    }

    public class Youth : Patron
    {

    }

    public class Child : Patron
    {

    }


}
