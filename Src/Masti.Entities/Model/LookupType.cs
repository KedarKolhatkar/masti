﻿namespace Masti.Entities.Model
{
    public abstract class LookupType
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public int DisplayOrder { get; set; }
    }

    public class GradeType : LookupType { }
    public class PatronType : LookupType { }
    public class AgeType : LookupType {
        public static string AGE_18_PLUS = "AGE_18_PLUS";
        public static string AGE_17 = "AGE_17";
        public static string AGE_4 = "AGE_4";
        public static string AGE_0_2 = "AGE_0_2";
    }

    public class RegistrationStatus : LookupType
    {
        public static string Initiated = "REGINIT";
        public static string Completed = "REGCOMPLETE";
        public static string Cancelled = "REGCANCELLED";
        public static string Refunded = "REGREUNDED";
    }
}

