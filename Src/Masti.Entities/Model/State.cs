﻿namespace Masti.Entities.Model
{
    public class State : BaseEntity
    {
        public string StateCode { get; set;}
        public string Name { get; set; }
    }
}
