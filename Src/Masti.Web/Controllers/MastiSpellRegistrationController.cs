﻿using System.Web.Mvc;
using Masti.Services;
using Masti.Web.Infrasturcture;
using Masti.Web.Models;

namespace Masti.Web.Controllers
{
    public class MastiSpellRegistrationController : MastiBaseController
    {
        private readonly ILookupService _lookupService;

        public MastiSpellRegistrationController(ILookupService lookupService, 
            IEventService eventService, IApplicationSettings appSettings, IApplicationCache applicationCache)
            : base(eventService, appSettings, applicationCache)
        {
            _lookupService = lookupService;
        }

        [HttpPost]
        public ActionResult TestPayment()
        {
            return Json(new { result = "success", returnData = "Return Data" });
        }

        // GET: /Registration/
        public ActionResult Index()
        {
            ViewBag.GetParticipantReferenceDataUrl = Url.Action("GetParticipantReferenceData");
            ViewBag.GetCountiesUrl = Url.Action("GetCounties");
            ViewBag.GetSchoolsUrl = Url.Action("GetSchools");
            ViewBag.InitiatePaymentUrl = Url.Action("InitiatePayment", "Payment");
            ViewBag.GetCurrentRegistrationUrl = Url.Action("Current");
            ViewBag.TestPaymentUrl = Url.Action("TestPayment");

            return View(GetEventAndRegistrationPriceModel());
        }

        // GET: /Registration/GetParticipantReferenceData
        [HttpGet]
        [OutputCache(CacheProfile = "LongProfile")]
        public ActionResult GetParticipantReferenceData()
        {
            var data = new ParticipantRefData
            {
                PatronTypes = GetPatronTypes(),
                States = _lookupService.GetStates(),
                RegistrationPriceDictionary = GetCurrentRegistrationPrices(),
                GradeTypeDictionary = _lookupService.GetPatronTypeGradeTypeDictionary(),
                AgeTypeDictionary = _lookupService.GetPatronTypeAgeTypeDictionary()
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: /Registration/GetCounties/{StateCode}
        [HttpGet]
        //[OutputCache(CacheProfile = "LongProfile")]
        public ActionResult GetCounties(string stateCode)
        {
            var counties = _lookupService.GetCounties(stateCode);
            return Json(counties, JsonRequestBehavior.AllowGet);
        }

        // GET: /Registration/GetSchools/{CountId}
        [HttpGet]
        //[OutputCache(CacheProfile = "LongProfile")]
        public ActionResult GetSchools(int? countyId)
        {
            var schools = _lookupService.GetSchools(countyId);
            return Json(schools, JsonRequestBehavior.AllowGet);
        }

        // POST: /Registration/Register
        //[HttpPost]
        //public ActionResult Register(Registration registration)
        //{
        //    registration.EventId = AppSettings.EventId;
        //    _registrationService.Save(registration);
        //    return Json(new {result = "success"});
        //}

        [HttpGet]
        // GET: /Registration/Current
        public ActionResult Current()
        {
            return Json(new { Result = "success", Registration = Session["registration"] }, JsonRequestBehavior.AllowGet);
        }

        #region Private Methods

        private EventAndRegistrationPriceModel GetEventAndRegistrationPriceModel()
        {
            var @event = GetCurrentEvent();
            var registrationPricesPerPatronType = GetRegistrationPrices();

            return new EventAndRegistrationPriceModel
            {
                Event = @event,
                RegististationPriceDictionary = registrationPricesPerPatronType
            };
        }

        #endregion
    }

}