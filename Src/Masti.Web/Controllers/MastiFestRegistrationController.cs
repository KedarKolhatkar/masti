﻿using Masti.Web.Models;
using System.Web.Mvc;
using Masti.Services;
using Masti.Web.Infrasturcture;

namespace Masti.Web.Controllers
{
    public class MastiFestRegistrationController : MastiBaseController
    {
        public MastiFestRegistrationController(IEventService eventService, IApplicationSettings appSettings,
            IApplicationCache applicationCache)
            : base(eventService, appSettings, applicationCache)
        {
        }

        // GET: MastiFest
        public ActionResult Index()
        {
            ViewBag.GetParticipantReferenceDataUrl = Url.Action("GetParticipantReferenceData");
            ViewBag.InitiatePaymentUrl = Url.Action("InitiatePayment", "Payment");
            //ViewBag.GetCurrentRegistrationUrl = Url.Action("Current");
            ViewBag.TestPaymentUrl = Url.Action("TestPayment");

            return View(GetMastiFestRegistrationModel());
        }

        // GET: /Registration/GetParticipantReferenceData
        [HttpGet]
        public ActionResult GetParticipantReferenceData()
        {
            var data = new ParticipantRefData
            {
                PatronTypes = GetPatronTypes(),
                RegistrationPriceDictionary = GetCurrentRegistrationPrices()
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public MastiFestRegistrationModel GetMastiFestRegistrationModel()
        {
            return new MastiFestRegistrationModel
            {
                Event = GetCurrentEvent(),
                RegististationPriceDictionary = GetRegistrationPrices()
            };
        }
    }

}