﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Masti.DTO;
using Masti.Services;
using Masti.Web.Infrasturcture;
using Masti.Web.Models;
using Masti.Web.Payment;
using PayPal.Api;
using NLog;

namespace Masti.Web.Controllers
{
    public class PaymentController : MastiBaseController
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private const string PAYMENT_ID = "paymentId";
        //private const string REGISTRATION = "registration";
        //private const string PAYPAL_PAYMENT_ID = "payPalPaymentId";
        private const string PAYER_ID = "PayerID";

        private readonly IRegistrationService _registrationService;
        private readonly IPaypalPaymentAdaptor _paypalPaymentAdaptor;
        private readonly IPaymentIdGenerator _paymentIdGenerator;

        public PaymentController(IRegistrationService registrationService, IEventService eventService, 
            IPaypalPaymentAdaptor paypalPaymentAdaptor, IPaymentIdGenerator paymentIdGenerator, 
            IApplicationSettings appSettings, IApplicationCache applicationCache) 
            : base(eventService, appSettings, applicationCache)
        {
            _registrationService = registrationService;
            _paypalPaymentAdaptor = paypalPaymentAdaptor;
            _paymentIdGenerator = paymentIdGenerator;
        }

        // POST: /Payment/InitiatePayment
        [HttpPost]
        public ActionResult InitiatePayment(RegistrationDTO registration)
        {
            Entities.Model.Event currentEvent = GetCurrentEvent();
            registration.EventId = currentEvent.Id;
            registration.EventName = currentEvent.Name;
            registration.EventTypeCode = currentEvent.EventTypeCode;
            registration.InvoiceNumber = Guid.NewGuid();
            registration.MastiPaymentId = _paymentIdGenerator.Generate();

            var createdPayment = _paypalPaymentAdaptor.CreatePayment(registration, 
                GetPaymentExecutionUrl(registration.MastiPaymentId),
                GetCancelPaymentUrl(registration.MastiPaymentId));

            registration.PmtVendorOrderId = createdPayment.id;
            _registrationService.SaveOnPaymentInitiation(registration);

            //Session[PAYMENT_ID] = registration.MastiPaymentId;

            return Json(new {result = "success", approvalUrl = _paypalPaymentAdaptor.GetApprovalUrl()});
        }

        public ActionResult Complete(int paymentId)
        {
            var registration = _registrationService.GetByMastiPaymentId(paymentId);
            if (registration == null) throw new ApplicationException(
                string.Format("Registration with mastiPaymentId={0} not found", paymentId));

            var paymentExecution = new PaymentExecution() { payer_id = Request[PAYER_ID] };
            var payment = new PayPal.Api.Payment() { id = registration.PmtVendorOrderId };
            try
            {
                var executedPayment = payment.Execute(PaypalConfiguration.GetAPIContext(), paymentExecution);
                _registrationService.SaveOnPaymentCompletion(registration.Id, executedPayment.id);
            }
            catch (PayPal.HttpException he)
            {
                _logger.Error(he,
                    string.Format("Error executing payment. payer_id={0} PmtVendorOrderId={1} Response:{2}",
                    Request[PAYER_ID], registration.PmtVendorOrderId, he.Response));
                return View("Error", he);
            }
            catch (Exception e)
            {
                _logger.Error(e, 
                    string.Format("Error executing/saving payment. payer_id={0} PmtVendorOrderId={1}", 
                    Request[PAYER_ID], registration.PmtVendorOrderId));
                return View("Error", e);
            }

            return View("Confirmation", new BaseRegistrationModel() {Event = GetCurrentEvent()});
        }

        public ActionResult Confirmation()
        {
            return View("Confirmation");
        }

        public ActionResult Cancel(int paymentId)
        {
            var registration = _registrationService.GetByMastiPaymentId(paymentId);
            if (registration == null) throw new ApplicationException(
                string.Format("Registration with mastiPaymentId={0} not found", paymentId));

            _registrationService.SaveOnPaymentCancellation(registration.Id);

            return RedirectToAction("Index", "Registration");
            //return View("Confirmation", new BaseRegistrationModel() { Event = GetCurrentEvent() });
        }

        #region Private Method
        private string GetCancelPaymentUrl(int paymentId)
        {
            return Url.Action("Cancel", "Payment", new RouteValueDictionary() { { PAYMENT_ID, paymentId } }, 
                Request.Url.Scheme, Request.Url.Host);
        }

        private string GetPaymentExecutionUrl(int paymentId)
        {
            return Url.Action("Complete", "Payment", new RouteValueDictionary() { { PAYMENT_ID, paymentId } },
                Request.Url.Scheme, Request.Url.Host);
        }
        #endregion
    }
}