﻿using System;
using System.IO;
using System.Text;
using System.Web.Mvc;
using Masti.Entities.Model;
using Masti.Services;
using Masti.Web.Infrasturcture;
using Newtonsoft.Json;
using System.Web;
using Masti.Web.Models;
using System.Collections.Generic;

namespace Masti.Web.Controllers
{
    public abstract class MastiBaseController : Controller
    {
        public static string CACHE_KEY_EVENT = "CURRENT_EVENT";
        public static string CACHE_REGISTRATION_PRICES = "REGISTRATION_PRICES";
        public static string CACHE_CURRENT_REGISTRATION_PRICES = "CURRENT_REGISTRATION_PRICES";
        public static string CACHE_PATRON_TYPES = "PATRON_TYPES";

        protected readonly IApplicationSettings AppSettings;
        protected readonly IEventService EventService;
        protected readonly IApplicationCache ApplicationCache;

        private int minutesToCache;

        protected MastiBaseController(IEventService eventService, 
            IApplicationSettings appSettings, IApplicationCache applicationCache)
        {
            EventService = eventService;
            AppSettings = appSettings;
            ApplicationCache = applicationCache;
            minutesToCache = AppSettings.MinutesToCacheAppObjects;
        }

        protected override JsonResult Json(object data,
            string contentType,
            Encoding contentEncoding,
            JsonRequestBehavior behavior)
        {
            return new JsonNetResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }

        protected Event GetCurrentEvent()
        {
            Event e = ApplicationCache.Get(CACHE_KEY_EVENT) as Event;
            if (e == null)
            {
                e = EventService.GetEvent(AppSettings.EventId);
                ApplicationCache.Insert(CACHE_KEY_EVENT, e, minutesToCache);
            }

            return e;
        }

        protected IDictionary<string, RegistrationPrice> GetRegistrationPrices()
        {
            var p = ApplicationCache.Get(CACHE_REGISTRATION_PRICES) as IDictionary<string, RegistrationPrice>;
            if (p == null )
            {
                p = EventService.GetRegistrationPrices(AppSettings.EventId);
                ApplicationCache.Insert(CACHE_REGISTRATION_PRICES, p, minutesToCache);
            }
            return p;
        }

        protected IDictionary<string, decimal> GetCurrentRegistrationPrices()
        {
            var p = ApplicationCache.Get(CACHE_CURRENT_REGISTRATION_PRICES) as IDictionary<string, decimal>;
            if (p == null)
            {
                p = EventService.GetCurrentRegistrationPrices(AppSettings.EventId);
                ApplicationCache.Insert(CACHE_CURRENT_REGISTRATION_PRICES, p, minutesToCache);
            }
            return p;
        }

        protected IList<PatronType> GetPatronTypes()
        {
            var p = ApplicationCache.Get(CACHE_PATRON_TYPES) as IList<PatronType>;
            if (p == null)
            {
                p = EventService.GetPatronTypes(AppSettings.EventId);
                ApplicationCache.Insert(CACHE_PATRON_TYPES, p, minutesToCache);
            }
            return p;
        }

    }

    public class JsonNetResult : JsonResult
    {
        public JsonNetResult()
        {
            Settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Error
            };
        }

        public JsonSerializerSettings Settings { get; private set; }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            if (JsonRequestBehavior == JsonRequestBehavior.DenyGet &&
                string.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
                throw new InvalidOperationException("JSON GET is not allowed");

            var response = context.HttpContext.Response;
            response.ContentType = string.IsNullOrEmpty(ContentType) ? "application/json" : ContentType;

            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;
            if (Data == null)
                return;

            var scriptSerializer = JsonSerializer.Create(Settings);

            using (var sw = new StringWriter())
            {
                scriptSerializer.Serialize(sw, Data);
                response.Write(sw.ToString());
            }
        }
    }
}