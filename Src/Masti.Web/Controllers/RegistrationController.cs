﻿using System.Web.Mvc;
using Masti.Services;
using Masti.Web.Infrasturcture;
using Masti.Entities.Model;

namespace Masti.Web.Controllers
{
    public class RegistrationController : MastiBaseController
    {
        public RegistrationController(IEventService eventService, IApplicationSettings appSettings,
            IApplicationCache applicationCache) 
            : base(eventService, appSettings, applicationCache)
        {
        }

        // GET: Registration
        public ActionResult Index()
        {
            var @event = GetCurrentEvent();
            if (@event.IsMastiSpellEvent())
            {
                return RedirectToAction("Index", "MastiSpellRegistration");
            } else if (@event.IsMastiFestEvent())
            {
                return RedirectToAction("Index", "MastiFestRegistration");
            }
            return View();
        }
    }
}