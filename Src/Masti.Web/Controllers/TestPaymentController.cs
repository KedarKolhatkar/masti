﻿using System.Web.Mvc;
using Masti.Services;
using Masti.Web.Infrasturcture;
using Masti.Web.Payment;

namespace Masti.Web.Controllers
{
    public class TestPaymentController : MastiBaseController
    {
        private const string PAYMENT_ID = "paymentId";
        private const string REGISTRATION = "registration";
        private const string PAYPAL_PAYMENT_ID = "payPalPaymentId";
        private const string PAYER_ID = "PayerID";

        private readonly IRegistrationService _registrationService;
        private readonly IPaypalPaymentAdaptor _paypalPaymentAdaptor;
        private readonly IPaymentIdGenerator _paymentIdGenerator;

        public TestPaymentController(IRegistrationService registrationService, IEventService eventService, 
            IPaypalPaymentAdaptor paypalPaymentAdaptor, IPaymentIdGenerator paymentIdGenerator, 
            IApplicationSettings appSettings, IApplicationCache applicationCache) 
            : base(eventService, appSettings, applicationCache)
        {
            _registrationService = registrationService;
            _paypalPaymentAdaptor = paypalPaymentAdaptor;
            _paymentIdGenerator = paymentIdGenerator;
        }

        // GET: Test
        public ActionResult Index()
        {
            return Content("This is TestPaymentController.Index method");
        }
    }
}