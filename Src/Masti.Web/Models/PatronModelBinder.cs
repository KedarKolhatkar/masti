﻿using System;
using System.Web.Mvc;
using Masti.Entities.Model;

namespace Masti.Web.Models
{
    public class PatronModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext,
            ModelBindingContext bindingContext)
        {
            if (bindingContext.ValueProvider.ContainsPrefix(bindingContext.ModelName + ".PatronTypeCode"))
            {
                //get the model type
                var patronTypeName = (string) bindingContext
                    .ValueProvider
                    .GetValue(bindingContext.ModelName + ".PatronTypeCode")
                    .ConvertTo(typeof (string));

                Type modelType;

                switch (patronTypeName)
                {
                    case "JrBeeSpeller" :
                        modelType = typeof (JrBeeSpeller);
                        break;
                    case "OlympiadSpeller":
                        modelType = typeof(OlympiadSpeller);
                        break;
                    case "Parent":
                        modelType = typeof(Parent);
                        break;
                    case "Infant":
                        modelType = typeof(Infant);
                        break;
                    case "KidSpectator":
                        modelType = typeof(KidSpectator);
                        break;
                    case "OtherSpectator":
                        modelType = typeof(OtherSpectator);
                        break;
                    default:
                        throw new ApplicationException("Unhandled modelType " + patronTypeName);
                }

                //tell the binder to use it
                bindingContext.ModelMetadata =
                    ModelMetadataProviders
                        .Current
                        .GetMetadataForType(null, modelType);
            }
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}