﻿using Masti.Entities.Model;

namespace Masti.Web.Models
{
    public class BaseRegistrationModel
    {
        public Event Event { get; set; }
    }
}