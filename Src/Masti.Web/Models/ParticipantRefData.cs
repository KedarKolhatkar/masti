﻿using System.Collections.Generic;
using Masti.Entities.Model;

namespace Masti.Web.Models
{
    public class ParticipantRefData
    {
        public IList<PatronType> PatronTypes { get; set; }
        public IList<State> States { get; set; }
        public IDictionary<string, decimal> RegistrationPriceDictionary { get; set; }
        public IDictionary<string, IList<GradeType>> GradeTypeDictionary { get; set; }
        public IDictionary<string, IList<AgeType>> AgeTypeDictionary { get; set; }
    }
}