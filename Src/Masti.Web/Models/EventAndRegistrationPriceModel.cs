﻿using System.Collections.Generic;
using Masti.Entities.Model;

namespace Masti.Web.Models
{
    public class EventAndRegistrationPriceModel : BaseRegistrationModel
    {
        public IDictionary<string, RegistrationPrice> RegististationPriceDictionary { get; set; }
    }
}