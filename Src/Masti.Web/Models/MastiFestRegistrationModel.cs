﻿using Masti.Entities.Model;
using System.Collections.Generic;

namespace Masti.Web.Models
{
    public class MastiFestRegistrationModel : BaseRegistrationModel
    {
        public IDictionary<string, RegistrationPrice> RegististationPriceDictionary { get; set; }
    }
}