﻿var DropdownUtil = function() {

    var resetDropDown = function (dropDown, addFirstRecord, firstOptionTemplateStr) {

        dropDown.empty();

        if (addFirstRecord) {
            if (firstOptionTemplateStr === "" || firstOptionTemplateStr == undefined) {
                firstOptionTemplateStr = '<option value="">-- N/A --</option>';
            };
            dropDown.append(firstOptionTemplateStr);
        }

    };

    var populateDropdown = function (dropDown, optionTemplate, data, selectedValue, firstOptionTemplateStr) {

        if (data == undefined || data === "") {
            resetDropDown(dropDown, true);
        } else {
            if (firstOptionTemplateStr == undefined || firstOptionTemplateStr === "")
                firstOptionTemplateStr = "<option value=''>-- Select --</option>";

            resetDropDown(dropDown, true, firstOptionTemplateStr);

            //var optionTemplate = Handlebars.compile(optionTemplateStr);
            $.each(data, function (index, item) {
                dropDown.append(optionTemplate(item));
            });

            if (selectedValue != undefined) {
                dropDown.find("[value='" + selectedValue + "']").attr("selected", "selected");
            }
        }
    };

    return {
        ResetDropdown: function (dropDown, addFirstRecord, firstOptionTemplateStr) {
            resetDropDown(dropDown, addFirstRecord, firstOptionTemplateStr);
        },
        PopulateDropdown: function (dropDown, optionTemplate, data, selectedValue, firstOptionTemplateStr) {
            populateDropdown(dropDown, optionTemplate, data, selectedValue, firstOptionTemplateStr);
        }
    }
}();