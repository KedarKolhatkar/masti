﻿//@reference ~/jquery
//@reference ../handlebars.js
//@reference ../autoNumeric/autoNumeric-1.9.25.js
//@reference ../jquery.validate.js
//@reference ../bootstrap.js
//@reference ../Util/Util.js

var Registration = function () {
    var settings;
    var participantReferenceData;
    var formValidator;

    var stateOptionTmpl;
    var countyOptionTmpl;
    var schoolOptionTmpl;
    var gradeOptionTmpl;
    var ageOptionTmpl;

    var participantCount = 0;
    var patrons = [];

    var compileTemplates = function() {
        stateOptionTmpl = Handlebars.compile($("#stateOptionTmpl").html());
        countyOptionTmpl = Handlebars.compile($("#countyOptionTmpl").html());
        schoolOptionTmpl = Handlebars.compile($("#schoolOptionTmpl").html());
        gradeOptionTmpl = Handlebars.compile($("#gradeOptionTmpl").html());
        ageOptionTmpl = Handlebars.compile($("#ageOptionTmpl").html());
    };

    var refreshTotalAmount = function () {
        var totalAmount = 0.00;
        $("#participants").find("[id^='amount']").each(function (index, element) {
            totalAmount += Number($(element).autoNumeric("get"));
        });

        $("#totalAmt").autoNumeric("set", totalAmount);
    };

    var refreshPatronTable = function () {
        var context = {
            //participantNum: ++participantCount,
            patrons: patrons
        };

        var patronRowTmpl = Handlebars.compile($("#patronRowTmpl").html());
        var patronBodyHtml = patronRowTmpl(context);

        $("#patronTableBody")
            .empty()
            .append(patronBodyHtml);

        var totalAmount = 0.00;
        $.each(patrons, function (index, element) {
            totalAmount += parseInt(element.Amount);
        });

        $("#totalAmt").autoNumeric("set", totalAmount);
    };

    var showAddEditPatronModal = function (patronId) {


        var context = {
            patron: patronId == undefined ? {} : patrons[patronId],
            refData: participantReferenceData
        };

        var patronFormTmpl = Handlebars.compile($("#patronFormTmpl").html());
        var patronFormHtml = patronFormTmpl(context);

        $("#addParticipantModal #addPatronForm")
            .empty()
            .append(patronFormHtml);

        $("#addParticipantModal #addPatronForm #amount").autoNumeric("init");

        if (patronId == undefined) {
            $("#addParticipantModal").find("h4#modalTitle").html("Add Patron");
            $("#addPatronButton").text("Add");

            $("#otherSchoolChkBox").prop("checked", false);
            $("#otherSchoolChkBox").trigger("change");
            $("#addPatronButton").text("Add");
        } else {
            $("#addParticipantModal").find("h4#modalTitle").html("Edit Patron");
            $("#addPatronButton").text("Update");

            $("#otherSchoolChkBox").prop("checked", context.patron.HasOtherSchool);
            $("#patronType").val(context.patron.PatronTypeCode);

            var amountTextBox = $("#amount");
            var amt = participantReferenceData.RegistrationPriceDictionary[context.patron.PatronTypeCode];
            amountTextBox.autoNumeric("set", amt == undefined ? "0.00" : amt);
            refreshTotalAmount();

            DropdownUtil.PopulateDropdown($("#state"), stateOptionTmpl,
                participantReferenceData.States, context.patron.StateCode);

            $.getJSON(settings.GetCountiesUrl + "/" + context.patron.StateCode, function (data) {
                DropdownUtil.PopulateDropdown($("#county"), countyOptionTmpl,
                    data, context.patron.CountyId);
            });

            $.getJSON(settings.GetSchoolsUrl + "/" + context.patron.CountyId, function (data) {
                DropdownUtil.PopulateDropdown($("#school"), schoolOptionTmpl,
                    data, context.patron.SchoolId);
            });

            DropdownUtil.PopulateDropdown($("#age"), ageOptionTmpl,
                participantReferenceData.AgeTypeDictionary[context.patron.PatronTypeCode],
                context.patron.AgeTypeCode);

            // This code is duplicated. Needs refactored.
            if (context.patron.PatronTypeCode === "JrBeeSpeller"
                || context.patron.PatronTypeCode === "OlympiadSpeller"
                || context.patron.PatronTypeCode === "KidSpectator") {

                $("#state").prop("disabled", false);
                $("#county").prop("disabled", false);
                $("#school").prop("disabled", false);

                $("#grade").prop("disabled", false);
                DropdownUtil.PopulateDropdown($("#grade"), gradeOptionTmpl,
                    participantReferenceData.GradeTypeDictionary[context.patron.PatronTypeCode],
                    context.patron.GradeTypeCode);

            } else {

                $("#state").prop("disabled", true);
                $("#county").prop("disabled", true);
                $("#school").prop("disabled", true);
                $("#grade").prop("disabled", true);

            }
        }


        $("#addParticipantModal").modal("show");
    };

    var deleteParticipant = function () {
        console.log("deleteParticipant patron.length=" + patrons.length);
        if (patrons.length >= 1) {
            patrons.pop();
        }

        if (patrons.length === 0) {
            $("#DeleteParticipants").prop("disabled", true);
        }

        refreshPatronTable();
        //RegistrationCommon.RefreshTotal();
    };

    var loadReferenceData = function () {
        $.getJSON(settings.GetParticipantReferenceDataUrl, function(data) {
            participantReferenceData = data;
        });
    };

    var patronTypeSelected = function () {

        var patronTypeValue = $("#patronType").val();

        var amountTextBox = $("#amount");
        var amt = participantReferenceData.RegistrationPriceDictionary[patronTypeValue];
        amountTextBox.autoNumeric("set", amt == undefined ? "0.00" : amt);
        refreshTotalAmount();

        if (patronTypeValue !== "") {

            DropdownUtil.PopulateDropdown($("#state"), stateOptionTmpl,
                participantReferenceData.States);
            DropdownUtil.PopulateDropdown($("#age"), ageOptionTmpl,
                participantReferenceData.AgeTypeDictionary[patronTypeValue]);

        }

        if (patronTypeValue === "JrBeeSpeller" || patronTypeValue === "OlympiadSpeller") {

            $("#state").prop("disabled", false);
            $("#county").prop("disabled", false);
            $("#school").prop("disabled", false);
            $("#otherSchoolChkBox").prop("disabled", false);

            $("#grade").prop("disabled", false);
            DropdownUtil.PopulateDropdown($("#grade"), gradeOptionTmpl,
                participantReferenceData.GradeTypeDictionary[patronTypeValue]);

        } else {

            $("#state").val("");
            $("#state").trigger("change");

            $("#grade").val("");
            $("#state").prop("disabled", true);
            $("#county").prop("disabled", true);
            $("#school").prop("disabled", true);
            $("#grade").prop("disabled", true);
            $("#otherSchoolChkBox").prop("disabled", true);

        }
    };

    var stateChanged = function () {
        var stateDropdown = $("#addPatronForm #state");
        var countyDropDown = $("#addPatronForm #county");

        if ($(stateDropdown).val() === "") {
            DropdownUtil.ResetDropdown(countyDropDown, true);
            countyDropDown.trigger("change");
        } else {
            $.getJSON(settings.GetCountiesUrl + "/" + $(stateDropdown).val(), function (data) {
                DropdownUtil.PopulateDropdown(countyDropDown, countyOptionTmpl, data);
            });
        }
    };

    var countyChanged = function () {
        var countyDropDown = $("#addPatronForm #county");
        var schoolDropDown = $("#addPatronForm #school");
        if ($(countyDropDown).val() === "") {
            DropdownUtil.ResetDropdown(schoolDropDown, true);
        } else {
            $.getJSON(settings.GetSchoolsUrl + "/" + $(countyDropDown).val(), function (data) {
                DropdownUtil.PopulateDropdown(schoolDropDown, schoolOptionTmpl, data);
            });
        }
    };

    var otherSchoolChkBoxChanged = function () {
        if ($("#otherSchoolChkBox").is(":checked")) {
            $("#otherSchoolDiv").show();
            $("#school").val("");
            $("#school").prop("disabled", true);
        } else {
            $("#otherSchool").val("");
            $("#otherSchoolDiv").hide();
            $("#school").prop("disabled", false);
        }
    };

    var atLeastOneParentAdded = function () {

        var valid = false;
        $.each(patrons, function(index, element) {
            if (element.PatronTypeCode === "Parent" || element.PatronTypeCode === "OtherSpectator")
                valid = true;
        });

        return valid;
    };

    var validateSubmit = function() {
        var validationMessage = "";

        if ($("#agreement").is(":checked") === false) {
            validationMessage = "Please accept the agreement";
        }

        if (patrons.length === 0) {
            validationMessage = "At least one patron is required";
        }

        if (atLeastOneParentAdded() === false) {
            validationMessage = "Add at least one parent or other spectator";
        }

        if (validationMessage !== "") {
            $("#validationMessage").html(validationMessage);
            $("#validattionMessageDiv").show();
            return false;
        }

        $("#validattionMessageDiv").hide();

        return true;
    };

    var regiserPatrons = function () {

        if (!validateSubmit())
            return;

        var registration = {
            Amount: $("#totalAmt").autoNumeric("get"),
            AttendingSpellingBootcamp: $("#AttendingSpellingBootcamp").is(":checked"),
            ReferredBy: $("#referredBy").val(),
            Patrons: patrons
        };

        $.ajax({
            type: "POST",
            url: settings.RegisterUrl,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(registration)
        }).done(function(msg) {
            alert("Message: " + msg);
        }).error(function(jqxhr, status, errorMsg) {
            alert("Error: " + errorMsg);
        });
    };

    var validateForm = function () {

        var emailRegex = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
        $('#addPatronForm').validate({
            ignore: [],
            rules: {
                patronType: {
                    required: true
                },
                firstName: {
                    minlength: 2,
                    maxlength: 50,
                    required: true
                },
                middleName: {
                    minlength: 1,
                    maxlength: 50,
                    required: false
                },
                lastName: {
                    minlength: 2,
                    maxlength: 50,
                    required: true
                },
                email: {
                    regex: emailRegex,
                    emailConditionallyRequired: true
                },
                state: {
                    stateConditionallyRequired: true
                },
                county: {
                    countyConditionallyRequired: true
                },
                school: {
                    schoolConditionallyRequired: true,
                    maxlength: 100
                },
                otherSchool: {
                    otherSchoolConditionallyRequired: true,
                    maxlength: 100
                },
                grade: {
                    gradeConditionallyRequired: true
                },
                age: {
                    ageConditionallyRequired: true
                }
            },
            errorElement: "span",
            errorClass: "help-block",
            highlight: function (element) {
                $(element).closest(".form-group").addClass("has-error");
            },
            unhighlight: function (element) {
                $(element).closest(".form-group").removeClass("has-error");
            },
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    };

    var addUpdateOnePatron = function () {
        validateForm();

        if ($("#addPatronForm").valid()) {
            var patronId = $("#patronId").val();

            var pForm = $("#addPatronForm");
            var patron = {
                PatronId: patronId === "" ? patrons.length : patronId,
                FirstName: pForm.find("#firstName").val(),
                MiddleName: pForm.find("#middleName").val(),
                LastName: pForm.find("#lastName").val(),
                Email: pForm.find("#email").val(),
                PatronTypeCode: pForm.find("#patronType").val(),
                PatronTypeDescription: pForm.find("#patronType option:selected").text(),
                StateCode: pForm.find("#state").val(),
                CountyId: pForm.find("#county").val(),
                CountyName: pForm.find("#county option:selected").text(),
                SchoolId: pForm.find("#school").val(),
                SchoolName: pForm.find("#otherSchoolChkBox").is(":checked") ?
                    pForm.find("#otherSchool").val() : pForm.find("#school option:selected").text(),
                HasOtherSchool: pForm.find("#otherSchoolChkBox").is(":checked"),
                OtherSchool: pForm.find("#otherSchool").val(),
                GradeTypeCode: pForm.find("#grade").val(),
                GradeTypeDescription: pForm.find("#grade option:selected").text(),
                AgeTypeCode: pForm.find("#age").val(),
                AgeTypeDescription: pForm.find("#age option:selected").text(),
                Amount: pForm.find("#amount").val()
            };

            if (patronId === "") {
                patrons.push(patron);
            } else {
                patrons[patronId] = patron;
            }

            refreshPatronTable();

            $("#addParticipantModal").modal("hide");
        }

    };

    var addCustomValidationMethods = function () {
        $.validator.addMethod(
                "regex",
                function (value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "Please enter a valid email."
        );

        $.validator.addMethod("emailConditionallyRequired", function (value) {
            var isValid;
            if ($("#patronType").val() !== "Parent" || ($("#patronType").val() === "Parent" && value !== ""))
                isValid = true;
            else
                isValid = false;

            return isValid; 
        }, "Email is required for Parent/Guardian");

        $.validator.addMethod("stateConditionallyRequired", function (value) {
            var isValid;
            var patronTypeValue = $("#patronType").val();
            var hasOtherSchool = $("#otherSchoolChkBox").is(":checked");
            if (
                (patronTypeValue !== "JrBeeSpeller" && patronTypeValue !== "OlympiadSpeller") || (value !== "")
                || hasOtherSchool)
                isValid = true;
            else
                isValid = false;

            return isValid; 
        }, "State is required");

        $.validator.addMethod("countyConditionallyRequired", function (value) {
            var isValid;
            var patronTypeValue = $("#patronType").val();
            var hasOtherSchool = $("#otherSchoolChkBox").is(":checked");
            if ((patronTypeValue !== "JrBeeSpeller" && patronTypeValue !== "OlympiadSpeller") || (value !== "")
                || hasOtherSchool)
                isValid = true;
            else
                isValid = false;

            return isValid;
        }, "County is required");

        $.validator.addMethod("schoolConditionallyRequired", function (value) {
            var isValid;
            var patronTypeValue = $("#patronType").val();

            isValid = true;
            if ((patronTypeValue === "JrBeeSpeller" || patronTypeValue === "OlympiadSpeller") 
                && $("#otherSchoolChkBox").is(":checked") === false
                && value === "") {
                    isValid = false;
            }

            return isValid;
        }, "School is required");

        $.validator.addMethod("otherSchoolConditionallyRequired", function (value) {
            var isValid;
            var patronTypeValue = $("#patronType").val();

            isValid = true;
            if ((patronTypeValue === "JrBeeSpeller" || patronTypeValue === "OlympiadSpeller")
                && $("#otherSchoolChkBox").is(":checked") === true
                && value === "") {
                isValid = false;
            }

            return isValid;
        }, "Other School is required");

        $.validator.addMethod("gradeConditionallyRequired", function (value) {
            var isValid;
            var patronTypeValue = $("#patronType").val();
            if ((patronTypeValue !== "JrBeeSpeller" && patronTypeValue !== "OlympiadSpeller") || (value !== ""))
                isValid = true;
            else
                isValid = false;

            return isValid;
        }, "Grade is required");

        $.validator.addMethod("ageConditionallyRequired", function (value) {
            return value !== "";
        }, "Age is required"); 

    };

    var setupAddPatronModal = function() {
        $("#addParticipantModal").on("hidden.bs.modal", function () {
            if (formValidator != undefined)
                formValidator.resetForm();
        });

        addCustomValidationMethods();
    };

    var initiatePayment = function (btnElement) {

        if (!validateSubmit())
            return;

        $(btnElement).attr("disabled", "disabled");

        var registration = {
            Amount: $("#totalAmt").autoNumeric("get"),
            AttendingSpellingBootcamp: $("#AttendingSpellingBootcamp").is(":checked"),
            ReferredBy: $("#referredBy").val(),
            Patrons: patrons
        };

        $.ajax({
            type: "POST",
            url: settings.InitiatePaymentUrl,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(registration)
        }).done(function (msg) {
            //alert("Message: " + msg.result);
            window.location.href = msg.approvalUrl;
        }).error(function (jqxhr, status, errorMsg) {
            alert("Error: " + errorMsg);
        });
    };

    var loadCurrentRegistration = function() {
        $.getJSON(settings.GetCurrentRegistrationUrl, function (data) {
            if (data.Result === "success" && data.Registration != null) {
                $("#referredBy").val(data.Registration.ReferredBy);
                $("#AttendingSpellingBootcamp").prop("checked", data.Registration.AttendingSpellingBootcamp);


                $.each(data.Registration.Patrons, function(index, patron) {
                    patrons.push(patron);
                });

                refreshPatronTable();
            }
        });
    };

    return {
        InitView: function() {
            settings = window.settingsObject;
            compileTemplates();
            $("#totalAmt").autoNumeric("init", { aSign: "$" });
            loadReferenceData();
            setupAddPatronModal();
            loadCurrentRegistration();
        },
        ShowAddPatronModal: function() {
            showAddEditPatronModal();
        },
        ShowEditPatronModal: function (patronId) {
            showAddEditPatronModal(patronId);
        },
        DeleteParticipant: function () {
            deleteParticipant();
        },
        StateChanged: function() {
            stateChanged();
        },
        CountyChanged: function() {
            countyChanged();
        },
        PatronTypeSelected: function() {
            patronTypeSelected();
        },
        OtherSchoolChkBoxChanged: function() {
            otherSchoolChkBoxChanged();
        },
        ProcessSubmit: function() {
            regiserPatrons();
        },
        InitiatePayment: function (btnElement) {
            initiatePayment(btnElement);
        },
        AddUpdatePatron: function () {
            addUpdateOnePatron();
        }
    }
}();
