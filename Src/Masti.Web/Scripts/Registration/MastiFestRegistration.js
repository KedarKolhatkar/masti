﻿//@reference ~/jquery
//@reference ../handlebars.js
//@reference ../autoNumeric/autoNumeric-1.9.25.js
//@reference ../jquery.validate.js
//@reference ../bootstrap.js
//@reference ../Util/Util.js

var MastiFestRegistration = function () {
    var settings;
    var participantReferenceData;
    var formValidator;

    var participantCount = 0;
    var patrons = [];

    var refreshTotalAmount = function () {
        var totalAmount = 0.00;
        $("#participants").find("[id^='amount']").each(function (index, element) {
            totalAmount += Number($(element).autoNumeric("get"));
        });

        $("#totalAmt").autoNumeric("set", totalAmount);
    };

    var refreshPatronTable = function () {
        var context = {
            //participantNum: ++participantCount,
            patrons: patrons
        };

        var patronRowTmpl = Handlebars.compile($("#patronRowTmpl").html());
        var patronBodyHtml = patronRowTmpl(context);

        $("#patronTableBody")
            .empty()
            .append(patronBodyHtml);

        var totalAmount = 0.00;
        $.each(patrons, function (index, element) {
            totalAmount += parseInt(element.Amount);
        });

        $("#totalAmt").autoNumeric("set", totalAmount);
    };

    var showAddEditPatronModal = function (patronId) {


        var context = {
            patron: patronId == undefined ? {} : patrons[patronId],
            refData: participantReferenceData
        };

        var patronFormTmpl = Handlebars.compile($("#patronFormTmpl").html());
        var patronFormHtml = patronFormTmpl(context);

        $("#addParticipantModal #addPatronForm")
            .empty()
            .append(patronFormHtml);

        $("#addParticipantModal #addPatronForm #amount").autoNumeric("init");

        if (patronId == undefined) {
            $("#addParticipantModal").find("h4#modalTitle").html("Add Patron");
            $("#addPatronButton").text("Add");

            $("#addPatronButton").text("Add");
        } else {
            $("#addParticipantModal").find("h4#modalTitle").html("Edit Patron");
            $("#addPatronButton").text("Update");

            $("#patronType").val(context.patron.PatronTypeCode);

            var amountTextBox = $("#amount");
            var amt = participantReferenceData.RegistrationPriceDictionary[context.patron.PatronTypeCode];
            amountTextBox.autoNumeric("set", amt == undefined ? "0.00" : amt);
            refreshTotalAmount();
        }

        $("#addParticipantModal").modal("show");
    };

    var deleteParticipant = function () {
        console.log("deleteParticipant patron.length=" + patrons.length);
        if (patrons.length >= 1) {
            patrons.pop();
        }

        if (patrons.length === 0) {
            $("#DeleteParticipants").prop("disabled", true);
        }

        refreshPatronTable();
        //RegistrationCommon.RefreshTotal();
    };

    var loadReferenceData = function () {
        $.getJSON(settings.GetParticipantReferenceDataUrl, function(data) {
            participantReferenceData = data;
        });
    };

    var patronTypeSelected = function () {

        var patronTypeValue = $("#patronType").val();

        var amountTextBox = $("#amount");
        var amt = participantReferenceData.RegistrationPriceDictionary[patronTypeValue];
        amountTextBox.autoNumeric("set", amt == undefined ? "0.00" : amt);
        refreshTotalAmount();
    };

    var atLeastOneAdultAdded = function () {

        var valid = false;
        $.each(patrons, function(index, element) {
            if (element.PatronTypeCode === "Adult")
                valid = true;
        });

        return valid;
    };

    var validateSubmit = function() {
        var validationMessage = "";

        if ($("#agreement").is(":checked") === false) {
            validationMessage = "Please accept the agreement";
        }

        if (patrons.length === 0) {
            validationMessage = "At least one patron is required";
        }

        if (atLeastOneAdultAdded() === false) {
            validationMessage = "Add at least one adult patron";
        }

        if (validationMessage !== "") {
            $("#validationMessage").html(validationMessage);
            $("#validattionMessageDiv").show();
            return false;
        }

        $("#validattionMessageDiv").hide();

        return true;
    };

    var regiserPatrons = function () {

        if (!validateSubmit())
            return;

        var registration = {
            Amount: $("#totalAmt").autoNumeric("get"),
            AttendingSpellingBootcamp: $("#AttendingSpellingBootcamp").is(":checked"),
            ReferredBy: $("#referredBy").val(),
            Patrons: patrons
        };

        $.ajax({
            type: "POST",
            url: settings.RegisterUrl,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(registration)
        }).done(function(msg) {
            alert("Message: " + msg);
        }).error(function(jqxhr, status, errorMsg) {
            alert("Error: " + errorMsg);
        });
    };

    var validateForm = function () {

        var emailRegex = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
        $('#addPatronForm').validate({
            ignore: [],
            rules: {
                patronType: {
                    required: true
                },
                firstName: {
                    minlength: 2,
                    maxlength: 50,
                    required: true
                },
                middleName: {
                    minlength: 1,
                    maxlength: 50,
                    required: false
                },
                lastName: {
                    minlength: 2,
                    maxlength: 50,
                    required: true
                },
                email: {
                    regex: emailRegex,
                    emailConditionallyRequired: true
                }
            },
            errorElement: "span",
            errorClass: "help-block",
            highlight: function (element) {
                $(element).closest(".form-group").addClass("has-error");
            },
            unhighlight: function (element) {
                $(element).closest(".form-group").removeClass("has-error");
            },
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    };

    var addUpdateOnePatron = function () {
        validateForm();

        if ($("#addPatronForm").valid()) {
            var patronId = $("#patronId").val();

            var pForm = $("#addPatronForm");
            var patron = {
                PatronId: patronId === "" ? patrons.length : patronId,
                FirstName: pForm.find("#firstName").val(),
                MiddleName: pForm.find("#middleName").val(),
                LastName: pForm.find("#lastName").val(),
                Email: pForm.find("#email").val(),
                PatronTypeCode: pForm.find("#patronType").val(),
                PatronTypeDescription: pForm.find("#patronType option:selected").text(),
                Amount: pForm.find("#amount").val()
            };

            if (patronId === "") {
                patrons.push(patron);
            } else {
                patrons[patronId] = patron;
            }

            refreshPatronTable();

            $("#addParticipantModal").modal("hide");
        }

    };

    var addCustomValidationMethods = function () {
        $.validator.addMethod(
                "regex",
                function (value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "Please enter a valid email."
        );

        $.validator.addMethod("emailConditionallyRequired", function (value) {
            var isValid;
            if ($("#patronType").val() !== "Adult" || ($("#patronType").val() === "Adult" && value !== ""))
                isValid = true;
            else
                isValid = false;

            return isValid; 
        }, "Email is required for Adult");
    };

    var setupAddPatronModal = function() {
        $("#addParticipantModal").on("hidden.bs.modal", function () {
            if (formValidator != undefined)
                formValidator.resetForm();
        });

        addCustomValidationMethods();
    };

    var initiatePayment = function (btnElement) {

        if (!validateSubmit())
            return;

        $(btnElement).attr("disabled", "disabled");
        $("body").css("cursor", "wait");

        var registration = {
            Amount: $("#totalAmt").autoNumeric("get"),
            AttendingSpellingBootcamp: false, // TODO: Needs refactoring. Attribute not required for MastFest
            ReferredBy: $("#referredBy").val(),
            Patrons: patrons
        };

        $.ajax({
            type: "POST",
            url: settings.InitiatePaymentUrl,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(registration)
        }).done(function (msg) {
            //alert("Message: " + msg.result);
            window.location.href = msg.approvalUrl;
        }).error(function (jqxhr, status, errorMsg) {
            $("body").css("cursor", "default");
            alert("Error: " + errorMsg);
        });
    };

    var loadCurrentRegistration = function() {
        $.getJSON(settings.GetCurrentRegistrationUrl, function (data) {
            if (data.Result === "success" && data.Registration != null) {
                $("#referredBy").val(data.Registration.ReferredBy);

                $.each(data.Registration.Patrons, function(index, patron) {
                    patrons.push(patron);
                });

                refreshPatronTable();
            }
        });
    };

    return {
        InitView: function() {
            settings = window.settingsObject;
            $("#totalAmt").autoNumeric("init", { aSign: "$" });
            loadReferenceData();
            setupAddPatronModal();
            //loadCurrentRegistration();
        },
        ShowAddPatronModal: function() {
            showAddEditPatronModal();
        },
        ShowEditPatronModal: function (patronId) {
            showAddEditPatronModal(patronId);
        },
        DeleteParticipant: function () {
            deleteParticipant();
        },
        PatronTypeSelected: function() {
            patronTypeSelected();
        },
        ProcessSubmit: function() {
            regiserPatrons();
        },
        InitiatePayment: function (btnElement) {
            initiatePayment(btnElement);
        },
        AddUpdatePatron: function () {
            addUpdateOnePatron();
        }
    }
}();
