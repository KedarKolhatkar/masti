﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Masti.Web.Infrasturcture;
using NLog;

namespace Masti.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private IContainer _container;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelBinderConfig.RegisterBinders();
            _container = ContainerConfiguration.Register(GlobalConfiguration.Configuration);

            ////////////////////////////////////////////////////////////////////////////////////////////////////
            /** This is a defect fix for EntityFramework 6.0. 
             * EntityFramework 6.0 will not copy EntityFramework.SqlServer.dll file (even when it is marked as 'Copy Local'
             */

            var bugFix = typeof(System.Data.Entity.SqlServer.SqlProviderServices).ToString();
            ////////////////////////////////////////////////////////////////////////////////////////////////////
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var exc = Server.GetLastError();
            _logger.Error(exc, "Uncaught error", null);
            //Server.ClearError();
        }
    }
}