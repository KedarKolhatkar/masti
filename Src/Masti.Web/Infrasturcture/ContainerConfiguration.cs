﻿using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Masti.Services.Infrastructure;
using Masti.Web.Payment;

namespace Masti.Web.Infrasturcture
{
    public class ContainerConfiguration
    {
        public static IContainer Register(HttpConfiguration configuration)
        {
            var builder = new ContainerBuilder();

            // Register dependent modules
            builder.RegisterModule(new MastiServicesModule());

            builder.Register(c => new ApplicationSettings()).As<IApplicationSettings>();
            builder.Register(c => new PaypalPaymentAdaptor()).As<IPaypalPaymentAdaptor>();
            builder.Register(c => new PaymentIdGenerator()).As<IPaymentIdGenerator>();
            builder.Register(c => new ApplicationCache()).As<IApplicationCache>();

            // Register Controllers
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            return container;
        }
    }
}
