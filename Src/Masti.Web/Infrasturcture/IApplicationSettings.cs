﻿namespace Masti.Web.Infrasturcture
{
    public interface IApplicationSettings
    {
        int EventId { get; }
        int MinutesToCacheAppObjects { get; }
    }
}