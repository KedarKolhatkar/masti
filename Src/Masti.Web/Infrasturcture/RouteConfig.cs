﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Masti.Web.Infrasturcture
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "SchoolsByCountyId",
                url: "MastiSpellRegistration/GetSchools/{countyId}",
                defaults: new { controller = "MastiSpellRegistration", action = "GetSchools", countyId = UrlParameter.Optional }
            );
            
            routes.MapRoute(
                name: "CountiesByStateCode",
                url: "MastiSpellRegistration/GetCounties/{stateCode}",
                defaults: new { controller = "MastiSpellRegistration", action = "GetCounties", stateCode = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "InitiatePayment",
            //    url: "Payment/TestPaymentGet",
            //    defaults: new { controller = "Payment", action = "TestPaymentGet" }
            //);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Registration", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}