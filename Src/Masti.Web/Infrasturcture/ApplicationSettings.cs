﻿using System;
using System.Configuration;

namespace Masti.Web.Infrasturcture
{
    public class ApplicationSettings : IApplicationSettings
    {
        // AppSettings keys
        private const string EVENTID = "EventId";
        private const string MINUTES_TO_CACHE_APP_OBJECTS = "ApplicationObjectToCacheInMinutes";

        private int? _eventId;
        private int? _minutesToCacheAppObjects;

        public int EventId
        {
            get
            {
                if (_eventId != null) return _eventId.Value;

                _eventId = GetIntValue(EVENTID);
                return _eventId.Value;
            }
        }

        public int MinutesToCacheAppObjects
        {
            get
            {
                if (_minutesToCacheAppObjects != null) return _minutesToCacheAppObjects.Value;

                _minutesToCacheAppObjects = GetIntValue(MINUTES_TO_CACHE_APP_OBJECTS);
                return _minutesToCacheAppObjects.Value;
            }
        }

        #region Private Methods
        private int GetIntValue(string key)
        {
            var value = ConfigurationManager.AppSettings[key];
            int result;
            if (string.IsNullOrEmpty(value) || !int.TryParse(value, out result))
            {
                throw new ApplicationException(string.Format("Required AppSetting key {0} not found in web.config file", key));
            }

            return result;
        }

        private string GetStringValue(string key)
        {
            var value = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrEmpty(value))
            {
                throw new ApplicationException(string.Format("Required AppSetting key {0} not found in web.config file", key));
            }

            return value;
        }

        #endregion

    }
}