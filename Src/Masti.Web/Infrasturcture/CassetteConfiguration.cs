using Cassette;
using Cassette.Scripts;
using Cassette.Stylesheets;

namespace Masti.Web.Infrasturcture
{
    /// <summary>
    /// Configures the Cassette asset bundles for the web application.
    /// </summary>
    public class CassetteBundleConfiguration : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            // TODO: Configure your bundles here...
            // Please read http://getcassette.net/documentation/configuration

            // This default configuration treats each file as a separate 'bundle'.
            // In production the content will be minified, but the files are not combined.
            // So you probably want to tweak these defaults!
            bundles.AddPerIndividualFile<StylesheetBundle>("Content");
            bundles.AddPerIndividualFile<StylesheetBundle>("Styles");
            bundles.AddPerIndividualFile<ScriptBundle>("Scripts");

            bundles.AddUrlWithAlias("//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js", "~/jquery");

            // To combine files, try something like this instead:
            //   bundles.Add<StylesheetBundle>("Content");
            // In production mode, all of ~/Content will be combined into a single bundle.
            
            // If you want a bundle per folder, try this:
//            bundles.AddPerSubDirectory<ScriptBundle>("Scripts", true);
            //bundles.Add<ScriptBundle>("autoNumeric", b => b.PageLocation = "AtEndOfBody");
            //bundles.AddPerSubDirectory<ScriptBundle>("Registration", b => b.PageLocation = "AtEndOfBody");
            //bundles.AddPerSubDirectory<ScriptBundle>("Util", b => b.PageLocation = "AtEndOfBody");
            //bundles.AddPerSubDirectory<ScriptBundle>("Workarounds", b => b.PageLocation = "AtEndOfBody");
            
            //bundles.AddUrlWithAlias("jquery", "//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js");
            // Each immediate sub-directory of ~/Scripts will be combined into its own bundle.
            // This is useful when there are lots of scripts for different areas of the website.
        }
    }
}