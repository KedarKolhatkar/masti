﻿using System;
using System.Web;
using System.Web.Caching;

namespace Masti.Web.Infrasturcture
{
    public interface IApplicationCache
    {
        object Get(string key);
        void Insert(string key, object value, int minutes);
        void Remove(string key);
    }

    public class ApplicationCache : IApplicationCache
    {
        public object Get(string key)
        {
            return HttpRuntime.Cache.Get(key);
        }

        public void Insert(string key, object value, int minutes)
        {
            HttpRuntime.Cache.Insert(key, value, null,
                    Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(minutes));
        }

        public void Remove(string key)
        {
            HttpRuntime.Cache.Remove(key);
        }
    }
}