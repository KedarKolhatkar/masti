﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Masti.DTO;
using PayPal.Api;

namespace Masti.Web.Payment
{
    public class PaypalPaymentAdaptor : IPaypalPaymentAdaptor
    {
        private readonly APIContext _apiContext;
        private PayPal.Api.Payment _createdPayment;

        public PaypalPaymentAdaptor()
        {
            _apiContext = PaypalConfiguration.GetAPIContext();
        }

        public PayPal.Api.Payment CreatePayment(RegistrationDTO registration, string completeUrl, string cancelUrl)
        {
            var itemList = new ItemList
            {
                items = (from patron in registration.Patrons
                    select new Item
                    {
                        name = GetPatronDescription(registration, patron),
                        currency = "USD",
                        price = patron.Amount.ToString(CultureInfo.InvariantCulture),
                        quantity = "1"
                    }).ToList()
            };

            var payer = new Payer {payment_method = "paypal"};

            var redirUrls = new RedirectUrls
            {
                cancel_url = cancelUrl,
                return_url = completeUrl
            };

            var details = new Details
            {
                tax = "0",
                shipping = "0",
                subtotal = registration.Amount.ToString(CultureInfo.InvariantCulture)
            };

            var amount = new Amount
            {
                currency = "USD",
                total = registration.Amount.ToString(CultureInfo.InvariantCulture), // Total must be equal to sum of shipping, tax and subtotal.
                details = details
            };

            var transactionList = new List<Transaction>();
            transactionList.Add(new Transaction
            {
                description = string.Format("Registrationn for {0}. Confirmation #: {1}", 
                    registration.EventName, registration.InvoiceNumber.ToString()),
                invoice_number = registration.InvoiceNumber.ToString(),
                amount = amount,
                item_list = itemList
            });

            var payment = new PayPal.Api.Payment
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };

            _createdPayment = payment.Create(_apiContext);
            return _createdPayment;;
        }

        public string GetPatronDescription(RegistrationDTO registration, PatronDTO patron)
        {
            if (registration.EventTypeCode == "MASTISPELL")
            {
                if (patron.PatronTypeCode == "JrBeeSpeller")
                {
                    return string.Format("{0} registration for {1} ({2}) {3}", registration.EventName,
                        patron.FullName(), patron.PatronTypeDescription, patron.GradeTypeDescription);
                }
                else
                {
                    return string.Format("{0} registration for {1} ({2})", registration.EventName,
                        patron.FullName(), patron.PatronTypeDescription);
                }
            }
            else
            {
                return string.Format("{0} registration for {1} ({2})", registration.EventName,
                    patron.FullName(), patron.PatronTypeDescription);
            }
        }

        public string GetApprovalUrl()
        {
            if (_createdPayment == null)
                throw new ApplicationException("Payment not created yet");

            var links = _createdPayment.links.GetEnumerator();

            string approvalUrl = string.Empty;
            while (links.MoveNext())
            {
                var link = links.Current;
                if (link == null)
                    throw new ApplicationException("links.Current is null");

                if (link.rel.ToLower().Trim().Equals("approval_url"))
                {
                    approvalUrl = link.href;
                }
            }

            return approvalUrl;
        }
    }
}