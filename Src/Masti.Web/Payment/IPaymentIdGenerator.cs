﻿namespace Masti.Web.Payment
{
    public interface IPaymentIdGenerator
    {
        int Generate();
    }
}