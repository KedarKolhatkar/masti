using Masti.DTO;

namespace Masti.Web.Payment
{
    public interface IPaypalPaymentAdaptor
    {
        PayPal.Api.Payment CreatePayment(RegistrationDTO registration, string completeUrl, string cancelUrl);
        string GetApprovalUrl();
        string GetPatronDescription(RegistrationDTO registration, PatronDTO patron);
    }
}