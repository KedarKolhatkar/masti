﻿using System;

namespace Masti.Web.Payment
{
    public class PaymentIdGenerator : IPaymentIdGenerator
    {
        public int Generate()
        {
            var rnd = new Random();
            return rnd.Next(1, 1000);
        }
    }
}