﻿using Autofac;
using Masti.DataAccess;
using Masti.DataAccess.Infrastructure;
using Masti.Entities.Model;
using Masti.Services.Impl;
using Masti.Services.Translators;

namespace Masti.Services.Infrastructure
{
    public class MastiServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new MastiDataAccessModule());
            builder.Register(c => new PatronTranslator()).As<IPatronTranslator>();
            builder.Register(c => new RegistrationTranslator()).As<IRegistrationTranslator>();

            builder.Register(c => new LookupService(c.Resolve<ILookupTypeRepository>(),
                c.Resolve<IEntityRepository<School>>(), c.Resolve<IEntityRepository<County>>(),
                c.Resolve<IEntityRepository<State>>(), c.Resolve<IEntityRepository<PatronTypeGradeType>>(),
                c.Resolve<IEntityRepository<PatronTypeAgeType>>()))
                .As<ILookupService>();
            builder.Register(
                c =>
                    new EventService(c.Resolve<IEntityRepository<Event>>(),
                        c.Resolve<IEntityRepository<RegistrationPrice>>(),
                        c.Resolve<IEntityRepository<EventTypePatronType>>())).As<IEventService>();

            builder.Register(c => new RegistrationService(c.Resolve<IEntityRepository<Registration>>(), 
                c.Resolve<IPatronTranslator>(),
                c.Resolve<IRegistrationTranslator>()))
                .As<IRegistrationService>();
        }
    }
}
