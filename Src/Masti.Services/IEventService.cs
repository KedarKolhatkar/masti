﻿using System.Collections.Generic;
using Masti.Entities.Model;

namespace Masti.Services
{
    public interface IEventService
    {
        Event GetEvent(int eventId);
        IList<RegistrationPrice> GetEventRegistrationPrices(int eventId);
        IDictionary<string, RegistrationPrice> GetRegistrationPrices(int eventId);
        IDictionary<string, decimal> GetCurrentRegistrationPrices(int eventId);

        IList<PatronType> GetPatronTypes(int eventId);
    }
}