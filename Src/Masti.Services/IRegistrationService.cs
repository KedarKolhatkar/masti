﻿using Masti.DTO;
using Masti.Entities.Model;

namespace Masti.Services
{
    public interface IRegistrationService
    {
        void SaveOnPaymentInitiation(RegistrationDTO registrationDTO);
        void SaveOnPaymentCompletion(int registrationId, string pmtVendorOrderId);
        Registration GetByMastiPaymentId(int registrationId);
        void SaveOnPaymentCancellation(int id);
    }
}