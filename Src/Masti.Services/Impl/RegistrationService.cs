using System;
using Masti.DataAccess;
using Masti.DTO;
using Masti.Entities.Model;
using Masti.Services.Translators;
using System.Linq;

namespace Masti.Services.Impl
{
    public class RegistrationService : IRegistrationService
    {
        private readonly IEntityRepository<Registration> _registrationRepository;
        private readonly IPatronTranslator _patronTranslator;
        private readonly IRegistrationTranslator _registrationTranslator;

        public RegistrationService(IEntityRepository<Registration> registrationRepository, IPatronTranslator patronTranslator, 
            IRegistrationTranslator registrationTranslator)
        {
            _registrationRepository = registrationRepository;
            _patronTranslator = patronTranslator;
            _registrationTranslator = registrationTranslator;
        }

        public void SaveOnPaymentInitiation(RegistrationDTO registrationDTO)
        {
            if (registrationDTO == null) throw new ArgumentNullException("registrationDTO");
            if (registrationDTO.InvoiceNumber == Guid.Empty) throw new ApplicationException("registrationDTO.InvoiceNumber cannot be zero");

            Registration reg = _registrationTranslator.TranslateToRegistration(registrationDTO);
            reg.PmtVendorCode = "PAYPAL";
            reg.RegisteredDate = reg.PaymentInitiationDate = DateTime.Now;

            reg.StatusCode = RegistrationStatus.Initiated;

            foreach (var patronDTO in registrationDTO.Patrons)
            {
                reg.Patrons.Add(_patronTranslator.TranslateToPatron(patronDTO));
            }

            _registrationRepository.Add(reg);
        }

        public void SaveOnPaymentCompletion(int registrationId, string pmtVendorOrderId)
        {
            if (string.IsNullOrEmpty(pmtVendorOrderId))
                throw new ApplicationException("pmtVendorOrderId is cannot be blank or null");

            var reg = _registrationRepository.GetById(registrationId);
            if (reg == null) throw new ApplicationException(
                string.Format("Registration with id={0} not found", registrationId));

            reg.PaymentCompleteDate = DateTime.Now;
            reg.PmtVendorOrderId = pmtVendorOrderId;
            reg.StatusCode = RegistrationStatus.Completed;

            _registrationRepository.Update(reg);
        }

        public Registration GetByMastiPaymentId(int mastiPaymentId)
        {
            return _registrationRepository.GetAll().FirstOrDefault(t => t.MastiPaymentId == mastiPaymentId);
        }

        public void SaveOnPaymentCancellation(int registrationId)
        {
            var reg = _registrationRepository.GetById(registrationId);
            if (reg == null) throw new ApplicationException(
                string.Format("Registration with id={0} not found", registrationId));

            reg.StatusCode = RegistrationStatus.Cancelled;
            reg.PaymentCancelledDate = DateTime.Now;
            _registrationRepository.Update(reg);
        }
    }
}
