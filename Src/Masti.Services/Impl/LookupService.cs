﻿using System.Collections.Generic;
using System.Linq;
using Masti.DataAccess;
using Masti.Entities.Model;

namespace Masti.Services.Impl
{
    public class LookupService : ILookupService
    {
        private readonly IEntityRepository<County> _countyRepository;
        private readonly ILookupTypeRepository _lookupTypeRepository;
        private readonly IEntityRepository<School> _schoolRepository;
        private readonly IEntityRepository<State> _stateRepository;
        private readonly IEntityRepository<PatronTypeGradeType> _patronTypeGradeTypeRepository;
        private readonly IEntityRepository<PatronTypeAgeType> _patronTypeAgeTypeRepository;

        public LookupService(ILookupTypeRepository lookupTypeRepository, IEntityRepository<School> schoolRepository,
            IEntityRepository<County> countyRepository, IEntityRepository<State> stateRepository,
            IEntityRepository<PatronTypeGradeType> patronTypeGradeTypeRepository, IEntityRepository<PatronTypeAgeType> patronTypeAgeTypeRepository )
        {
            _lookupTypeRepository = lookupTypeRepository;
            _schoolRepository = schoolRepository;
            _countyRepository = countyRepository;
            _stateRepository = stateRepository;
            _patronTypeGradeTypeRepository = patronTypeGradeTypeRepository;
            _patronTypeAgeTypeRepository = patronTypeAgeTypeRepository;
        }

        public IList<GradeType> GetGradeTypes()
        {
            return _lookupTypeRepository.GetAll<GradeType>().ToList();
        }

        public IList<PatronType> GetPatronTypes()
        {
            return _lookupTypeRepository.GetAll<PatronType>().OrderBy(x => x.DisplayOrder).ToList();
        }

        public IList<School> GetSchools()
        {
            return _schoolRepository.GetAll().ToList();
        }

        public IList<State> GetStates()
        {
            return _stateRepository.GetAll().ToList();
        }

        public IList<County> GetCounties()
        {
            return _countyRepository.GetAll().ToList();
        }

        public IList<int> GetAges()
        {
            return new List<int>
            {
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11
            };
        }

        public IList<County> GetCounties(string stateCode)
        {
            return string.IsNullOrEmpty(stateCode)
                ? _countyRepository.GetAll().ToList()
                : _countyRepository.GetAll().Where(x => x.State.StateCode == stateCode).ToList();
        }

        public IList<School> GetSchools(int? countyId)
        {
            return countyId == null
                ? _schoolRepository.GetAll().ToList()
                : _schoolRepository.GetAll().Where(x => x.County.Id == countyId).ToList();
        }

        public IDictionary<string, IList<GradeType>> GetPatronTypeGradeTypeDictionary()
        {
            var groupBy = _patronTypeGradeTypeRepository.GetAll().GroupBy(t=>t.PatronTypeCode);
            return groupBy.ToDictionary(t => t.Key, t => (t.Select(x => x.GradeType).ToList() as IList<GradeType>));
        }

        public IDictionary<string, IList<AgeType>> GetPatronTypeAgeTypeDictionary()
        {
            var groupBy = _patronTypeAgeTypeRepository.GetAll().GroupBy(t => t.PatronTypeCode);
            return groupBy.ToDictionary(t => t.Key, t => (t.Select(x => x.AgeType).ToList() as IList<AgeType>));
        }
    }
}