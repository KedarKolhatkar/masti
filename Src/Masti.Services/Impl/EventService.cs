﻿using System;
using System.Collections.Generic;
using System.Linq;
using Masti.DataAccess;
using Masti.Entities.Model;

namespace Masti.Services.Impl
{
    public class EventService : IEventService
    {
        private readonly IEntityRepository<Event> _eventRepository;
        private readonly IEntityRepository<RegistrationPrice> _registrationPriceRepository;
        private readonly IEntityRepository<EventTypePatronType> _eventTypePatronTypeRepository;

        public EventService(IEntityRepository<Event> eventRepository,
            IEntityRepository<RegistrationPrice> registrationPriceRepository, IEntityRepository<EventTypePatronType> eventTypePatronTypeRepository )
        {
            _eventRepository = eventRepository;
            _registrationPriceRepository = registrationPriceRepository;
            _eventTypePatronTypeRepository = eventTypePatronTypeRepository;
        }

        public Event GetEvent(int eventId)
        {
            return _eventRepository.GetById(eventId);
        }

        public IList<RegistrationPrice> GetEventRegistrationPrices(int eventId)
        {
            return _registrationPriceRepository.GetAll().Where(x => x.EventId == eventId).ToList();
        }

        public IDictionary<string, RegistrationPrice> GetRegistrationPrices(int eventId)
        {
            return
                _registrationPriceRepository.GetAll()
                    .Where(x => x.EventId == eventId)
                    .ToDictionary(x => x.PatronType.Code);
        }

        public IDictionary<string, decimal> GetCurrentRegistrationPrices(int eventId)
        {
            var now = DateTime.Now;
            return _registrationPriceRepository.GetAll().Where(x => x.EventId == eventId)
                .ToDictionary(price => price.PatronType.Code, price => now > price.EndDate ? price.Amount : price.DiscountedAmount);
        }

        public IList<PatronType> GetPatronTypes(int eventId)
        {
            string eventTypeCode = _eventRepository.GetById(eventId).EventTypeCode;

            return
                _eventTypePatronTypeRepository.GetAll()
                    .Where(t => t.EventTypeCode == eventTypeCode)
                    .Select(t => t.PatronType)
                    .OrderBy(t=>t.DisplayOrder)
                    .ToList();
        }
    }
}