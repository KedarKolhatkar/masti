﻿using System.Collections.Generic;
using Masti.Entities.Model;

namespace Masti.Services
{
    public interface ILookupService
    {
        IList<GradeType> GetGradeTypes();
        IList<PatronType> GetPatronTypes();
        IList<School> GetSchools();
        IList<State> GetStates();
        IList<County> GetCounties();
        IList<int> GetAges();

        IList<County> GetCounties(string stateCode);
        IList<School> GetSchools(int? countyId);
        IDictionary<string, IList<GradeType>> GetPatronTypeGradeTypeDictionary();
        IDictionary<string, IList<AgeType>> GetPatronTypeAgeTypeDictionary();
    }
}