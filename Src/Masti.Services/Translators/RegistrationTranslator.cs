﻿using System.Collections.Generic;
using Masti.DTO;
using Masti.Entities.Model;

namespace Masti.Services.Translators
{
    public interface IRegistrationTranslator
    {
        Registration TranslateToRegistration(RegistrationDTO dto);
    }

    public class RegistrationTranslator : IRegistrationTranslator
    {
        public Registration TranslateToRegistration(RegistrationDTO dto)
        {
            return new Registration
            {
                InvoiceNumber = dto.InvoiceNumber,
                MastiPaymentId = dto.MastiPaymentId,
                Amount = dto.Amount,
                AttendingSpellingBootcamp = dto.AttendingSpellingBootcamp,
                EventId = dto.EventId,
                PmtVendorOrderId = dto.PmtVendorOrderId,
                ReferredBy = dto.ReferredBy,
                Patrons = new List<Patron>()
            };
        }
    }
}