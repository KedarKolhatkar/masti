﻿using System;
using Masti.DTO;
using Masti.Entities.Model;

namespace Masti.Services.Translators
{
    public interface IPatronTranslator
    {
        Patron TranslateToPatron(PatronDTO dto);
    }

    public class PatronTranslator : IPatronTranslator
    {
        public Patron TranslateToPatron(PatronDTO dto)
        {
            Patron patron;
            switch (dto.PatronTypeCode)
            {
                case "JrBeeSpeller":
                    patron = new JrBeeSpeller
                    {
                        FirstName = dto.FirstName,
                        MiddleName = dto.MiddleName,
                        LastName = dto.LastName,
                        Email = dto.Email,
                        AgeTypeCode = dto.AgeTypeCode,
                        Amount = dto.Amount,
                        GradeTypeCode = dto.GradeTypeCode,
                        SchoolId = dto.HasOtherSchool ? (int?) null : dto.SchoolId,
                        OtherSchool = dto.OtherSchool
                    };
                    break;
                case "OlympiadSpeller":
                    patron = new OlympiadSpeller
                    {
                        FirstName = dto.FirstName,
                        MiddleName = dto.MiddleName,
                        LastName = dto.LastName,
                        Email = dto.Email,
                        AgeTypeCode = dto.AgeTypeCode,
                        Amount = dto.Amount,
                        GradeTypeCode = dto.GradeTypeCode,
                        SchoolId = dto.SchoolId,
                        OtherSchool = dto.OtherSchool
                    };
                    break;
                case "Parent":
                    patron = new Parent
                    {
                        FirstName = dto.FirstName,
                        MiddleName = dto.MiddleName,
                        LastName = dto.LastName,
                        Email = dto.Email,
                        AgeTypeCode = dto.AgeTypeCode,
                        Amount = dto.Amount
                    };
                    break;
                case "Infant":
                case "Infant-1":
                    patron = new Infant
                    {
                        FirstName = dto.FirstName,
                        MiddleName = dto.MiddleName,
                        LastName = dto.LastName,
                        Email = dto.Email,
                        AgeTypeCode = string.IsNullOrEmpty(dto.AgeTypeCode) ? AgeType.AGE_0_2 : dto.AgeTypeCode,
                        Amount = dto.Amount
                    };
                    break;
                case "KidSpectator":
                    patron = new KidSpectator
                    {
                        FirstName = dto.FirstName,
                        MiddleName = dto.MiddleName,
                        LastName = dto.LastName,
                        Email = dto.Email,
                        AgeTypeCode = dto.AgeTypeCode,
                        Amount = dto.Amount
                    };
                    break;
                case "OtherSpectator":
                    patron = new OtherSpectator
                    {
                        FirstName = dto.FirstName,
                        MiddleName = dto.MiddleName,
                        LastName = dto.LastName,
                        Email = dto.Email,
                        AgeTypeCode = dto.AgeTypeCode,
                        Amount = dto.Amount
                    };
                    break;
                case "Adult":
                    patron = new Adult
                    {
                        FirstName = dto.FirstName,
                        MiddleName = dto.MiddleName,
                        LastName = dto.LastName,
                        Email = dto.Email,
                        AgeTypeCode = AgeType.AGE_18_PLUS,
                        Amount = dto.Amount
                    };
                    break;
                case "Youth":
                    patron = new Youth
                    {
                        FirstName = dto.FirstName,
                        MiddleName = dto.MiddleName,
                        LastName = dto.LastName,
                        Email = dto.Email,
                        AgeTypeCode = AgeType.AGE_17,
                        Amount = dto.Amount
                    };
                    break;
                case "Child":
                    patron = new Child
                    {
                        FirstName = dto.FirstName,
                        MiddleName = dto.MiddleName,
                        LastName = dto.LastName,
                        Email = dto.Email,
                        AgeTypeCode = AgeType.AGE_4,
                        Amount = dto.Amount
                    };
                    break;
                default:
                    throw new ApplicationException("Unhandled patronTypeCode " + dto.PatronTypeCode);
            }

            return patron;
        }
    }
}