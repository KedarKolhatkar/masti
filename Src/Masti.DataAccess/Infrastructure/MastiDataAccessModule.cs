﻿using Autofac;
using Masti.DataAccess.Repositories;
using Masti.Entities.Model;

namespace Masti.DataAccess.Infrastructure
{
    public class MastiDataAccessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new MastiContext()).AsSelf();
            builder.Register(c => new LookupTypeRepository(c.Resolve<MastiContext>())).As<ILookupTypeRepository>();
            
            //builder.RegisterGeneric(typeof (EntityRepository<>)).As(typeof (IRepository<>)).InstancePerLifetimeScope();
            builder.Register(c => new EntityRepository<Event>(c.Resolve<MastiContext>())).As<IEntityRepository<Event>>();
            builder.Register(c => new EntityRepository<Registration>(c.Resolve<MastiContext>())).As<IEntityRepository<Registration>>();
            builder.Register(c => new EntityRepository<School>(c.Resolve<MastiContext>())).As<IEntityRepository<School>>();
            builder.Register(c => new EntityRepository<County>(c.Resolve<MastiContext>())).As<IEntityRepository<County>>();
            builder.Register(c => new EntityRepository<State>(c.Resolve<MastiContext>())).As<IEntityRepository<State>>();
            builder.Register(c => new EntityRepository<RegistrationPrice>(c.Resolve<MastiContext>())).As<IEntityRepository<RegistrationPrice>>();
            builder.Register(c => new EntityRepository<EventTypePatronType>(c.Resolve<MastiContext>())).As<IEntityRepository<EventTypePatronType>>();
            builder.Register(c => new EntityRepository<PatronTypeGradeType>(c.Resolve<MastiContext>())).As<IEntityRepository<PatronTypeGradeType>>();
            builder.Register(c => new EntityRepository<PatronTypeAgeType>(c.Resolve<MastiContext>())).As<IEntityRepository<PatronTypeAgeType>>();

            //builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
        }
    }
}
