﻿using System.Data.Entity.ModelConfiguration;
using Masti.Entities.Model;

namespace Masti.DataAccess.Mappings
{
    public class SchoolConfiguration : EntityTypeConfiguration<School>
    {
        public SchoolConfiguration()
        {
            ToTable("School");
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id).HasColumnName("SchoolId");
            Property(t => t.Name).IsRequired().HasMaxLength(100);

            HasRequired(t => t.County).WithMany().Map(x => x.MapKey("CountyId"));
        }
    }
}
