﻿using System.Data.Entity.ModelConfiguration;
using Masti.Entities.Model;

namespace Masti.DataAccess.Mappings
{
    public class PatronTypeAgeTypeConfiguraton : EntityTypeConfiguration<PatronTypeAgeType>
    {
        public PatronTypeAgeTypeConfiguraton()
        {
            ToTable("PatronTypeAgeType");
            HasKey(t => t.Id);
            Property(t => t.Id).HasColumnName("PatronTypeAgeTypeId");
            Property(t => t.PatronTypeCode).IsRequired();
            Property(t => t.AgeTypeCode).IsRequired();

            Property(t => t.DisplayOrder).IsRequired();

            HasRequired(t => t.AgeType).WithMany().WillCascadeOnDelete(false);
            HasRequired(t => t.PatronType).WithMany();
        }
    }
}