﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Masti.Entities.Model;

namespace Masti.DataAccess.Mappings
{
    public class RegistrationConfiguration : EntityTypeConfiguration<Registration>
    {
        public RegistrationConfiguration()
        {
            ToTable("Registration");
            // Primary Key
            HasKey(t => t.Id);


            // Properties
            Property(t => t.Id)
                .HasColumnName("RegistrationId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(t => t.InvoiceNumber).IsRequired();
            Property(t => t.Amount).IsRequired();
            Property(t => t.MastiPaymentId);
            Property(t => t.PmtVendorOrderId).HasMaxLength(100).IsOptional();
            Property(t => t.PmtVendorCode).IsOptional();
            Property(t => t.PaymentInitiationDate).IsRequired();
            Property(t => t.PaymentCompleteDate).IsOptional();
            Property(t => t.PaymentCancelledDate).IsOptional();
            Property(t => t.RegisteredDate).IsRequired();
            Property(t => t.StatusCode).IsRequired();
            Property(t => t.RefundedDate).IsOptional();
            Property(t => t.EventId).IsRequired();
            Property(t => t.AttendingSpellingBootcamp).IsRequired();
            Property(t => t.ReferredBy).IsOptional();

            //Map Many-to-One relationship between County and State without property defined for StateCode
            HasRequired(t => t.Event).WithMany().HasForeignKey(x => x.EventId);
            HasRequired(t => t.Status).WithMany().HasForeignKey(x => x.StatusCode).WillCascadeOnDelete(false);
        }
    }
}
