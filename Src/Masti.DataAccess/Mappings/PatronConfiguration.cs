﻿using System.Data.Entity.ModelConfiguration;
using Masti.Entities.Model;

namespace Masti.DataAccess.Mappings
{
    public class PatronConfiguration : EntityTypeConfiguration<Patron>
    {
        public PatronConfiguration()
        {
            ToTable("Patron");
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id).HasColumnName("PatronId");
            Property(t => t.FirstName).IsRequired().HasMaxLength(100);
            Property(t => t.MiddleName).IsOptional().HasMaxLength(100);
            Property(t => t.LastName).IsRequired().HasMaxLength(100);
            Property(t => t.Email).IsOptional().HasMaxLength(100);
            Property(t => t.Amount).IsRequired();
            Property(t => t.RegistrationId).IsRequired();
            Property(t => t.AgeTypeCode).IsRequired();

            Map<JrBeeSpeller>(m => m.Requires("PatronTypeCode").HasValue(typeof(JrBeeSpeller).Name));
            Map<OlympiadSpeller>(m => m.Requires("PatronTypeCode").HasValue(typeof(OlympiadSpeller).Name));
            Map<Parent>(m => m.Requires("PatronTypeCode").HasValue(typeof(Parent).Name));
            Map<Infant>(m => m.Requires("PatronTypeCode").HasValue(typeof(Infant).Name));
            Map<KidSpectator>(m => m.Requires("PatronTypeCode").HasValue(typeof(KidSpectator).Name));
            Map<OtherSpectator>(m => m.Requires("PatronTypeCode").HasValue(typeof(OtherSpectator).Name));
            Map<Adult>(m => m.Requires("PatronTypeCode").HasValue(typeof(Adult).Name));
            Map<Youth>(m => m.Requires("PatronTypeCode").HasValue(typeof(Youth).Name));
            Map<Child>(m => m.Requires("PatronTypeCode").HasValue(typeof(Child).Name));

            HasRequired(t => t.Age).WithMany();
            HasRequired(t => t.Registration).WithMany(x=>x.Patrons);
        }
    }

    public class SpellerConfiguration : EntityTypeConfiguration<Speller>
    {
        public SpellerConfiguration()
        {
            Property(t => t.SchoolId).IsOptional();
            Property(t => t.GradeTypeCode).IsRequired();
            Property(t => t.OtherSchool).IsOptional().HasMaxLength(100);

            // Relations
            HasRequired(t => t.Grade).WithMany().WillCascadeOnDelete(false);
            HasRequired(t => t.School).WithMany();
        }
    }
}
