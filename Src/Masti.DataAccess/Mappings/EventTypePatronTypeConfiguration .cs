﻿using System.Data.Entity.ModelConfiguration;
using Masti.Entities.Model;

namespace Masti.DataAccess.Mappings
{
    public class EventTypePatronTypeConfiguration : EntityTypeConfiguration<EventTypePatronType>
    {
        public EventTypePatronTypeConfiguration()
        {
            ToTable("EventTypePatronType");
            HasKey(t => t.Id);
            Property(t => t.Id).HasColumnName("EventTypePatronTypeId");
            Property(t => t.PatronTypeCode).IsRequired();
            Property(t => t.EventTypeCode).IsRequired();

            Property(t => t.DisplayOrder).IsRequired();

            HasRequired(t => t.EventType).WithMany();
            HasRequired(t => t.PatronType).WithMany();
        }
    }
}