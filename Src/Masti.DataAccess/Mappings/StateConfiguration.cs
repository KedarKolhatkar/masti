﻿using System.Data.Entity.ModelConfiguration;
using Masti.Entities.Model;

namespace Masti.DataAccess.Mappings
{
    public class StateConfiguration : EntityTypeConfiguration<State>
    {
        public StateConfiguration()
        {
            ToTable("State");
            HasKey(t => t.StateCode);

            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);
        }
    }
}
