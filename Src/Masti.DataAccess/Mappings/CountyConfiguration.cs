﻿using System.Data.Entity.ModelConfiguration;
using Masti.Entities.Model;

namespace Masti.DataAccess.Mappings
{
    public class CountyConfiguration : EntityTypeConfiguration<County>
    {
        public CountyConfiguration()
        {
            ToTable("County");

            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id).HasColumnName("CountyId");
            Property(t => t.Name).IsRequired().HasMaxLength(100);

            // Relationships
            //Map Many-to-One relationship between County and State without property defined for StateCode
            HasRequired(t => t.State)
                .WithMany()
                .Map(x => x.MapKey("StateCode"));
            
        }
    }
}
