﻿using System.Data.Entity.ModelConfiguration;
using Masti.Entities.Model;

namespace Masti.DataAccess.Mappings
{
    public class RegistrationPriceConfiguration : EntityTypeConfiguration<RegistrationPrice>
    {
        public RegistrationPriceConfiguration()
        {
            ToTable("RegistrationPrice");
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id).HasColumnName("RegistrationPriceId");
            Property(t => t.EventId).IsRequired();
            Property(t => t.Amount).IsRequired();
            Property(t => t.DiscountedAmount).IsRequired();
            Property(t => t.StartDate).IsRequired();
            Property(t => t.EndDate).IsRequired();

            //Map Many-to-One relationship between County and State without property defined for StateCode
            HasRequired(t => t.Event).WithMany();
            HasRequired(t => t.PatronType).WithMany().Map(x => x.MapKey("PatronTypeCode"));
        }
    }
}
