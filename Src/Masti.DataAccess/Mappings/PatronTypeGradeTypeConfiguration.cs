﻿using System.Data.Entity.ModelConfiguration;
using Masti.Entities.Model;

namespace Masti.DataAccess.Mappings
{
    public class PatronTypeGradeTypeConfiguraton : EntityTypeConfiguration<PatronTypeGradeType>
    {
        public PatronTypeGradeTypeConfiguraton()
        {
            ToTable("PatronTypeGradeType");
            HasKey(t => t.Id);
            Property(t => t.Id).HasColumnName("PatronTypeGradeTypeId");
            Property(t => t.PatronTypeCode).IsRequired();
            Property(t => t.GradeTypeCode).IsRequired();

            Property(t => t.DisplayOrder).IsRequired();

            HasRequired(t => t.GradeType).WithMany().WillCascadeOnDelete(false);
            HasRequired(t => t.PatronType).WithMany();
        }
    }
}