﻿using System.Data.Entity.ModelConfiguration;
using Masti.Entities.Model;

namespace Masti.DataAccess.Mappings
{
    public class EventTypeConfiguration : EntityTypeConfiguration<EventType>
    {
        public EventTypeConfiguration()
        {
            ToTable("EventType");
            HasKey(t => t.EventTypeCode);

            Property(t => t.Name).IsRequired().HasMaxLength(50);
            Property(t => t.Description).IsRequired().HasMaxLength(200);
        }
    }
}
