﻿using System.Data.Entity.ModelConfiguration;
using Masti.Entities.Model;

namespace Masti.DataAccess.Mappings
{
    public class EventConfiguration : EntityTypeConfiguration<Event>
    {
        public EventConfiguration()
        {
            ToTable("Event");
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id).HasColumnName("EventId");
            Property(t => t.Name).IsRequired().HasMaxLength(50);
            Property(t => t.Description).HasMaxLength(200);
            Property(t => t.MainMessage).HasMaxLength(250);
            Property(t => t.HomePageURL).HasMaxLength(250);
            Property(t => t.EventDate).IsRequired();
            Property(t => t.RegistrationStartDate);
            Property(t => t.RegistrationEndDate);
            Property(t => t.VenueName).HasMaxLength(100);
            Property(t => t.VenueAddress).HasMaxLength(500);
            Property(t => t.EventTypeCode).IsRequired().HasMaxLength(20);
            Property(t => t.EarlyBirdOn).IsRequired();

            //Map Many-to-One relationship between County and State without property defined for StateCode
            HasRequired(t => t.EventType)
                .WithMany()
                .HasForeignKey(x=>x.EventTypeCode);
        }
    }
}
