﻿using System.Data.Entity.ModelConfiguration;
using Masti.Entities.Model;

namespace Masti.DataAccess.Mappings
{
    public class LookupTypeConfiguration : EntityTypeConfiguration<LookupType>
    {
        public LookupTypeConfiguration()
        {
            ToTable("ReferenceCode");
            HasKey(t => t.Code);

            Property(t => t.Description).IsRequired().HasMaxLength(100);
            Property(t => t.DisplayOrder).IsRequired();

            Map<GradeType>(m => m.Requires("Category").HasValue("GradeType"));
            Map<PatronType>(m => m.Requires("Category").HasValue("PatronType"));
            Map<AgeType>(m => m.Requires("Category").HasValue("AgeType"));
            Map<RegistrationStatus>(m => m.Requires("Category").HasValue("RegistrationStatus"));
        }
    }
}
