namespace Masti.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.County",
                c => new
                    {
                        CountyId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        StateCode = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.CountyId)
                .ForeignKey("dbo.State", t => t.StateCode, cascadeDelete: true)
                .Index(t => t.StateCode);
            
            CreateTable(
                "dbo.State",
                c => new
                    {
                        StateCode = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.StateCode);
            
            CreateTable(
                "dbo.Event",
                c => new
                    {
                        EventId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 200),
                        EventDate = c.DateTime(nullable: false),
                        RegistrationStartDate = c.DateTime(nullable: false),
                        RegistrationEndDate = c.DateTime(nullable: false),
                        VenueName = c.String(maxLength: 100),
                        VenueAddress = c.String(maxLength: 500),
                        EventTypeCode = c.String(nullable: false, maxLength: 128),
                        EarlyBirdOn = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EventId)
                .ForeignKey("dbo.EventType", t => t.EventTypeCode, cascadeDelete: true)
                .Index(t => t.EventTypeCode);
            
            CreateTable(
                "dbo.EventType",
                c => new
                    {
                        EventTypeCode = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.EventTypeCode);
            
            CreateTable(
                "dbo.EventTypePatronType",
                c => new
                    {
                        EventTypePatronTypeId = c.Int(nullable: false, identity: true),
                        EventTypeCode = c.String(nullable: false, maxLength: 128),
                        PatronTypeCode = c.String(nullable: false, maxLength: 128),
                        DisplayOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EventTypePatronTypeId)
                .ForeignKey("dbo.EventType", t => t.EventTypeCode, cascadeDelete: true)
                .ForeignKey("dbo.ReferenceCode", t => t.PatronTypeCode, cascadeDelete: true)
                .Index(t => t.EventTypeCode)
                .Index(t => t.PatronTypeCode);
            
            CreateTable(
                "dbo.ReferenceCode",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 128),
                        Description = c.String(nullable: false, maxLength: 100),
                        DisplayOrder = c.Int(nullable: false),
                        Category = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.Patron",
                c => new
                    {
                        PatronId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        MiddleName = c.String(maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        Email = c.String(maxLength: 100),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RegistrationId = c.Int(nullable: false),
                        AgeTypeCode = c.String(nullable: false, maxLength: 128),
                        GradeTypeCode = c.String(maxLength: 128),
                        SchoolId = c.Int(),
                        OtherSchool = c.String(maxLength: 100),
                        PatronTypeCode = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.PatronId)
                .ForeignKey("dbo.ReferenceCode", t => t.AgeTypeCode, cascadeDelete: true)
                .ForeignKey("dbo.Registration", t => t.RegistrationId, cascadeDelete: true)
                .ForeignKey("dbo.ReferenceCode", t => t.GradeTypeCode)
                .ForeignKey("dbo.School", t => t.SchoolId, cascadeDelete: true)
                .Index(t => t.RegistrationId)
                .Index(t => t.AgeTypeCode)
                .Index(t => t.GradeTypeCode)
                .Index(t => t.SchoolId);
            
            CreateTable(
                "dbo.Registration",
                c => new
                    {
                        RegistrationId = c.Int(nullable: false, identity: true),
                        InvoiceNumber = c.Guid(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PmtVendorOrderId = c.String(maxLength: 100),
                        PmtVendorCode = c.String(),
                        RegisteredDate = c.DateTime(nullable: false),
                        RefundedDate = c.DateTime(),
                        EventId = c.Int(nullable: false),
                        StatusCode = c.String(nullable: false, maxLength: 128),
                        AttendingSpellingBootcamp = c.Boolean(nullable: false),
                        ReferredBy = c.String(),
                    })
                .PrimaryKey(t => t.RegistrationId)
                .ForeignKey("dbo.Event", t => t.EventId, cascadeDelete: true)
                .ForeignKey("dbo.ReferenceCode", t => t.StatusCode)
                .Index(t => t.EventId)
                .Index(t => t.StatusCode);
            
            CreateTable(
                "dbo.School",
                c => new
                    {
                        SchoolId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        CountyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SchoolId)
                .ForeignKey("dbo.County", t => t.CountyId, cascadeDelete: true)
                .Index(t => t.CountyId);
            
            CreateTable(
                "dbo.PatronTypeAgeType",
                c => new
                    {
                        PatronTypeAgeTypeId = c.Int(nullable: false, identity: true),
                        PatronTypeCode = c.String(nullable: false, maxLength: 128),
                        AgeTypeCode = c.String(nullable: false, maxLength: 128),
                        DisplayOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PatronTypeAgeTypeId)
                .ForeignKey("dbo.ReferenceCode", t => t.AgeTypeCode)
                .ForeignKey("dbo.ReferenceCode", t => t.PatronTypeCode, cascadeDelete: true)
                .Index(t => t.PatronTypeCode)
                .Index(t => t.AgeTypeCode);
            
            CreateTable(
                "dbo.PatronTypeGradeType",
                c => new
                    {
                        PatronTypeGradeTypeId = c.Int(nullable: false, identity: true),
                        PatronTypeCode = c.String(nullable: false, maxLength: 128),
                        GradeTypeCode = c.String(nullable: false, maxLength: 128),
                        DisplayOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PatronTypeGradeTypeId)
                .ForeignKey("dbo.ReferenceCode", t => t.GradeTypeCode)
                .ForeignKey("dbo.ReferenceCode", t => t.PatronTypeCode, cascadeDelete: true)
                .Index(t => t.PatronTypeCode)
                .Index(t => t.GradeTypeCode);
            
            CreateTable(
                "dbo.RegistrationPrice",
                c => new
                    {
                        RegistrationPriceId = c.Int(nullable: false, identity: true),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DiscountedAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        EventId = c.Int(nullable: false),
                        PatronTypeCode = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.RegistrationPriceId)
                .ForeignKey("dbo.Event", t => t.EventId, cascadeDelete: true)
                .ForeignKey("dbo.ReferenceCode", t => t.PatronTypeCode, cascadeDelete: true)
                .Index(t => t.EventId)
                .Index(t => t.PatronTypeCode);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RegistrationPrice", "PatronTypeCode", "dbo.ReferenceCode");
            DropForeignKey("dbo.RegistrationPrice", "EventId", "dbo.Event");
            DropForeignKey("dbo.PatronTypeGradeType", "PatronTypeCode", "dbo.ReferenceCode");
            DropForeignKey("dbo.PatronTypeGradeType", "GradeTypeCode", "dbo.ReferenceCode");
            DropForeignKey("dbo.PatronTypeAgeType", "PatronTypeCode", "dbo.ReferenceCode");
            DropForeignKey("dbo.PatronTypeAgeType", "AgeTypeCode", "dbo.ReferenceCode");
            DropForeignKey("dbo.Patron", "SchoolId", "dbo.School");
            DropForeignKey("dbo.School", "CountyId", "dbo.County");
            DropForeignKey("dbo.Patron", "GradeTypeCode", "dbo.ReferenceCode");
            DropForeignKey("dbo.Patron", "RegistrationId", "dbo.Registration");
            DropForeignKey("dbo.Registration", "StatusCode", "dbo.ReferenceCode");
            DropForeignKey("dbo.Registration", "EventId", "dbo.Event");
            DropForeignKey("dbo.Patron", "AgeTypeCode", "dbo.ReferenceCode");
            DropForeignKey("dbo.EventTypePatronType", "PatronTypeCode", "dbo.ReferenceCode");
            DropForeignKey("dbo.EventTypePatronType", "EventTypeCode", "dbo.EventType");
            DropForeignKey("dbo.Event", "EventTypeCode", "dbo.EventType");
            DropForeignKey("dbo.County", "StateCode", "dbo.State");
            DropIndex("dbo.RegistrationPrice", new[] { "PatronTypeCode" });
            DropIndex("dbo.RegistrationPrice", new[] { "EventId" });
            DropIndex("dbo.PatronTypeGradeType", new[] { "GradeTypeCode" });
            DropIndex("dbo.PatronTypeGradeType", new[] { "PatronTypeCode" });
            DropIndex("dbo.PatronTypeAgeType", new[] { "AgeTypeCode" });
            DropIndex("dbo.PatronTypeAgeType", new[] { "PatronTypeCode" });
            DropIndex("dbo.School", new[] { "CountyId" });
            DropIndex("dbo.Registration", new[] { "StatusCode" });
            DropIndex("dbo.Registration", new[] { "EventId" });
            DropIndex("dbo.Patron", new[] { "SchoolId" });
            DropIndex("dbo.Patron", new[] { "GradeTypeCode" });
            DropIndex("dbo.Patron", new[] { "AgeTypeCode" });
            DropIndex("dbo.Patron", new[] { "RegistrationId" });
            DropIndex("dbo.EventTypePatronType", new[] { "PatronTypeCode" });
            DropIndex("dbo.EventTypePatronType", new[] { "EventTypeCode" });
            DropIndex("dbo.Event", new[] { "EventTypeCode" });
            DropIndex("dbo.County", new[] { "StateCode" });
            DropTable("dbo.RegistrationPrice");
            DropTable("dbo.PatronTypeGradeType");
            DropTable("dbo.PatronTypeAgeType");
            DropTable("dbo.School");
            DropTable("dbo.Registration");
            DropTable("dbo.Patron");
            DropTable("dbo.ReferenceCode");
            DropTable("dbo.EventTypePatronType");
            DropTable("dbo.EventType");
            DropTable("dbo.Event");
            DropTable("dbo.State");
            DropTable("dbo.County");
        }
    }
}
