using System.Linq;
using Masti.Entities.Model;

namespace Masti.DataAccess.Repositories
{
    public class LookupTypeRepository: ILookupTypeRepository
    {
        private readonly MastiContext _mastiContext;

        public LookupTypeRepository(MastiContext mastiContext)
        {
            _mastiContext = mastiContext;
        }

        public virtual T GetById<T>(string id) where T : LookupType
        {
            return _mastiContext.LookupTypes.Find(id) as T;
        }

        public virtual IQueryable<T> GetAll<T>() where T : LookupType
        {
            return _mastiContext.LookupTypes.OfType<T>();
        }
    }
}