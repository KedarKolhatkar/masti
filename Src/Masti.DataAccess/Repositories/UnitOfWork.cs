﻿using System;
using System.Diagnostics.Contracts;
using Masti.Entities.Model;

namespace Masti.DataAccess.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        //private readonly MastiContext _context;
        //public IRepository<Event> EventRepository { get; private set; }
        //public IRepository<Registration> RegistrationRepository { get; private set; }
        //public IRepository<School> SchoolRepository { get; private set; }
        //public ILookupTypeRepository LookupTypeRepository { get; private set; }

        //public UnitOfWork(MastiContext context, IRepository<Event> eventRepository, IRepository<Registration> registrationRepository, 
        //    IRepository<School> schoolRepository, ILookupTypeRepository lookupTypeRepository)
        //{
        //    Contract.Requires<ArgumentNullException>(context != null, "context is null");
        //    Contract.Requires<ArgumentNullException>(eventRepository != null, "eventRepository is null");
        //    Contract.Requires<ArgumentNullException>(registrationRepository != null, "registrationRepository is null");
        //    Contract.Requires<ArgumentNullException>(schoolRepository != null, "schoolRepository is null");
        //    Contract.Requires<ArgumentNullException>(lookupTypeRepository != null, "lookupTypeRepository is null");

        //    _context = context;
        //    EventRepository = eventRepository;
        //    RegistrationRepository = registrationRepository;
        //    SchoolRepository = schoolRepository;
        //    LookupTypeRepository = lookupTypeRepository;
        //}

        //public void Save()
        //{
        //    _context.SaveChanges();
        //}

        //private bool _disposed;

        //public virtual void Dispose(bool disposing)
        //{
        //    if (!_disposed)
        //    {
        //        if (disposing)
        //        {
        //            _context.Dispose();
        //        }
        //    }
        //    _disposed = true;
        //}

        public void Dispose()
        {
            //Dispose(true);
            //GC.SuppressFinalize(this);
        }
    }
}
