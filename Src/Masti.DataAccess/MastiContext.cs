using System.Data.Entity;
using System.Reflection;
using Masti.Entities.Model;

namespace Masti.DataAccess
{
    public class MastiContext : DbContext
    {
        // Your context has been configured to use a 'MastiEntities' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Masti.DataAccess.Model.MastiEntities' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'MastiEntities' 
        // connection string in the application configuration file.
        public MastiContext()
            : base("name=MastiEntities")
        {
            ///////////////////////////////////////////////////////////////////////////////////////////////////
            // THIS IS A WORKAROUND for EF 6.0 issue. See http://pinter.org/?p=2374
            ///////////////////////////////////////////////////////////////////////////////////////////////////
            
            var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);

            // END OF WORKAROUND //
            ///////////////////////////////////////////////////////////////////////////////////////////////////

            Database.SetInitializer<MastiContext>(null);
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<LookupType> LookupTypes { get; set; }
        public virtual DbSet<EventType> EventTypes { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<County> Counties { get; set; }
        public virtual DbSet<School> Schools { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<Registration> Registrations { get; set; }
        public virtual DbSet<Patron> Patrons { get; set; }
        public virtual DbSet<EventTypePatronType> EventTypePatronTypes { get; set; }
        public virtual DbSet<PatronTypeGradeType> PatronTypeGradeTypes { get; set; }
        public virtual DbSet<PatronTypeAgeType> PatronTypeAgeTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }

}