﻿using Masti.Entities.Model;

namespace Masti.DataAccess
{
    public interface IEntityRepository<T> : IRepository<T, int> where T : BaseEntity
    {
    }
}