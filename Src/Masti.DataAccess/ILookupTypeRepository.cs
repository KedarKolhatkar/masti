﻿using System.Linq;
using Masti.Entities.Model;

namespace Masti.DataAccess
{
    public interface ILookupTypeRepository
    {
        T GetById<T>(string id) where T : LookupType;
        IQueryable<T> GetAll<T>() where T : LookupType;
    }
}