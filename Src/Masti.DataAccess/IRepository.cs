﻿using System.Linq;
using Masti.Entities.Model;

namespace Masti.DataAccess
{
    public interface IRepository<T, in TKey> where T : BaseEntity
    {
        T GetById(TKey id);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        IQueryable<T> GetAll();
    }
}