﻿SET IDENTITY_INSERT [Event] ON

INSERT [dbo].[Event] ([EventId], [Name], [Description], [EventTypeCode], [EventDate], 
[RegistrationStartDate], [RegistrationEndDate], [VenueName], [VenueAddress], [EarlyBirdOn],
[HomePageURL]) 
VALUES (4, N'Jr. Spelling Bee - MastiSpell 2017', N'MastiFest Event for 2017', N'MASTISPELL', CAST(N'2017-06-10 00:00:00.000' AS DateTime), 
CAST(N'2016-10-29 00:00:00.000' AS DateTime), CAST(N'2017-06-11 00:00:00.000' AS DateTime), 
N'Stone Hill Middle School', N'23415 Evergreen Ridge Drive, Ashburn VA, 20148', 1,
'http://www.mastispell.com')

SET IDENTITY_INSERT [Event] OFF

SET IDENTITY_INSERT [RegistrationPrice] ON

INSERT INTO [dbo].[RegistrationPrice]
([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate])
VALUES
(13, 4, 'Parent', 20.00, 40.00, '10/29/2016', '06/11/2017')

INSERT INTO [dbo].[RegistrationPrice]
([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate])
VALUES
(14, 4, 'JrBeeSpeller', 80.00, 100.00, '10/29/2016', '06/11/2017')

INSERT INTO [dbo].[RegistrationPrice]
([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate])
VALUES
(15, 4, 'KidSpectator', 10.00, 20.00, '10/29/2016', '06/11/2017')

INSERT INTO [dbo].[RegistrationPrice]
([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate])
VALUES
(16, 4, 'OtherSpectator', 20.00, 40.00, '10/29/2016', '06/11/2017')

SET IDENTITY_INSERT [RegistrationPrice] OFF

/* Correct state spelling */
update State
set Name = 'Connecticut'
where StateCode = 'CT'

