using System;
using Masti.DataAccess;
using Masti.DataAccess.Repositories;
using Masti.Entities.Model;
using Masti.UnitTests.Masti.DataAccess.Builders;
using NUnit.Framework;

namespace Masti.UnitTests.Masti.DataAccess.RepositoryTests
{
    [TestFixture]
    public class RegistrationRepositoryTests
    {
        private IEntityRepository<Registration> _registrationRepository;
        private MastiContext _mastiContext;

        [SetUp]
        public void SetUp()
        {
            _mastiContext = new MastiContext();
            _registrationRepository = new EntityRepository<Registration>(_mastiContext);
        }

        [Test]
        [Category("Database")]
        public void TestAddRegistration()
        {
            var registration = new RegistrationBuilder()
                .WithPatron(new ParentBuilder().Build())
                .Build();

            _registrationRepository.Add(registration);
            _mastiContext.SaveChanges();
            Assert.True(registration.Id > 0);
        }

        [Test]
        [Category("Database")]
        public void TestAddRegistrationWithSpellerWithOtherSchool()
        {
            var registration = new RegistrationBuilder()
                .WithPatron(new JrBeeSpllerBuilder().WithOtherSchool("my other school").Build())
                .Build();

            _registrationRepository.Add(registration);
            _mastiContext.SaveChanges();
            Assert.True(registration.Id > 0);
        }

        [Test]
        [Category("Database")]
        public void TestFindRegistration()
        {
            var registration = _registrationRepository.GetById(1);
            Assert.NotNull(registration);
            Assert.AreEqual(1, registration.Id);
        }

        [Test]
        [Category("Database")]
        public void TestGetRegistrationById()
        {
            // 1000 is assumed to be not existing in the database
            Assert.Null(_registrationRepository.GetById(1000));
        }

        [Test]
        [Category("Database")]
        public void TestUpdateRegistration()
        {
            var registration = _registrationRepository.GetById(1);
            registration.StatusCode = RegistrationStatus.Cancelled;
            registration.RefundedDate = DateTime.Parse("10/20/2014");
            _registrationRepository.Update(registration);
            _mastiContext.SaveChanges();
        }
    }
}
