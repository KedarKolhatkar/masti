using System.Linq;
using Masti.DataAccess;
using Masti.DataAccess.Repositories;
using Masti.Entities.Model;
using NUnit.Framework;

namespace Masti.UnitTests.Masti.DataAccess.RepositoryTests
{
    [TestFixture]
    public class SchoolRepositoryTests
    {
        private IEntityRepository<School> _schoolRepository; 

        [SetUp]
        public void SetUp()
        {
            _schoolRepository = new EntityRepository<School>(new MastiContext());
        }

        [Test]
        [Category("Database")]
        public void TestGetSchools()
        {
            var schools = _schoolRepository.GetAll();
            Assert.NotNull(schools);
            Assert.True(schools.Any());
        }
    }
}
