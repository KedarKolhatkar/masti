﻿using System.Linq;
using Masti.DataAccess;
using Masti.DataAccess.Repositories;
using Masti.Entities.Model;
using NUnit.Framework;

namespace Masti.UnitTests.Masti.DataAccess.RepositoryTests
{
    [TestFixture]
    public class PatronTyppeGradeTypeRepositoryTests
    {
        private IEntityRepository<PatronTypeGradeType> _pattronTypeGradeTypeRepository;

        [SetUp]
        public void SetUp()
        {
           _pattronTypeGradeTypeRepository = new EntityRepository<PatronTypeGradeType>(new MastiContext());
        }

        [Test]
        [Category("Database")]
        public void TestFindGradeType()
        {
            var gradeK = _pattronTypeGradeTypeRepository.GetAll().FirstOrDefault(t => t.PatronTypeCode == "JrBeeSpeller"
                                                                                      && t.GradeTypeCode == "GRADE_K");

            Assert.NotNull(gradeK);
        }
    }
}
