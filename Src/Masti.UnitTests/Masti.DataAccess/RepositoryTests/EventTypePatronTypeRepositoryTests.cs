﻿using System.Linq;
using Masti.DataAccess;
using Masti.DataAccess.Repositories;
using Masti.Entities.Model;
using NUnit.Framework;

namespace Masti.UnitTests.Masti.DataAccess.RepositoryTests
{
    [TestFixture]
    public class EventTypePatronTypeRepositoryTests
    {
        private IEntityRepository<EventTypePatronType> _eventTypePatronTypeRepository;

        [SetUp]
        public void SetUp()
        {
           _eventTypePatronTypeRepository = new EntityRepository<EventTypePatronType>(new MastiContext());
        }

        [Test]
        [Category("Database")]
        public void TestFindPatronTypesForMASTISPELLEvent()
        {
            var list = _eventTypePatronTypeRepository.GetAll().Where(t => t.EventTypeCode == "MASTISPELL").ToList<EventTypePatronType>();
            Assert.AreEqual(5, list.Count);

            Assert.True(list.Any(t => t.PatronTypeCode == "JrBeeSpeller"));
            Assert.True(list.Any(t => t.PatronTypeCode == "Parent"));
            Assert.True(list.Any(t => t.PatronTypeCode == "Infant"));
            Assert.True(list.Any(t => t.PatronTypeCode == "KidSpectator"));
            Assert.True(list.Any(t => t.PatronTypeCode == "OtherSpectator"));
        }

        [Test]
        [Category("Database")]
        public void TestFindPatronTypesForMASTIFESETEvent()
        {
            var list = _eventTypePatronTypeRepository.GetAll().Where(t=>
                t.EventTypeCode == "MASTIFEST")  .ToList<EventTypePatronType>();
            Assert.AreEqual(4, list.Count);

            Assert.True(list.Any(t => t.PatronTypeCode == "Adult"));
            Assert.True(list.Any(t => t.PatronTypeCode == "Youth"));
            Assert.True(list.Any(t => t.PatronTypeCode == "Child"));
            Assert.True(list.Any(t => t.PatronTypeCode == "Infant-1"));
        }

    }
}
