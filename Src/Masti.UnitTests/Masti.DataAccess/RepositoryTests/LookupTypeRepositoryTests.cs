﻿using System.Linq;
using Masti.DataAccess;
using Masti.DataAccess.Repositories;
using Masti.Entities.Model;
using NUnit.Framework;

namespace Masti.UnitTests.Masti.DataAccess.RepositoryTests
{
    [TestFixture]
    public class LookupTypeRepositoryTests
    {
        private ILookupTypeRepository _lookupTypeRepository;

        [SetUp]
        public void SetUp()
        {
           _lookupTypeRepository = new LookupTypeRepository(new MastiContext());
        }

        [Test]
        [Category("Database")]
        public void TestFindGradeType()
        {
            var gradeK = _lookupTypeRepository.GetById<GradeType>("GRADE_K");
            Assert.NotNull(gradeK);
            Assert.AreEqual("K-Level", gradeK.Description);
        }

        [Test]
        [Category("Database")]
        public void TestFindPatronType()
        {
            var parent = _lookupTypeRepository.GetById<PatronType>("PARENT");
            Assert.NotNull(parent);
            Assert.AreEqual("Parent/Guardian", parent.Description);
        }

        [Test]
        [Category("Database")]
        public void TestGetAllPatronTypes()
        {
            var patronTypes = _lookupTypeRepository.GetAll<PatronType>();
            Assert.NotNull(patronTypes);
            Assert.True(patronTypes.Any());
        }

        [Test]
        [Category("Database")]
        public void TestGetAllGradeTypes()
        {
            var gradeTypes = _lookupTypeRepository.GetAll<GradeType>();
            Assert.NotNull(gradeTypes);
            Assert.True(gradeTypes.Any());
        }
    }
}
