﻿using System;
using Masti.DataAccess;
using Masti.DataAccess.Repositories;
using Masti.Entities.Model;
using Masti.UnitTests.Masti.DataAccess.Builders;
using NUnit.Framework;

namespace Masti.UnitTests.Masti.DataAccess.RepositoryTests
{
    [TestFixture]
    public class EventRepositoryTests
    {
        private MastiContext _mastiContext;
        private IEntityRepository<Event> _eventRepository; 

        [SetUp]
        public void SetUp()
        {
            _mastiContext = new MastiContext();
            _eventRepository = new EntityRepository<Event>(_mastiContext);
        }

        [Test]
        [Category("Database")]
        public void TestInsertEventWithNullEntity()
        {
            var argumentNullException = Assert.Throws<ArgumentNullException>(
                () => _eventRepository.Add(null));
            Assert.NotNull(argumentNullException);
        }

        [Test]
        [Category("Database")]
        public void TestInsertEvent()
        {
            var @event = new EventBuilder().WithEventTypeCode("MASTISPELL").Build();
            _eventRepository.Add(@event);
            _mastiContext.SaveChanges();
            Assert.True(@event.Id > 0);
        }

        [Test]
        [Category("Database")]
        public void TestFindEvent()
        {
            var @event = _eventRepository.GetById(1);
            Assert.NotNull(@event);
            Assert.AreEqual("MASTISPELL", @event.EventTypeCode);
        }

        [Test]
        [Category("Database")]
        public void TestUpdateEvent()
        {
            var @event = _eventRepository.GetById(1);
            @event.Name = "Mastispell - Changed";
            _eventRepository.Update(@event);
            _mastiContext.SaveChanges();
        }
    }
}
