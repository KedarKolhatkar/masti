using System;
using Masti.Entities.Model;

namespace Masti.UnitTests.Masti.DataAccess.Builders
{
    public class RegistrationPriceBuilder
    {
        private decimal _amount = 20.00M;
        private decimal _discountedAmount = 15.00M;
        private int _id = 0;

        private PatronType _patronType = new PatronType
        {
            Code = "PARENT",
            Description = "Parent/Guardian",
            DisplayOrder = 2
        };

        private int _eventId = 1;
        private DateTime _endDate = DateTime.Parse("01/01/1965");

        public RegistrationPrice Build()
        {
            return new RegistrationPrice
            {
                Id = _id,
                Amount = _amount,
                DiscountedAmount = _discountedAmount,
                PatronType = _patronType,
                EventId = _eventId,
                EndDate = _endDate
            };
        }

        public RegistrationPriceBuilder WithEventId(int eventId)
        {
            _eventId = eventId;
            return this;
        }

        public RegistrationPriceBuilder WithAmount(decimal amount)
        {
            _amount = amount;
            return this;
        }

        public RegistrationPriceBuilder WithDiscountedAmount(decimal discountedAmount)
        {
            _discountedAmount = discountedAmount;
            return this;
        }

        public RegistrationPriceBuilder WithId(int id)
        {
            _id = id;
            return this;
        }

        public RegistrationPriceBuilder WithPatronType(string patronTypeCode)
        {
            _patronType = new PatronType {Code = patronTypeCode};
            return this;
        }

        public RegistrationPriceBuilder WithEndDate(DateTime endDate)
        {
            _endDate = endDate;
            return this;
        }
    }
}