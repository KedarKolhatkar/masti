﻿using System;
using System.Collections.Generic;
using Masti.Entities.Model;

namespace Masti.UnitTests.Masti.DataAccess.Builders
{
    public class RegistrationBuilder
    {
        private DateTime _datePaymentInitiated = DateTime.Parse("11/02/2014 06:00 AM");
        private DateTime _dateRegistered = DateTime.Parse("11/02/2014 06:01 AM");
        private DateTime _datePaymentCompleted = DateTime.Parse("11/02/2014 06:00 AM");
        private decimal _amount = 100.00M;
        private string _statusCode = RegistrationStatus.Completed;
        private string _gwOrderId = "1234567890";
        private int _eventId = 1;
        private readonly IList<Patron> _patrons = new List<Patron>();

        public Registration Build()
        {
            return new Registration
            {
                RegisteredDate = _dateRegistered,
                PaymentInitiationDate = _datePaymentInitiated,
                PaymentCompleteDate = _datePaymentCompleted,
                Amount = _amount,
                EventId =  _eventId,
                PmtVendorOrderId = _gwOrderId,
                StatusCode = _statusCode,
                RefundedDate = null,
                Patrons = _patrons
            };
        }

        public RegistrationBuilder WithDateReigstered(DateTime dateRegistered)
        {
            _dateRegistered = dateRegistered;
            return this;
        }

        public RegistrationBuilder WithAmount(decimal amount)
        {
            _amount = amount;
            return this;
        }

// ReSharper disable InconsistentNaming
        public RegistrationBuilder WithGWOrderId(string gwOrderId)
// ReSharper restore InconsistentNaming
        {
            _gwOrderId = gwOrderId;
            return this;
        }

        public RegistrationBuilder WithStatus(string statusCode)
        {
            _statusCode = statusCode;
            return this;
        }

        public RegistrationBuilder WithEventId(int eventId)
        {
            _eventId = eventId;
            return this;
        }

        public RegistrationBuilder WithPatron(Patron patron)
        {
            _patrons.Add(patron);
            return this;
        }

    }
}
