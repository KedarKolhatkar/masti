﻿using Masti.Entities.Model;

namespace Masti.UnitTests.Masti.DataAccess.Builders
{
    public class ParentBuilder
    {
        private const string _firstName = "John";
        private const string _middleName = "James";
        private const string _lastName = "Doe";
        private const string _email = "john.doe@gmail.com";
        private const decimal _amount = 20.00M;
        private const string _ageTypeCode = "AGE_18_PLUS";

        public Parent Build()
        {
            return new Parent
            {
                FirstName = _firstName,
                MiddleName = _middleName,
                LastName = _lastName,
                Email = _email,
                AgeTypeCode = _ageTypeCode,
                Amount = _amount
            };
        }
    }
}
