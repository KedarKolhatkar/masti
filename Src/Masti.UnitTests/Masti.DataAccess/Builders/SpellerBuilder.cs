﻿using Masti.Entities.Model;

namespace Masti.UnitTests.Masti.DataAccess.Builders
{
    public class JrBeeSpllerBuilder
    {
        private const string _firstName = "Jenna";
        private const string _middleName = "John";
        private const string _lastName = "Doe";
        private const string _email = "jenna.doe@gmail.com";
        private const decimal _amount = 80.00M;
        private const string _ageTypeCode = "AGE_8";
        private string _otherSchool = "my other school";
        private int? _schoolId = 1;

        public JrBeeSpeller Build()
        {
            return new JrBeeSpeller
            {
                FirstName = _firstName,
                MiddleName = _middleName,
                LastName = _lastName,
                Email = _email,
                AgeTypeCode = _ageTypeCode,
                Amount = _amount,
                SchoolId = _schoolId,
                OtherSchool = _otherSchool
            };
        }

        public JrBeeSpllerBuilder WithOtherSchool(string otherSchool)
        {
            _otherSchool = otherSchool;
            _schoolId = (int?)null;
            return this;
        }
    }
}
