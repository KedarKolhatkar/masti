﻿using System;
using Masti.Entities.Model;

namespace Masti.UnitTests.Masti.DataAccess.Builders
{
    public class EventBuilder
    {
        private int _id = 0;
        private const string _name = "Test Event 1";
        private const string _description = "Event created for unit testing";
        private string _eventTypeCode = "MASTISPELL";
        private readonly DateTime _eventDate = DateTime.Parse("11/01/2014");
        private readonly DateTime _registrationStartDate = DateTime.Parse("01/01/2014");
        private readonly DateTime _registrationEndDate = DateTime.Parse("11/01/2014");
        private const string _venueName = "StoneBridge High";
        private const string _venueAddress = "123 Main Street, Ashburn VA 20148";
        private const bool _earlyBirdOn = true;

        public Event Build()
        {
            return new Event
            {
                Id = _id,
                Name = _name,
                Description = _description,
                EventDate = _eventDate,
                EventTypeCode = _eventTypeCode,
                RegistrationStartDate = _registrationStartDate,
                RegistrationEndDate = _registrationEndDate,
                VenueName = _venueName,
                VenueAddress = _venueAddress,
                EarlyBirdOn = _earlyBirdOn
            };
        }

        public EventBuilder WithEventTypeCode(string eventTypeCode)
        {
            _eventTypeCode = eventTypeCode;
            return this;
        }

        public EventBuilder WithId(int id)
        {
            _id = id;
            return this;
        }
    
    }
}
