﻿using System;
using System.Collections.Generic;
using Masti.DataAccess;
using Masti.DTO;
using Masti.Entities.Model;
using Masti.Services;
using Masti.Services.Impl;
using Masti.Services.Infrastructure;
using Masti.Services.Translators;
using Moq;
using NUnit.Framework;

namespace Masti.UnitTests.Masti.Services
{
    public class RegistrationServiceTests
    {
        private Mock<IEntityRepository<Registration>> _registrationRepositoryMock;
        private IRegistrationService _registrationService;
        private Mock<Registration> _registrationMock;
        private Mock<RegistrationDTO> _registrationDTOMock;
        private Mock<IPatronTranslator> _patronTranslatorMock;
        private Mock<IRegistrationTranslator> _registrationTranslatorMock;
         
        [SetUp]
        public void SetUp()
        {
            _registrationRepositoryMock = new Mock<IEntityRepository<Registration>>();
            _registrationMock = new Mock<Registration>();
            _registrationDTOMock = new Mock<RegistrationDTO>();
            _patronTranslatorMock = new Mock<IPatronTranslator>();
            _registrationTranslatorMock = new Mock<IRegistrationTranslator>();

            _registrationService = new RegistrationService(_registrationRepositoryMock.Object, _patronTranslatorMock.Object,
                _registrationTranslatorMock.Object);
        }

        [Test]
        public void TestSaveOnPaymentInitiation()
        {
            var registration = _registrationMock.Object;
            var registrationDTO = _registrationDTOMock.Object;
            registrationDTO.Patrons = new List<PatronDTO>();

            _registrationTranslatorMock.Setup(c => c.TranslateToRegistration(registrationDTO)).Returns(registration);
            _registrationRepositoryMock.Setup(c => c.Add(registration));

            //_registrationMock.SetupProperty(t => t.Status);
            //_registrationMock.SetupProperty(t => t.RegisteredDate);


            registrationDTO.InvoiceNumber = Guid.NewGuid();

            _registrationService.SaveOnPaymentInitiation(registrationDTO);
            _registrationTranslatorMock.VerifyAll();
            _registrationRepositoryMock.VerifyAll();

            Assert.AreEqual(RegistrationStatus.Initiated, registration.StatusCode);
            Assert.Null(registration.PaymentCompleteDate);
            Assert.NotNull(registration.PaymentInitiationDate);

            Assert.True(registration.PaymentInitiationDate > DateTime.Now.AddMinutes(-2));
       }

        [Test]
        public void TestSaveOnPaymentInitiation_NullRegistration_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _registrationService.SaveOnPaymentInitiation(null));
        }

        [Test]
        public void TestSaveOnPaymentInitiation_ZeroRegistrationId_ThrowsException()
        {
            var registrationDTO = new RegistrationDTO() {};
            var exception = Assert.Throws<ApplicationException>(() => _registrationService.SaveOnPaymentInitiation(registrationDTO));
            Assert.AreEqual("registrationDTO.InvoiceNumber cannot be zero", exception.Message);
        }

        [Test]
        public void TestSaveOnPaymentCompletion_throwsExceptionWhenPmtVendorIdBlank()
        {
            Assert.Throws<ApplicationException>(() => _registrationService.SaveOnPaymentCompletion(0, ""),
                "pmtVendorOrderId is cannot be blank or null");
        }

        [Test]
        public void TestSaveOnPaymentCompletion_throwsExceptionWhenRegistrationIdNotFound()
        {
            Assert.Throws<ApplicationException>(() => _registrationService.SaveOnPaymentCompletion(0, "paypal-1"),
                "Registration with id=0 not found");
        }

        [Test]
        public void TestSaveOnPaymentCompletion()
        {
            var reg = new Registration() { Id = 1 };
            _registrationRepositoryMock.Setup(t => t.GetById(1)).Returns(reg);
            _registrationRepositoryMock.Setup(t => t.Update(reg));
            _registrationService.SaveOnPaymentCompletion(1, "paypal-1");

            _registrationRepositoryMock.VerifyAll();
        }

        [Test]
        public void TestSaveOnPaymentCancellation()
        {
            var reg = new Registration() { Id = 1 };
            _registrationRepositoryMock.Setup(t => t.GetById(1)).Returns(reg);
            _registrationRepositoryMock.Setup(t => t.Update(reg));
            _registrationService.SaveOnPaymentCancellation(1);

            _registrationRepositoryMock.VerifyAll();
        }

    }
}