﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;
using NUnit.Framework;
using Masti.Services.Translators;
using Masti.Entities.Model;
using Masti.UnitTests.Masti.Services.Builders;

namespace Masti.UnitTests.Masti.Services.Translators
{
    [TestFixture]
    public class PatronTranslatorTests
    {
        private PatronTranslator _translator;

        [SetUp]
        public void SetUp()
        {
            _translator = new PatronTranslator();
        }

        [Test]
        public void Translate()
        {
            Assert.That(_translator.TranslateToPatron(new PatronDTOBuilder().WithPatronTypeCode("Adult").Build())
                is Adult);
            Assert.That(_translator.TranslateToPatron(new PatronDTOBuilder().WithPatronTypeCode("Child").Build())
                is Child);
            Assert.That(_translator.TranslateToPatron(new PatronDTOBuilder().WithPatronTypeCode("Infant").Build())
                is Infant);
            Assert.That(_translator.TranslateToPatron(new PatronDTOBuilder().WithPatronTypeCode("Infant-1").Build())
                is Infant);
            Assert.That(_translator.TranslateToPatron(new PatronDTOBuilder().WithPatronTypeCode("Youth").Build())
                is Youth);
        }

        [Test]
        public void TranslateForOtherSchool()
        {
            var patronDTO = new PatronDTOBuilder()
                .WithPatronTypeCode("JrBeeSpeller")
                .WithHasOtherSchool(true)
                .WithOtherSchool("my other school").Build();
            JrBeeSpeller speller = _translator.TranslateToPatron(patronDTO) as JrBeeSpeller;

            Assert.Null(speller.SchoolId);
            Assert.AreEqual("my other school", speller.OtherSchool);

        }
    }
}
