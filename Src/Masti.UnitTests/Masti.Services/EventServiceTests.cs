﻿using System;
using System.Collections.Generic;
using System.Linq;
using Masti.DataAccess;
using Masti.Entities.Model;
using Masti.Services;
using Masti.Services.Impl;
using Masti.UnitTests.Masti.DataAccess.Builders;
using Moq;
using NUnit.Framework;

namespace Masti.UnitTests.Masti.Services
{
    [TestFixture]
    public class EventServiceTests
    {
        private Mock<IEntityRepository<Event>> _eventRepositoryMock;
        private IEventService _eventService;
        private Mock<IEntityRepository<EventTypePatronType>> _eventTypePatronTypeRepositoryMock;
        private Mock<IEntityRepository<RegistrationPrice>> _registrationPriceRepositoryMock;

        [SetUp]
        public void Setup()
        {
            _eventRepositoryMock = new Mock<IEntityRepository<Event>>();
            _registrationPriceRepositoryMock = new Mock<IEntityRepository<RegistrationPrice>>();
            _eventTypePatronTypeRepositoryMock = new Mock<IEntityRepository<EventTypePatronType>>();

            _eventService = new EventService(_eventRepositoryMock.Object, _registrationPriceRepositoryMock.Object,
                _eventTypePatronTypeRepositoryMock.Object);
        }

        [Test]
        public void TestGetEvents()
        {
            const int eventId = 100;
            _eventRepositoryMock.Setup(c => c.GetById(eventId)).Returns((new EventBuilder().WithId(eventId).Build()));
            _eventService.GetEvent(eventId);
            _eventRepositoryMock.VerifyAll();
        }

        [Test]
        public void TestGetEventRegistrationPrices()
        {
            const int eventId = 100;
            var registrationPriceList = new List<RegistrationPrice>
            {
                new RegistrationPriceBuilder().WithEventId(eventId).WithId(1).Build(),
                new RegistrationPriceBuilder().WithEventId(eventId).WithId(2).Build()
            };

            _registrationPriceRepositoryMock.Setup(c => c.GetAll()).Returns(registrationPriceList.AsQueryable);
            _eventService.GetEventRegistrationPrices(eventId);
            _registrationPriceRepositoryMock.VerifyAll();
        }

        [Test]
        public void TestGetRegistrationPricesPerPatronType1()
        {
            const int eventId = 100;

            var registrationPriceList = new List<RegistrationPrice>
            {
                new RegistrationPriceBuilder().WithEventId(eventId)
                    .WithId(1)
                    .WithPatronType(typeof (Speller).Name)
                    .Build(),
                new RegistrationPriceBuilder().WithEventId(eventId)
                    .WithId(2)
                    .WithPatronType(typeof (Parent).Name)
                    .Build()
            };

            _registrationPriceRepositoryMock.Setup(c => c.GetAll()).Returns(registrationPriceList.AsQueryable);
            var result = _eventService.GetRegistrationPrices(eventId);
            _registrationPriceRepositoryMock.VerifyAll();

            Assert.NotNull(result);
            Assert.True(result.Count == 2);
        }

        [Test]
        public void TestGetRegistrationPricesPerPatronType_NoDiscountedPrices()
        {
            const int eventId = 100;

            DateTime endDateBeforeToday = DateTime.Now.AddDays(-1);
            var registrationPriceList = new List<RegistrationPrice>
            {
                new RegistrationPriceBuilder().WithEventId(eventId)
                    .WithPatronType(typeof (Speller).Name)
                    .WithEndDate(endDateBeforeToday)
                    .WithAmount(50.00M)
                    .WithDiscountedAmount(30.00M)
                    .Build(),
                new RegistrationPriceBuilder().WithEventId(eventId)
                    .WithPatronType(typeof (Parent).Name)
                    .WithEndDate(endDateBeforeToday)
                    .WithAmount(15.00M)
                    .WithDiscountedAmount(10.00M)
                    .Build()
            };

            _registrationPriceRepositoryMock.Setup(c => c.GetAll()).Returns(registrationPriceList.AsQueryable);
            var result = _eventService.GetCurrentRegistrationPrices(eventId);
            _registrationPriceRepositoryMock.VerifyAll();

            Assert.NotNull(result);
            Assert.True(result.Count == 2);

            Assert.True(50.00M == result[typeof (Speller).Name]);
            Assert.True(15.00M == result[typeof (Parent).Name]);
        }

        [Test]
        public void TestGetRegistrationPricesPerPatronType_DiscountedPrices()
        {
            const int eventId = 100;

            DateTime endDateAfterToday = DateTime.Now.AddDays(1);
            var registrationPriceList = new List<RegistrationPrice>
            {
                new RegistrationPriceBuilder().WithEventId(eventId)
                    .WithPatronType(typeof (Speller).Name)
                    .WithEndDate(endDateAfterToday)
                    .WithAmount(50.00M)
                    .WithDiscountedAmount(30.00M)
                    .Build(),
                new RegistrationPriceBuilder().WithEventId(eventId)
                    .WithPatronType(typeof (Parent).Name)
                    .WithEndDate(endDateAfterToday)
                    .WithAmount(15.00M)
                    .WithDiscountedAmount(10.00M)
                    .Build()
            };

            _registrationPriceRepositoryMock.Setup(c => c.GetAll()).Returns(registrationPriceList.AsQueryable);
            var result = _eventService.GetCurrentRegistrationPrices(eventId);
            _registrationPriceRepositoryMock.VerifyAll();

            Assert.NotNull(result);
            Assert.True(result.Count == 2);

            Assert.True(30.00M == result[typeof (Speller).Name]);
            Assert.True(10.00M == result[typeof (Parent).Name]);
        }

        [Test]
        public void TestGetPatronTypes()
        {
            _eventRepositoryMock.Setup(t => t.GetById(1))
                .Returns(new EventBuilder().WithEventTypeCode("MASTISPELL").Build());

            IList<EventTypePatronType> patronTypeList = new List<EventTypePatronType>();
            patronTypeList.Add(new EventTypePatronType() { EventTypeCode = "MASTISPELL", PatronTypeCode = "JrBeeSpeller", DisplayOrder = 1, PatronType = new PatronType() });
            patronTypeList.Add(new EventTypePatronType() { EventTypeCode = "MASTISPELL", PatronTypeCode = "Parent", DisplayOrder = 2, PatronType = new PatronType()});
            _eventTypePatronTypeRepositoryMock.Setup(t => t.GetAll()).Returns(patronTypeList.AsQueryable);

            _eventService.GetPatronTypes(1);

            _eventRepositoryMock.VerifyAll();
            _eventTypePatronTypeRepositoryMock.VerifyAll();
        }
    }
}