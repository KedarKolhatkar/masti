﻿using System.Collections.Generic;
using System.Linq;
using Masti.DataAccess;
using Masti.Entities.Model;
using Masti.Services;
using Masti.Services.Impl;
using Moq;
using NUnit.Framework;

namespace Masti.UnitTests.Masti.Services
{
    [TestFixture]
    public class LookupServiceTests
    {
        private Mock<IEntityRepository<County>> _countyRepositoryMock;
        private ILookupService _lookupService;
        private Mock<ILookupTypeRepository> _lookupTypeRepositoryMock;
        private Mock<IEntityRepository<PatronTypeGradeType>> _patronTypeGradeTypeRepositoryMock;
        private Mock<IEntityRepository<School>> _schoolRepositoryMock;
        private Mock<IEntityRepository<State>> _stateRepositoryMock;
        private Mock<IEntityRepository<PatronTypeAgeType>> _patronTypeAgeTypeRepositoryMock;

        [SetUp]
        public void SetUp()
        {
            _lookupTypeRepositoryMock = new Mock<ILookupTypeRepository>();
            _schoolRepositoryMock = new Mock<IEntityRepository<School>>();
            _countyRepositoryMock = new Mock<IEntityRepository<County>>();
            _stateRepositoryMock = new Mock<IEntityRepository<State>>();
            _patronTypeGradeTypeRepositoryMock = new Mock<IEntityRepository<PatronTypeGradeType>>();
            _patronTypeAgeTypeRepositoryMock = new Mock<IEntityRepository<PatronTypeAgeType>>();

            _lookupService = new LookupService(_lookupTypeRepositoryMock.Object,
                _schoolRepositoryMock.Object, _countyRepositoryMock.Object,
                _stateRepositoryMock.Object, _patronTypeGradeTypeRepositoryMock.Object,
                _patronTypeAgeTypeRepositoryMock.Object);
        }

        [Test]
        public void TestGetGradeTypes()
        {
            _lookupTypeRepositoryMock.Setup(c => c.GetAll<GradeType>()).Returns(new List<GradeType>().AsQueryable);
            _lookupService.GetGradeTypes();
            _lookupTypeRepositoryMock.VerifyAll();
        }

        [Test]
        public void TestGetPatronTypes()
        {
            _lookupTypeRepositoryMock.Setup(c => c.GetAll<PatronType>()).Returns(new List<PatronType>().AsQueryable);
            _lookupService.GetPatronTypes();
            _lookupTypeRepositoryMock.VerifyAll();
        }

        [Test]
        public void TestGetStates()
        {
            _stateRepositoryMock.Setup(c => c.GetAll()).Returns(new List<State>().AsQueryable);
            _lookupService.GetStates();
            _stateRepositoryMock.VerifyAll();
        }

        [Test]
        public void TestGetCounties()
        {
            _countyRepositoryMock.Setup(c => c.GetAll()).Returns(new List<County>().AsQueryable);
            _lookupService.GetCounties();
            _countyRepositoryMock.VerifyAll();
        }

        [Test]
        public void TestGetSchools()
        {
            _schoolRepositoryMock.Setup(c => c.GetAll()).Returns(new List<School>().AsQueryable);
            _lookupService.GetSchools();
            _schoolRepositoryMock.VerifyAll();
        }

        [Test]
        public void TestGetPatronTypeGradeTypeDictionary()
        {
            var list = new List<PatronTypeGradeType>
            {
                new PatronTypeGradeType
                {
                    PatronTypeCode = "JrBeeSpeller",
                    GradeTypeCode = "GRADE_K",
                    GradeType = new GradeType {Code = "GRADE_K"}
                },
                new PatronTypeGradeType
                {
                    PatronTypeCode = "OlympiadSpeller",
                    GradeTypeCode = "GRADE_5",
                    GradeType = new GradeType {Code = "GRADE_5"}
                },
            };

            _patronTypeGradeTypeRepositoryMock.Setup(t => t.GetAll()).Returns(list.AsQueryable);

            var dic = _lookupService.GetPatronTypeGradeTypeDictionary();

            _patronTypeGradeTypeRepositoryMock.VerifyAll();
            Assert.NotNull(dic);
            Assert.True(dic.Count == 2);
            Assert.NotNull(dic["JrBeeSpeller"].FirstOrDefault());
// ReSharper disable PossibleNullReferenceException
            Assert.True(dic["JrBeeSpeller"].FirstOrDefault().Code == "GRADE_K");
// ReSharper restore PossibleNullReferenceException
        }

        [Test]
        public void TestGetPatronTypeAgeTypeDictionary()
        {
            var list = new List<PatronTypeAgeType>
            {
                new PatronTypeAgeType
                {
                    PatronTypeCode = "JrBeeSpeller",
                    AgeTypeCode = "AGE_4",
                    AgeType = new AgeType {Code = "AGE_4"}
                },
                new PatronTypeAgeType
                {
                    PatronTypeCode = "OlympiadSpeller",
                    AgeTypeCode = "AGE_9",
                    AgeType = new AgeType {Code = "AGE_9"}
                },
            };

            _patronTypeAgeTypeRepositoryMock.Setup(t => t.GetAll()).Returns(list.AsQueryable);

            var dic = _lookupService.GetPatronTypeAgeTypeDictionary();

            _patronTypeGradeTypeRepositoryMock.VerifyAll();
            Assert.NotNull(dic);
            Assert.True(dic.Count == 2);
            Assert.NotNull(dic["JrBeeSpeller"].FirstOrDefault());
// ReSharper disable PossibleNullReferenceException
            Assert.True(dic["JrBeeSpeller"].FirstOrDefault().Code == "AGE_4");
// ReSharper restore PossibleNullReferenceException
// ReSharper disable PossibleNullReferenceException
            Assert.True(dic["OlympiadSpeller"].FirstOrDefault().Code == "AGE_9");
// ReSharper restore PossibleNullReferenceException
        }
    
    }
}