﻿using Masti.DTO;

namespace Masti.UnitTests.Masti.Services.Builders
{
    public class PatronDTOBuilder
    {
        private string _patronTypeCode = "Adult"; 
        private string _firatName = "John";
        private string _middleName = "Martin";
        private string _lastName = "Smith";
        private bool _hasOtherSchool = false;
        private string _otherSchool = "my other school";

        public PatronDTO Build()
        {
            return new PatronDTO()
            {
                PatronTypeCode = _patronTypeCode,
                FirstName = _firatName,
                MiddleName = _middleName,
                LastName = _lastName,
                HasOtherSchool = _hasOtherSchool,
                OtherSchool = _otherSchool
            };
        }

        public PatronDTOBuilder WithPatronTypeCode(string patronTypeCode)
        {
            _patronTypeCode = patronTypeCode;
            return this;
        }

        public PatronDTOBuilder WithHasOtherSchool(bool hasOtherSchool)
        {
            _hasOtherSchool = hasOtherSchool;
            return this;
        }

        public PatronDTOBuilder WithOtherSchool(string otherSchool)
        {
            _otherSchool = otherSchool;
            return this;
        }
    }
}
