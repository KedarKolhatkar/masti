﻿using System;
using System.Linq;
using Masti.DataAccess;
using Masti.Entities.Model;
using NUnit.Framework;

namespace Masti.UnitTests.Masti.Entities
{
    [TestFixture]
    public class MastiContextXTests : IDisposable
    {
        private MastiContext _mastiContext;

        [SetUp]
        public void SetUp()
        {
            _mastiContext = new MastiContext();
        }

        [Test]
        [Category("Database")]
        public void Test_Get_States()
        {
            Assert.True(_mastiContext.States.Count() > 1);
        }

        [Test]
        [Category("Database")]
        public void Test_Get_All_Counties()
        {
            Assert.True(_mastiContext.Counties.Count() > 1);
        }

        [Test]
        [Category("Database")]
        public void Test_Get_LoudounConty()
        {
            var county = _mastiContext.Counties.SingleOrDefault(c => c.Name == "Loudoun");
            Assert.NotNull(county);
            Assert.AreEqual("Loudoun", county.Name);
            Assert.NotNull(county.State);
            Assert.AreEqual("VA", county.State.StateCode);
        }

        [Test]
        [Category("Database")]
        public void Test_Get_GradeType()
        {
            var gradeType = _mastiContext.LookupTypes.OfType<GradeType>().FirstOrDefault();
            Assert.NotNull(gradeType);
        }

        [Test]
        [Category("Database")]
        public void Test_Get_PatronType()
        {
            var patronType = _mastiContext.LookupTypes.OfType<PatronType>().FirstOrDefault();
            Assert.NotNull(patronType);
        }

        [Test]
        [Category("Database")]
        public void Test_Get_School()
        {
            var school = _mastiContext.Schools.FirstOrDefault();
            Assert.NotNull(school);
        }

        [Test]
        [Category("Database")]
        public void Test_Get_EventType()
        {
            var school = _mastiContext.EventTypes.FirstOrDefault();
            Assert.NotNull(school);
        }

        [Test]
        [Category("Database")]
        public void Test_Get_Events()
        {
            var @event = _mastiContext.Events.FirstOrDefault();
            Assert.NotNull(@event);
            Assert.NotNull(@event.EventDate);
        }

        [Test]
        [Category("Database")]
        public void Test_Get_Registrations()
        {
            var @event = _mastiContext.Events.FirstOrDefault();
            Assert.NotNull(@event);
            Assert.NotNull(@event.EventDate);
        }

        [Test]
        [Category("Database")]
        public void Test_Get_Speller()
        {
            var speller = _mastiContext.Patrons.OfType<JrBeeSpeller>().FirstOrDefault();
            Assert.NotNull(speller);
            // ReSharper disable once PossibleNullReferenceException
            Assert.NotNull(speller.Grade);
            Assert.NotNull(speller.School);
        }

        [Test]
        [Category("Database")]
        public void Test_Get_Parent()
        {
            var parent = _mastiContext.Patrons.OfType<Parent>().FirstOrDefault();
            Assert.NotNull(parent);
        }

        [Test]
        [Category("Database")]
        public void Test_Get_EventTypePatronType()
        {
            var patronTypes =
                _mastiContext.EventTypePatronTypes.Where(t => t.EventTypeCode == "MASTISPELL")
                    .Select(t => t.PatronType)
                    .ToList();
            Assert.NotNull(patronTypes);
            Assert.True(patronTypes.Any());
        }

        [Test]
        [Category("Database")]
        public void Test_Get_PatronTypeGradeType()
        {
            var gradeTypes =
                _mastiContext.PatronTypeGradeTypes.Where(t => t.PatronTypeCode == "JrBeeSpeller")
                    .Select(t => t.GradeType)
                    .ToList();
            Assert.NotNull(gradeTypes);
            Assert.True(gradeTypes.Any());
        }

        [Test]
        [Category("Database")]
        public void Test_Get_PatronTypeAgeType()
        {
            var gradeTypes =
                _mastiContext.PatronTypeAgeTypes.Where(t => t.PatronTypeCode == "JrBeeSpeller")
                    .Select(t => t.AgeType)
                    .ToList();
            Assert.NotNull(gradeTypes);
            Assert.True(gradeTypes.Any());
        }

        [Test]
        [Category("Database")]
        public void Test_Get_RegistrationStatuses()
        {
            Assert.True(_mastiContext.LookupTypes.OfType<RegistrationStatus>().Count(t => t.Code == "REGINIT") == 1);
        }

        public void Dispose()
        {
            _mastiContext.Dispose();
        }
    }
}