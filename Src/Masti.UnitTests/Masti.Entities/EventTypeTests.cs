﻿using NUnit.Framework;
using Masti.Entities.Model;

namespace Masti.UnitTests.Masti.Entities
{
    [TestFixture]
    public class EventTypeTests
    {
        [Test]
        public void Test_IsMastiSpellEvent()
        {
            var e = new EventType();
            e.EventTypeCode = "MASTISPELL";
            Assert.That(e.IsMastiSpellEvent());
        }

        [Test]
        public void Test_IsMastiFestEvent()
        {
            var e = new EventType();
            e.EventTypeCode = "MASTIFEST";
            Assert.That(e.IsMastiFestEvent());
        }

    }
}
