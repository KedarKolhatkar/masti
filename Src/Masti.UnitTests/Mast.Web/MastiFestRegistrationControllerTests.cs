﻿using System.Collections.Generic;
using Masti.Entities.Model;
using Masti.Services;
using Masti.Web.Controllers;
using Masti.Web.Infrasturcture;
using Moq;
using NUnit.Framework;
using Masti.UnitTests.Masti.DataAccess.Builders;

namespace Masti.UnitTests.Mast.Web
{
    [TestFixture]
    public class MastiFestRegistrationControllerTests
    {
        private Mock<IApplicationSettings> _appSettingsMock;
        private Mock<IEventService> _eventServiceMock;
        private Mock<IApplicationCache> _applicationCacheMock;
        private MastiFestRegistrationController _registrationController;

        [SetUp]
        public void SetUp()
        {
            _eventServiceMock = new Mock<IEventService>();
            _appSettingsMock = new Mock<IApplicationSettings>();
            _applicationCacheMock = new Mock<IApplicationCache>();
            _registrationController = new MastiFestRegistrationController( 
                _eventServiceMock.Object,  _appSettingsMock.Object, _applicationCacheMock.Object);
        }

        [Test]
        public void TestGetParticipantReferenceData()
        {
            _appSettingsMock.SetupGet(c => c.EventId).Returns(2);

            _eventServiceMock.Setup(c => c.GetPatronTypes(2)).Returns(new List<PatronType>());
            _eventServiceMock.Setup(c => c.GetCurrentRegistrationPrices(2)).Returns(new Dictionary<string, decimal>());

            Assert.NotNull(_registrationController.GetParticipantReferenceData());

            _appSettingsMock.VerifyAll();
            _eventServiceMock.VerifyAll();
        }

        [Test]
        public void TestGetMastiFestRegistrationModel()
        {
            _appSettingsMock.SetupGet(c => c.EventId).Returns(2);
            _eventServiceMock.Setup(c => c.GetEvent(2)).Returns(
                new EventBuilder().WithEventTypeCode("MASTIFEST").Build());
            _eventServiceMock.Setup(c => c.GetRegistrationPrices(2)).Returns(
                new Dictionary<string, RegistrationPrice>());

            var model = _registrationController.GetMastiFestRegistrationModel();
            Assert.NotNull(model);
            _eventServiceMock.VerifyAll();
            _appSettingsMock.VerifyAll();
        }

    }
}