﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Masti.DTO;
using Masti.Entities.Model;
using Masti.Services;
using Masti.Web.Controllers;
using Masti.Web.Infrasturcture;
using Masti.Web.Payment;
using Moq;
using NUnit.Framework;

namespace Masti.UnitTests.Mast.Web
{
    [TestFixture]
    public class PaymentControllerTests
    {
        private const string SCHEME = "http";
        private const string HOST = "localhost";
        private Mock<IApplicationSettings> _appSettingsMock;
        private Mock<IEventService> _eventServiceMock;
        private PaymentController _paymentController;
        private Mock<IPaypalPaymentAdaptor> _paypalPaymentAdaptorMock;
        private Mock<IRegistrationService> _registrationServiceMock;
        private Mock<IPaymentIdGenerator> _paymentIdGeneratorMock;
        private Mock<IApplicationCache> _applicationCacheMock;
        private Mock<UrlHelper> _urlHelperMock;

        [OneTimeSetUp]
        public void SetUp()
        {
            _eventServiceMock = new Mock<IEventService>();
            _registrationServiceMock = new Mock<IRegistrationService>();
            _appSettingsMock = new Mock<IApplicationSettings>();
            _paypalPaymentAdaptorMock = new Mock<IPaypalPaymentAdaptor>();
            _paymentIdGeneratorMock = new Mock<IPaymentIdGenerator>();
            _applicationCacheMock = new Mock<IApplicationCache>();

            _urlHelperMock = new Mock<UrlHelper>();

            _paymentController = new PaymentController(_registrationServiceMock.Object,
                _eventServiceMock.Object, _paypalPaymentAdaptorMock.Object, _paymentIdGeneratorMock.Object,
                _appSettingsMock.Object, _applicationCacheMock.Object);

            _paymentController.Url = _urlHelperMock.Object;
        }

        [Test]
        public void intialize_payment_successfully()
        {
            var httpContext = MvcMockHelpers.MockHttpContext("http://localhost/InitiatePayment");
            _paymentController.SetMockControllerContext(httpContext, null, RouteTable.Routes);

            const int paymentId = 23;
            _paymentIdGeneratorMock.Setup(c => c.Generate()).Returns(paymentId);
            var @event = new Event
            {
                Id = 1,
                Name = "MastiSpell"
            };

            // Setup objects for a call to CreatePayment
            _applicationCacheMock.Setup(c => c.Get(MastiBaseController.CACHE_KEY_EVENT))
                .Returns(@event);

            // Assumed event is cache so service call is not required. May write another
            // test for no cache.
            //_eventServiceMock.Setup(c => c.GetEvent(1)).Returns(@event);
            var registrationDTO = new RegistrationDTO();

            const string completeUrl = SCHEME + "://" + HOST + "/completeUrl";
            const string cancelUrl = SCHEME + "://" + HOST + "/cancelUrl";

            var payment = new PayPal.Api.Payment {id = "1a1a"};
            _paypalPaymentAdaptorMock.Setup(c => c.CreatePayment(registrationDTO, completeUrl, cancelUrl))
                .Returns(payment);

            // Setup calls to Url.Action
            _urlHelperMock.Setup(
                c => c.Action("Complete", "Payment", new RouteValueDictionary { { "paymentId", paymentId } }, SCHEME, HOST))
                .Returns(completeUrl);

            _urlHelperMock.Setup(
                c => c.Action("Cancel", "Payment", new RouteValueDictionary { { "paymentId", paymentId } }, SCHEME, HOST))
                .Returns(cancelUrl);

            _registrationServiceMock.Setup(t => t.SaveOnPaymentInitiation(registrationDTO));
            //_paymentController.Url = new UrlHelper();
            _paymentController.InitiatePayment(registrationDTO);

            Assert.AreEqual(1, registrationDTO.EventId);
            Assert.AreEqual("MastiSpell", registrationDTO.EventName);
            Assert.AreEqual(23, registrationDTO.MastiPaymentId);

            _paymentIdGeneratorMock.VerifyAll();
            _paypalPaymentAdaptorMock.VerifyAll();
            //_registrationServiceMock.VerifyAll();
            _applicationCacheMock.VerifyAll();
            _urlHelperMock.VerifyAll();
        }


    }
}