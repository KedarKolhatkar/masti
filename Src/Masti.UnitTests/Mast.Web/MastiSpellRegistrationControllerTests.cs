﻿using System.Collections.Generic;
using Masti.Entities.Model;
using Masti.Services;
using Masti.Web.Controllers;
using Masti.Web.Infrasturcture;
using Moq;
using NUnit.Framework;

namespace Masti.UnitTests.Mast.Web
{
    [TestFixture]
    public class MastiSpellRegistrationControllerTests
    {
        private Mock<IApplicationSettings> _appSettingsMock;
        private Mock<IEventService> _eventServiceMock;
        private Mock<ILookupService> _lookupServiceMock;
        private Mock<IApplicationCache> _applicationCacheMock;
        private MastiSpellRegistrationController _registrationController;

        [SetUp]
        public void SetUp()
        {
            _lookupServiceMock = new Mock<ILookupService>();
            _eventServiceMock = new Mock<IEventService>();
            _appSettingsMock = new Mock<IApplicationSettings>();
            _applicationCacheMock = new Mock<IApplicationCache>();
            _registrationController = new MastiSpellRegistrationController(_lookupServiceMock.Object,
                _eventServiceMock.Object,  _appSettingsMock.Object, _applicationCacheMock.Object);
        }

        [Test]
        public void TestGetParticipantReferenceData()
        {
            _appSettingsMock.SetupGet(c => c.EventId).Returns(2);

            _eventServiceMock.Setup(c => c.GetPatronTypes(2)).Returns(new List<PatronType>());
            _lookupServiceMock.Setup(c => c.GetStates()).Returns(new List<State>());
            _eventServiceMock.Setup(c => c.GetCurrentRegistrationPrices(2)).Returns(new Dictionary<string, decimal>());
            _lookupServiceMock.Setup(c => c.GetPatronTypeGradeTypeDictionary()).Returns(new Dictionary<string, IList<GradeType>>());
            _lookupServiceMock.Setup(c => c.GetPatronTypeAgeTypeDictionary()).Returns(new Dictionary<string, IList<AgeType>>());

            Assert.NotNull(_registrationController.GetParticipantReferenceData());

            _lookupServiceMock.VerifyAll();
            _appSettingsMock.VerifyAll();
            _eventServiceMock.VerifyAll();
        }

        [Test]
        public void TestGetCounties()
        {
            _lookupServiceMock.Setup(c => c.GetCounties("VA")).Returns(new List<County>());
            Assert.NotNull(_registrationController.GetCounties("VA"));
            _lookupServiceMock.VerifyAll();
        }

        [Test]
        public void TestGetSchools()
        {
            _lookupServiceMock.Setup(c => c.GetSchools(1)).Returns(new List<School>());
            Assert.NotNull(_registrationController.GetSchools(1));
            _lookupServiceMock.VerifyAll();
        }
    }
}