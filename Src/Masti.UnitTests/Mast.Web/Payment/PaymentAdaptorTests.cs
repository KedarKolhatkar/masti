﻿using Masti.Web.Payment;
using NUnit.Framework;
using Masti.UnitTests.Masti.DataAccess.Builders;
using Masti.DTO;
namespace Masti.UnitTests.Mast.Web.Payment
{
    [TestFixture]
    public class PaymentAdaptorTests
    {
        private IPaypalPaymentAdaptor _paymentAdaptor;

        [OneTimeSetUp]
        public void Setup()
        {
            _paymentAdaptor = new PaypalPaymentAdaptor();
        }

        [Test]
        public void GetPatronDescriptionForMASTISPELL_JrBeeSpeller()
        {
            var registration = new RegistrationDTO()
            {
                EventName = "MastiSpell 2017",
                EventTypeCode = "MASTISPELL"
            };

            var patron = new PatronDTO()
            {
                FirstName = "Jane",
                MiddleName = "John",
                LastName = "Doe",
                PatronTypeCode = "JrBeeSpeller",
                PatronTypeDescription = "Speller-Jr.Bee (K-Gr4)",
                GradeTypeDescription = "2nd Grade"
            };

            var patronDescription = _paymentAdaptor.GetPatronDescription(registration, patron);
            Assert.AreEqual("MastiSpell 2017 registration for Jane John Doe (Speller-Jr.Bee (K-Gr4)) 2nd Grade", patronDescription);
        }

        [Test]
        public void GetPatronDescriptionForMASTISPELL_Parent()
        {
            var registration = new RegistrationDTO()
            {
                EventName = "MastiSpell 2017",
                EventTypeCode = "MASTISPELL"
            };

            var patron = new PatronDTO()
            {
                FirstName = "John",
                MiddleName = "James",
                LastName = "Doe",
                PatronTypeCode = "Parent",
                PatronTypeDescription = "Parent/Guardian"
            };

            var patronDescription = _paymentAdaptor.GetPatronDescription(registration, patron);
            Assert.AreEqual("MastiSpell 2017 registration for John James Doe (Parent/Guardian)", patronDescription);
        }

        [Test]
        public void GetPatronDescriptionForMASTIFEST()
        {
            var registration = new RegistrationDTO()
            {
                EventName = "MastiFest 2017",
                EventTypeCode = "MASTIFEST"
            };

            var patron = new PatronDTO()
            {
                FirstName = "John",
                MiddleName = "James",
                LastName = "Doe",
                PatronTypeDescription = "Adult"
            };

            var patronDescription = _paymentAdaptor.GetPatronDescription(registration, patron);
            Assert.AreEqual("MastiFest 2017 registration for John James Doe (Adult)", patronDescription);
        }

    }
}