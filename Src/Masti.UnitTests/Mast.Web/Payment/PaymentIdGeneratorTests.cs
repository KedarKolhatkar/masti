﻿using Masti.Web.Payment;
using NUnit.Framework;

namespace Masti.UnitTests.Mast.Web.Payment
{
    [TestFixture]
    public class PaymentIdGeneratorTests
    {
        private IPaymentIdGenerator _paymentIdGenerator;

        [OneTimeSetUp]
        public void Setup()
        {
            _paymentIdGenerator = new PaymentIdGenerator();
        }

        [Test]
        public void payment_id_is_in_range()
        {
            var generatedId = _paymentIdGenerator.Generate();
            Assert.True(generatedId > 1);
            Assert.True(generatedId < 1000);
        }
    }
}