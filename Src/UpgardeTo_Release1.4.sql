﻿/****************************************************************/
/************** Schema Update Script **************************************/
/****************************************************************/
alter table [Event]
add 
MainMessage varchar(250) NULL,
HomePageURL varchar(250) NULL

alter table [Registration]
add 
MastiPaymentId [int] NULL,
PaymentInitiationDate [datetime] NOT NULL default '10/22/2016',
PaymentCompleteDate [datetime] NULL,
PaymentCancelledDate [datetime] NULL

update [Registration]
set PaymentInitiationDate = RegisteredDate

update [Registration]
set PaymentCompleteDate = RegisteredDate

/****************************************************************/
/************** Reference Data update Script ********************/
/****************************************************************/

/* New Patron Types to Support MastiFest */
Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('Adult', 'PatronType', 'Adult (over 18 Years)', 1)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('Youth', 'PatronType', 'Youth (11 - 17 Years)', 2)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('Child', 'PatronType', 'Child (3-10 Years)', 3)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('Infant-1', 'PatronType', 'Infant (below 3 Years)', 4)

/* Patron Types for MastiFeste */
Insert into [EventTypePatronType]
(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
values
(7, 'MASTIFEST', 'Adult', 1)

Insert into [EventTypePatronType]
(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
values
(8, 'MASTIFEST', 'Youth', 2)

Insert into [EventTypePatronType]
(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
values
(9, 'MASTIFEST', 'Child', 3)

Insert into [EventTypePatronType]
(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
values
(10, 'MASTIFEST', 'Infant-1', 4)

SET IDENTITY_INSERT [Event] ON

/* MastiFest Event 2016 */
INSERT [dbo].[Event] ([EventId], [Name], [Description], [EventTypeCode], [EventDate], 
[RegistrationStartDate], [RegistrationEndDate], [VenueName], [VenueAddress], [EarlyBirdOn],
[MainMessage], [HomePageURL]) 
VALUES (3, N'MastiFest 2016', N'MastiFest Event for 2016', N'MASTIFEST', CAST(N'2016-10-22 00:00:00.000' AS DateTime), 
CAST(N'2016-09-19 00:00:00.000' AS DateTime), CAST(N'2016-10-22 00:00:00.000' AS DateTime), 
N'Tuscarora High School', N'801 N King St, Leesburg, VA 20176', 1,
'Limited Early Bird Prices!! -- Offered for first 100 registrants only!! Buy Online to save $$$.',
'http://www.mastifest.com')

SET IDENTITY_INSERT [Event] OFF

SET IDENTITY_INSERT [RegistrationPrice] ON

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (9, 3, N'Adult', 22.0000, 25.0000, CAST(N'2016-09-01 00:00:00.000' AS DateTime), CAST(N'2016-10-22 00:00:00.000' AS DateTime))

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (10, 3, N'Youth', 22.0000, 25.0000, CAST(N'2016-09-01 00:00:00.000' AS DateTime), CAST(N'2016-10-22 00:00:00.000' AS DateTime))

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (11, 3, N'Child', 14.0000, 18.0000, CAST(N'2016-09-01 00:00:00.000' AS DateTime), CAST(N'2016-10-22 00:00:00.000' AS DateTime))

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (12, 3, N'Infant-1', 0.0000, 0.0000, CAST(N'2016-09-01 00:00:00.000' AS DateTime), CAST(N'2016-10-22 00:00:00.000' AS DateTime))

SET IDENTITY_INSERT [RegistrationPrice] OFF

