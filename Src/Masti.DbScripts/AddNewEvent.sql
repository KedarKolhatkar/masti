BEGIN
SET IDENTITY_INSERT [Event] ON

INSERT [dbo].[Event] ([EventId], [Name], [Description], [EventTypeCode], [EventDate], 
[RegistrationStartDate], [RegistrationEndDate], [VenueName], [VenueAddress], [EarlyBirdOn],
[HomePageURL]) 
VALUES (6, N'Jr. Spelling Bee - MastiSpell 2018', N'MastiFest Event for 2018', N'MASTISPELL', CAST(N'2018-06-09 00:00:00.000' AS DateTime), 
CAST(N'2017-10-03 00:00:00.000' AS DateTime), CAST(N'2018-06-10 00:00:00.000' AS DateTime), 
N'Stone Hill Middle School', N'23415 Evergreen Ridge Drive, Ashburn VA, 20148', 1,
'http://www.mastispell.com')

SET IDENTITY_INSERT [Event] OFF

SET IDENTITY_INSERT [RegistrationPrice] ON

INSERT INTO [dbo].[RegistrationPrice]
([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate])
VALUES
(21, 6, 'Parent', 20.00, 40.00, '10/03/2017', '06/10/2018')

INSERT INTO [dbo].[RegistrationPrice]
([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate])
VALUES
(22, 6, 'JrBeeSpeller', 80.00, 100.00, '10/03/2016', '06/10/2018')

INSERT INTO [dbo].[RegistrationPrice]
([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate])
VALUES
(23, 6, 'KidSpectator', 10.00, 20.00, '10/03/2016', '06/10/2018')

INSERT INTO [dbo].[RegistrationPrice]
([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate])
VALUES
(24, 6, 'OtherSpectator', 20.00, 40.00, '10/03/2016', '06/10/2018')

SET IDENTITY_INSERT [RegistrationPrice] OFF
END