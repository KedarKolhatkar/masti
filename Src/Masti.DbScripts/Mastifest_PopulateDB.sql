/******************************************************************************/
/******* Reference Data ****************************/
/******************************************************************************/
Insert into [State] (StateCode, Name) values ('VA', 'Virginia')
Insert into [State] (StateCode, Name) values ('MD', 'Maryland')
Insert into [State] (StateCode, Name) values ('WV', 'West Virginia')
Insert into [State] (StateCode, Name) values ('PA', 'Pennsylvania')
Insert into [State] (StateCode, Name) values ('DC', 'District of Columbia')
Insert into [State] (StateCode, Name) values ('NY', 'New York')
Insert into [State] (StateCode, Name) values ('CT', 'Connectinut')
Insert into [State] (StateCode, Name) values ('TX', 'Texas')
Insert into [State] (StateCode, Name) values ('FL', 'Florida')
Insert into [State] (StateCode, Name) values ('KS', 'Kansas')
Insert into [State] (StateCode, Name) values ('NC', 'North Carolina')
Insert into [State] (StateCode, Name) values ('NJ', 'New Jersey')
Insert into [State] (StateCode, Name) values ('IL', 'Illinois')

SET IDENTITY_INSERT [County] ON
--Insert into [County]
--(CountyId, Name, StateCode) values
--(1, 'Loudoun', 'VA')
Insert into [County] (CountyId, Name, StateCode) values (1,'Appomattox', 'VA');
Insert into [County] (CountyId, Name, StateCode) values (2,'Arlington', 'VA');
Insert into [County] (CountyId, Name, StateCode) values (3,'Baltimore', 'MD');
Insert into [County] (CountyId, Name, StateCode) values (4,'Berkeley', 'WV');
Insert into [County] (CountyId, Name, StateCode) values (5,'Bucks', 'PA');
Insert into [County] (CountyId, Name, StateCode) values (6,'Buncombe', 'NC');
Insert into [County] (CountyId, Name, StateCode) values (7,'Chester', 'PA');
Insert into [County] (CountyId, Name, StateCode) values (8,'DistrictOfColumbia', 'DC');
Insert into [County] (CountyId, Name, StateCode) values (9,'Erie', 'NY');
Insert into [County] (CountyId, Name, StateCode) values (10,'Fairfax', 'VA');
Insert into [County] (CountyId, Name, StateCode) values (11,'Fairfield', 'CT');
Insert into [County] (CountyId, Name, StateCode) values (12,'FallsChurchCity', 'VA');
Insert into [County] (CountyId, Name, StateCode) values (13,'Fauquier', 'VA');
Insert into [County] (CountyId, Name, StateCode) values (14,'FortBend', 'TX');
Insert into [County] (CountyId, Name, StateCode) values (15,'Frederick', 'MD');
Insert into [County] (CountyId, Name, StateCode) values (16,'GarnetValley', 'PA');
Insert into [County] (CountyId, Name, StateCode) values (17,'Hanover', 'VA');
Insert into [County] (CountyId, Name, StateCode) values (18,'Henrico', 'VA');
Insert into [County] (CountyId, Name, StateCode) values (19,'Hillsborough', 'FL');
Insert into [County] (CountyId, Name, StateCode) values (20,'Johnson', 'KS');
Insert into [County] (CountyId, Name, StateCode) values (21,'Loudoun', 'VA');
Insert into [County] (CountyId, Name, StateCode) values (22,'ManassasParkCity', 'VA');
Insert into [County] (CountyId, Name, StateCode) values (23,'Mecklenburg', 'NC');
Insert into [County] (CountyId, Name, StateCode) values (24,'Mercer', 'NJ');
Insert into [County] (CountyId, Name, StateCode) values (25,'Middlesex', 'NJ');
Insert into [County] (CountyId, Name, StateCode) values (26,'Montgomery', 'MD');
Insert into [County] (CountyId, Name, StateCode) values (27,'Morris', 'NJ');
Insert into [County] (CountyId, Name, StateCode) values (28,'Nassau', 'NY');
Insert into [County] (CountyId, Name, StateCode) values (29,'PrinceWilliam', 'VA');
Insert into [County] (CountyId, Name, StateCode) values (30,'Richmond', 'VA');
Insert into [County] (CountyId, Name, StateCode) values (31,'Wake', 'NC');
Insert into [County] (CountyId, Name, StateCode) values (32,'Will', 'IL');
SET IDENTITY_INSERT [County] OFF


--SET IDENTITY_INSERT [School] ON
-- Appomattox, VA
Insert into [School] (CountyId, Name) values (1, 'Appomattox ES');
Insert into [School] (CountyId, Name) values (1, 'Appomattox MS');
 
-- Arlington, VA
 Insert into [School] (CountyId, Name) values (2, 'Abingdon ES');
 Insert into [School] (CountyId, Name) values (2, 'Arlington Science Focus School');
 Insert into [School] (CountyId, Name) values (2, 'Arlington Traditional School');
 Insert into [School] (CountyId, Name) values (2, 'Ashlawn ES');
 Insert into [School] (CountyId, Name) values (2, 'Barcroft ES');
 Insert into [School] (CountyId, Name) values (2, 'Barrett ES');
 Insert into [School] (CountyId, Name) values (2, 'Campbell ES');
 Insert into [School] (CountyId, Name) values (2, 'Carlin Springs School');
 Insert into [School] (CountyId, Name) values (2, 'Chesterbrook Montessori (Pvt)');
 Insert into [School] (CountyId, Name) values (2, 'Claremont Immersion School');
 Insert into [School] (CountyId, Name) values (2, 'Drew Model ES');
 Insert into [School] (CountyId, Name) values (2, 'First Bapt Church Of Claredon (Pvt)');
 Insert into [School] (CountyId, Name) values (2, 'Francis Scott Key ES');
 Insert into [School] (CountyId, Name) values (2, 'Glebe ES');
 Insert into [School] (CountyId, Name) values (2, 'Gunston MS');
 Insert into [School] (CountyId, Name) values (2, 'Henry ES');
 Insert into [School] (CountyId, Name) values (2, 'Hoffman-Boston ES');
 Insert into [School] (CountyId, Name) values (2, 'Jamestown ES');
 Insert into [School] (CountyId, Name) values (2, 'Jefferson MS');
 Insert into [School] (CountyId, Name) values (2, 'Kenmore MS');
 Insert into [School] (CountyId, Name) values (2, 'Key ES');
 Insert into [School] (CountyId, Name) values (2, 'Long Branch ES');
 Insert into [School] (CountyId, Name) values (2, 'McKinley ES');
 Insert into [School] (CountyId, Name) values (2, 'Nottingham ES');
 Insert into [School] (CountyId, Name) values (2, 'Oakridge ES');
 Insert into [School] (CountyId, Name) values (2, 'Our Savior Lutheran (Pvt)');
 Insert into [School] (CountyId, Name) values (2, 'Randolph ES');
 Insert into [School] (CountyId, Name) values (2, 'Rivendell (Pvt)');
 Insert into [School] (CountyId, Name) values (2, 'St Agnes (Pvt)');
 Insert into [School] (CountyId, Name) values (2, 'St Ann ES (Pvt)');
 Insert into [School] (CountyId, Name) values (2, 'St Charles (Pvt)');
 Insert into [School] (CountyId, Name) values (2, 'St Thomas More Cathedral (Pvt)');
 Insert into [School] (CountyId, Name) values (2, 'Swanson MS');
 Insert into [School] (CountyId, Name) values (2, 'Taylor ES');
 Insert into [School] (CountyId, Name) values (2, 'Tuckahoe ES');
 Insert into [School] (CountyId, Name) values (2, 'Williamsburg MS');

 -- Baltimore, MD
 Insert into [School] (CountyId, Name) values (3, 'Arbutus MS');
Insert into [School] (CountyId, Name) values (3, 'Bridge Center MS');
Insert into [School] (CountyId, Name) values (3, 'Catonsville ES');
Insert into [School] (CountyId, Name) values (3, 'Catonsville MS');
Insert into [School] (CountyId, Name) values (3, 'Cockeysville MS');
Insert into [School] (CountyId, Name) values (3, 'Deep Creek Magnet MS');
Insert into [School] (CountyId, Name) values (3, 'Deer Park MS');
Insert into [School] (CountyId, Name) values (3, 'Dumbarton MS');
Insert into [School] (CountyId, Name) values (3, 'Dundalk MS');
Insert into [School] (CountyId, Name) values (3, 'Edmondson Heights ES');
Insert into [School] (CountyId, Name) values (3, 'Featherbed Lane ES');
Insert into [School] (CountyId, Name) values (3, 'Fifth District ES');
Insert into [School] (CountyId, Name) values (3, 'Franklin ES');
Insert into [School] (CountyId, Name) values (3, 'Franklin MS');
Insert into [School] (CountyId, Name) values (3, 'Garrison Forest School (Pvt)');
Insert into [School] (CountyId, Name) values (3, 'General John Stricker MS');
Insert into [School] (CountyId, Name) values (3, 'Glenmar ES');
Insert into [School] (CountyId, Name) values (3, 'Glyndon ES');
Insert into [School] (CountyId, Name) values (3, 'Golden Ring MS');
Insert into [School] (CountyId, Name) values (3, 'Halstead Academy');
Insert into [School] (CountyId, Name) values (3, 'Hawthorne ES');
Insert into [School] (CountyId, Name) values (3, 'Hebbville ES');
Insert into [School] (CountyId, Name) values (3, 'Hereford MS');
Insert into [School] (CountyId, Name) values (3, 'Holabird MS');
Insert into [School] (CountyId, Name) values (3, 'Jemicy School (Pvt)');
Insert into [School] (CountyId, Name) values (3, 'Johnnycake ES');
Insert into [School] (CountyId, Name) values (3, 'Lansdowne ES');
Insert into [School] (CountyId, Name) values (3, 'Lansdowne MS');
Insert into [School] (CountyId, Name) values (3, 'Loyola Blakefield (Pvt)');
Insert into [School] (CountyId, Name) values (3, 'Mars Estates ES');
Insert into [School] (CountyId, Name) values (3, 'Martin Boulevard ES');
Insert into [School] (CountyId, Name) values (3, 'McCormick ES');
Insert into [School] (CountyId, Name) values (3, 'McDonogh School (Pvt)');
Insert into [School] (CountyId, Name) values (3, 'Middle River MS');
Insert into [School] (CountyId, Name) values (3, 'Mount de Sales Academy (Pvt)');
Insert into [School] (CountyId, Name) values (3, 'New Town ES');
Insert into [School] (CountyId, Name) values (3, 'Old Court MS');
Insert into [School] (CountyId, Name) values (3, 'Oldfields School (Pvt)');
Insert into [School] (CountyId, Name) values (3, 'Oliver Beach ES');
Insert into [School] (CountyId, Name) values (3, 'Our Lady of Mt. Carmel (Pvt)');
Insert into [School] (CountyId, Name) values (3, 'Owings Mills ES');
Insert into [School] (CountyId, Name) values (3, 'Parkville MS');
Insert into [School] (CountyId, Name) values (3, 'Perry Hall MS');
Insert into [School] (CountyId, Name) values (3, 'Pikesville MS');
Insert into [School] (CountyId, Name) values (3, 'Pine Grove ES');
Insert into [School] (CountyId, Name) values (3, 'Pine Grove MS');
Insert into [School] (CountyId, Name) values (3, 'Pinewood ES');
Insert into [School] (CountyId, Name) values (3, 'Pot Spring ES');
Insert into [School] (CountyId, Name) values (3, 'Ridgely MS');
Insert into [School] (CountyId, Name) values (3, 'Rosedale Center MS');
Insert into [School] (CountyId, Name) values (3, 'Sandalwood ES');
Insert into [School] (CountyId, Name) values (3, 'Sandy Plains ES');
Insert into [School] (CountyId, Name) values (3, 'Seneca ES');
Insert into [School] (CountyId, Name) values (3, 'Seven Oaks ES');
Insert into [School] (CountyId, Name) values (3, 'Seventh District ES');
Insert into [School] (CountyId, Name) values (3, 'Southwest Academy MS');
Insert into [School] (CountyId, Name) values (3, 'Sparks ES');
Insert into [School] (CountyId, Name) values (3, 'Sparrows Point MS');
Insert into [School] (CountyId, Name) values (3, 'St. Pauls School (Pvt)');
Insert into [School] (CountyId, Name) values (3, 'St. Timothys School (Pvt)');
Insert into [School] (CountyId, Name) values (3, 'Stemmers Run MS');
Insert into [School] (CountyId, Name) values (3, 'Sudbrook Magnet MS');
Insert into [School] (CountyId, Name) values (3, 'The Park School (Pvt)');
Insert into [School] (CountyId, Name) values (3, 'West Towson ES');
Insert into [School] (CountyId, Name) values (3, 'Westchester ES');
Insert into [School] (CountyId, Name) values (3, 'Windsor Mill MS');
Insert into [School] (CountyId, Name) values (3, 'Winfield ES');
Insert into [School] (CountyId, Name) values (3, 'Woodbridge ES');
Insert into [School] (CountyId, Name) values (3, 'Woodlawn MS');

-- Berkeley, WV
Insert into [School] (CountyId, Name) values (4, 'Back Creek Valley ES'); 
Insert into [School] (CountyId, Name) values (4, 'Bedington ES'); 
Insert into [School] (CountyId, Name) values (4, 'Berkeley Heights ES'); 
Insert into [School] (CountyId, Name) values (4, 'Bunker Hill ES'); 
Insert into [School] (CountyId, Name) values (4, 'Burke Street ES'); 
Insert into [School] (CountyId, Name) values (4, 'Christian Academy (Pvt)'); 
Insert into [School] (CountyId, Name) values (4, 'Faith Christian Academy (Pvt)'); 
Insert into [School] (CountyId, Name) values (4, 'Gerrardstown ES'); 
Insert into [School] (CountyId, Name) values (4, 'Hedgesville ES'); 
Insert into [School] (CountyId, Name) values (4, 'Hedgesville MS'); 
Insert into [School] (CountyId, Name) values (4, 'Inwood Primary School'); 
Insert into [School] (CountyId, Name) values (4, 'Marlowe ES'); 
Insert into [School] (CountyId, Name) values (4, 'Martinsburg Christian Academy (Pvt)'); 
Insert into [School] (CountyId, Name) values (4, 'Martinsburg North MS'); 
Insert into [School] (CountyId, Name) values (4, 'Martinsburg South MS'); 
Insert into [School] (CountyId, Name) values (4, 'Morgan Academy (Pvt)'); 
Insert into [School] (CountyId, Name) values (4, 'Musselman MS'); 
Insert into [School] (CountyId, Name) values (4, 'Opequon ES'); 
Insert into [School] (CountyId, Name) values (4, 'Rocky Knoll School (Pvt)'); 
Insert into [School] (CountyId, Name) values (4, 'Rosemont ES'); 
Insert into [School] (CountyId, Name) values (4, 'Spring Mills MS'); 
Insert into [School] (CountyId, Name) values (4, 'St. Joseph School (Pvt)'); 
Insert into [School] (CountyId, Name) values (4, 'Tuscarora ES'); 
Insert into [School] (CountyId, Name) values (4, 'Valley View ES'); 
Insert into [School] (CountyId, Name) values (4, 'Winchester Avenue ES');

-- Bucks, PA
Insert into [School] (CountyId, Name) values (5, 'Barclay ES');
Insert into [School] (CountyId, Name) values (5, 'Barton ES');
Insert into [School] (CountyId, Name) values (5, 'Bedminster ES');
Insert into [School] (CountyId, Name) values (5, 'Charles H Boehm MS');
Insert into [School] (CountyId, Name) values (5, 'Devine ES');
Insert into [School] (CountyId, Name) values (5, 'Doyle ES');
Insert into [School] (CountyId, Name) values (5, 'Durham-Nockamixon ES');
Insert into [School] (CountyId, Name) values (5, 'Edgewood ES');
Insert into [School] (CountyId, Name) values (5, 'Gayman ES');
Insert into [School] (CountyId, Name) values (5, 'Grasse ES');
Insert into [School] (CountyId, Name) values (5, 'Heckman ES');
Insert into [School] (CountyId, Name) values (5, 'Hillcrest ES');
Insert into [School] (CountyId, Name) values (5, 'Holland ES');
Insert into [School] (CountyId, Name) values (5, 'Lafayette ES');
Insert into [School] (CountyId, Name) values (5, 'Log College MS');
Insert into [School] (CountyId, Name) values (5, 'Longstreth ES');
Insert into [School] (CountyId, Name) values (5, 'Manor ES');
Insert into [School] (CountyId, Name) values (5, 'Mcdonald ES');
Insert into [School] (CountyId, Name) values (5, 'Miller ES');
Insert into [School] (CountyId, Name) values (5, 'Newtown MS');
Insert into [School] (CountyId, Name) values (5, 'Oxford Valley ES');
Insert into [School] (CountyId, Name) values (5, 'Penn Valley ES');
Insert into [School] (CountyId, Name) values (5, 'Quakertown ES');
Insert into [School] (CountyId, Name) values (5, 'Richboro ES');
Insert into [School] (CountyId, Name) values (5, 'Robert K Shafer MS');
Insert into [School] (CountyId, Name) values (5, 'Roosevelt MS');
Insert into [School] (CountyId, Name) values (5, 'Samuel K Faust ES');
Insert into [School] (CountyId, Name) values (5, 'Sellersville ES');
Insert into [School] (CountyId, Name) values (5, 'Sol Feinstone ES');
Insert into [School] (CountyId, Name) values (5, 'Tinicum ES');
Insert into [School] (CountyId, Name) values (5, 'Valley ES');
Insert into [School] (CountyId, Name) values (5, 'West Rockhill ES');
Insert into [School] (CountyId, Name) values (5, 'William Penn MS');

-- Buncombe, NC
Insert into [School] (CountyId, Name) values (6, 'A.C. Reynolds MS');
Insert into [School] (CountyId, Name) values (6, 'Averys Creek ES');
Insert into [School] (CountyId, Name) values (6, 'Barnardsville ES');
Insert into [School] (CountyId, Name) values (6, 'Black Mountain ES');
Insert into [School] (CountyId, Name) values (6, 'Candler ES');
Insert into [School] (CountyId, Name) values (6, 'Cane Creek MS ');
Insert into [School] (CountyId, Name) values (6, 'Charles C. Bell ES');
Insert into [School] (CountyId, Name) values (6, 'Charles D. Owen MS');
Insert into [School] (CountyId, Name) values (6, 'Clyde A. Erwin MS');
Insert into [School] (CountyId, Name) values (6, 'Emma ES');
Insert into [School] (CountyId, Name) values (6, 'Enka MS');
Insert into [School] (CountyId, Name) values (6, 'Fairview ES');
Insert into [School] (CountyId, Name) values (6, 'Glen Arden ES');
Insert into [School] (CountyId, Name) values (6, 'Haw Creek ES');
Insert into [School] (CountyId, Name) values (6, 'Hominy Valley ES');
Insert into [School] (CountyId, Name) values (6, 'Johnston ES');
Insert into [School] (CountyId, Name) values (6, 'Leicester ES');
Insert into [School] (CountyId, Name) values (6, 'North Buncombe ES');
Insert into [School] (CountyId, Name) values (6, 'North Buncombe MS');
Insert into [School] (CountyId, Name) values (6, 'Oakley ES');
Insert into [School] (CountyId, Name) values (6, 'Pisgah ES');
Insert into [School] (CountyId, Name) values (6, 'Sand Hill - Venable ES');
Insert into [School] (CountyId, Name) values (6, 'Valley Springs MS');
Insert into [School] (CountyId, Name) values (6, 'W.D. Williams ES');
Insert into [School] (CountyId, Name) values (6, 'Weaverville ES');
Insert into [School] (CountyId, Name) values (6, 'West Buncombe ES');
Insert into [School] (CountyId, Name) values (6, 'William W. Estes ES');
Insert into [School] (CountyId, Name) values (6, 'Woodfin ES');

-- Chester, PA
Insert into [School] (CountyId, Name) values (7, 'Abrams Hebrew Academy (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Achievement House CS'); 
Insert into [School] (CountyId, Name) values (7, 'Avon Grove CS'); 
Insert into [School] (CountyId, Name) values (7, 'Beaver Creek ES'); 
Insert into [School] (CountyId, Name) values (7, 'Buckingham Friends School (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Calvary Christian (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Charles F Patton MS'); 
Insert into [School] (CountyId, Name) values (7, 'Chester County Family Academy CS'); 
Insert into [School] (CountyId, Name) values (7, 'Christian Life (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Collegium CS'); 
Insert into [School] (CountyId, Name) values (7, 'Downington MS'); 
Insert into [School] (CountyId, Name) values (7, 'E N Peirce MS'); 
Insert into [School] (CountyId, Name) values (7, 'Faith Baptist Christian Academy (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Faith Christian Academy (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Fred S Engle MS'); 
Insert into [School] (CountyId, Name) values (7, 'Gordon Education Center'); 
Insert into [School] (CountyId, Name) values (7, 'Graystone Academy CS'); 
Insert into [School] (CountyId, Name) values (7, 'Great Valley MS'); 
Insert into [School] (CountyId, Name) values (7, 'Grey Nun Academy (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Holy Family Regional Catholic (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Holy Trinity (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Hope Lutheran (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Hopewell ES'); 
Insert into [School] (CountyId, Name) values (7, 'Immaculate Conception (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'J R Fugett MS'); 
Insert into [School] (CountyId, Name) values (7, 'Kathryn D Markley ES'); 
Insert into [School] (CountyId, Name) values (7, 'Kennett MS'); 
Insert into [School] (CountyId, Name) values (7, 'Lionville MS'); 
Insert into [School] (CountyId, Name) values (7, 'Nativity Of Our Lord School (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Newtown Friends (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'North Brandywine MS'); 
Insert into [School] (CountyId, Name) values (7, 'Octorara MS'); 
Insert into [School] (CountyId, Name) values (7, 'Our Lady Fatima Elementary (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Our Lady Of Good Counsel (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Our Lady Of Grace (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Owen J Roberts MS'); 
Insert into [School] (CountyId, Name) values (7, 'Penns Grove School'); 
Insert into [School] (CountyId, Name) values (7, 'Pennsylvania Leadership CS'); 
Insert into [School] (CountyId, Name) values (7, 'Philadelphia Christian Center Academy (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Phoenixville Area MS'); 
Insert into [School] (CountyId, Name) values (7, 'Plumstead Christian (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Plumstead Christian Peace Valley Campus (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Quakertown Christian (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Queen Of The Universe (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Renaissance Academy CS'); 
Insert into [School] (CountyId, Name) values (7, 'Sankofa Academy CS'); 
Insert into [School] (CountyId, Name) values (7, 'Schoolhouse Learning Center (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Scott MS'); 
Insert into [School] (CountyId, Name) values (7, 'South Brandywine MS'); 
Insert into [School] (CountyId, Name) values (7, 'St. Agnes-Sacred Heart (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Andrew (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Ann School (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Charles Borromeo (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Ephrem (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Frances Cabrini (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Ignatius ES (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Isidore ES (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. John The Baptist Catholic (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. John The Evangelist School (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Joseph St Robert (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Jude Catholic Education Center (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Katharine Drexel Regional Catholic (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Mark ES (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Martin Of Tours (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Michael The Archangel (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'St. Thomas Aquinas School (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Stetson MS'); 
Insert into [School] (CountyId, Name) values (7, 'Tredyffrin-Easttown MS'); 
Insert into [School] (CountyId, Name) values (7, 'Trevose Day (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Twin Valley MS'); 
Insert into [School] (CountyId, Name) values (7, 'United Friends (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Upper Bucks Christian (Pvt)'); 
Insert into [School] (CountyId, Name) values (7, 'Washington Crossing Christian (Pvt)');

-- DistrictOfColumbia
Insert into [School] (CountyId, Name) values (8, 'Academy Of Christian Education (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Academy For Education Through The Arts PCS');
Insert into [School] (CountyId, Name) values (8, 'Academy For Ideal Education (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Adams ES');
Insert into [School] (CountyId, Name) values (8, 'Aiton ES');
Insert into [School] (CountyId, Name) values (8, 'Amidon ES');
Insert into [School] (CountyId, Name) values (8, 'Anacostia Bible Church Christian School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Annunciation School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Appletree Early Learing PCS');
Insert into [School] (CountyId, Name) values (8, 'Arts & Technology PCS');
Insert into [School] (CountyId, Name) values (8, 'Assumption Catholic School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'A-T Seban Mesut School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Auguste Montessori (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Ballou Stay');
Insert into [School] (CountyId, Name) values (8, 'Bancroft ES');
Insert into [School] (CountyId, Name) values (8, 'Barnard ES');
Insert into [School] (CountyId, Name) values (8, 'Beers ES');
Insert into [School] (CountyId, Name) values (8, 'Benning ES');
Insert into [School] (CountyId, Name) values (8, 'Birney ES');
Insert into [School] (CountyId, Name) values (8, 'Blessed Sacrament ES School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Bowen ES');
Insert into [School] (CountyId, Name) values (8, 'Brent ES');
Insert into [School] (CountyId, Name) values (8, 'Bridges PCS');
Insert into [School] (CountyId, Name) values (8, 'Brightwood ES');
Insert into [School] (CountyId, Name) values (8, 'British School Of Washington (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Brookland ES');
Insert into [School] (CountyId, Name) values (8, 'Bruce-Monroe ES');
Insert into [School] (CountyId, Name) values (8, 'Bunker Hill ES');
Insert into [School] (CountyId, Name) values (8, 'Burroughs ES');
Insert into [School] (CountyId, Name) values (8, 'Burrville ES');
Insert into [School] (CountyId, Name) values (8, 'Calvary Christian Academy (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Capital City PCS');
Insert into [School] (CountyId, Name) values (8, 'Capitol Hill Day School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Carlos Rosario International PCS');
Insert into [School] (CountyId, Name) values (8, 'Child & Family Services');
Insert into [School] (CountyId, Name) values (8, 'Children Studio School PCS');
Insert into [School] (CountyId, Name) values (8, 'Clara Muhammad School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Clark ES');
Insert into [School] (CountyId, Name) values (8, 'Cleveland ES');
Insert into [School] (CountyId, Name) values (8, 'Community Academy PCS');
Insert into [School] (CountyId, Name) values (8, 'Consolidated Headstart');
Insert into [School] (CountyId, Name) values (8, 'Cook ES');
Insert into [School] (CountyId, Name) values (8, 'Cooke ES');
Insert into [School] (CountyId, Name) values (8, 'Cornerstone Community School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Correctional Detention Facility');
Insert into [School] (CountyId, Name) values (8, 'Correctional Treatment Facility');
Insert into [School] (CountyId, Name) values (8, 'Davis ES');
Insert into [School] (CountyId, Name) values (8, 'DC Bilingual PCS');
Insert into [School] (CountyId, Name) values (8, 'Devereux Childrens Center (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Draper ES');
Insert into [School] (CountyId, Name) values (8, 'Drew ES');
Insert into [School] (CountyId, Name) values (8, 'Dupont Park Adventist School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'E.L. Haynes PCS');
Insert into [School] (CountyId, Name) values (8, 'Eagle Academy PCS PCS');
Insert into [School] (CountyId, Name) values (8, 'Early Childhood Academy PCS');
Insert into [School] (CountyId, Name) values (8, 'Eaton ES');
Insert into [School] (CountyId, Name) values (8, 'Edison-Friendship PCS');
Insert into [School] (CountyId, Name) values (8, 'Elsie Whitlow Stokes Community Freedom PCS');
Insert into [School] (CountyId, Name) values (8, 'Emery ES');
Insert into [School] (CountyId, Name) values (8, 'Emilia, Reggio');
Insert into [School] (CountyId, Name) values (8, 'Ferebee-Hope ES');
Insert into [School] (CountyId, Name) values (8, 'First Rock Baptist Church Christian School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Fletcher Johnson Ed Complex');
Insert into [School] (CountyId, Name) values (8, 'Franklin Montessori School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Gage Eckington ES');
Insert into [School] (CountyId, Name) values (8, 'Garfield ES');
Insert into [School] (CountyId, Name) values (8, 'Garrison ES');
Insert into [School] (CountyId, Name) values (8, 'Georgetown Day School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Georgetown Montessori School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Gibbs ES');
Insert into [School] (CountyId, Name) values (8, 'Green ES');
Insert into [School] (CountyId, Name) values (8, 'Hamilton Center Special Education');
Insert into [School] (CountyId, Name) values (8, 'Harris ES');
Insert into [School] (CountyId, Name) values (8, 'Harris, P R Educational Center');
Insert into [School] (CountyId, Name) values (8, 'Hearst ES');
Insert into [School] (CountyId, Name) values (8, 'Hendley ES');
Insert into [School] (CountyId, Name) values (8, 'High Road School Of Wash DC (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Holy Name Catholic School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Holy Redeemer Catholic School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Holy Trinity School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Hope Academy PCS');
Insert into [School] (CountyId, Name) values (8, 'Houston ES');
Insert into [School] (CountyId, Name) values (8, 'Howard Road Academy');
Insert into [School] (CountyId, Name) values (8, 'Howard University Early Learning Program (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Hyde ES');
Insert into [School] (CountyId, Name) values (8, 'Hyde Leadership PCS');
Insert into [School] (CountyId, Name) values (8, 'Ideal Academy PCS');
Insert into [School] (CountyId, Name) values (8, 'Ideal Alternative School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Immaculate Conception School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Janney ES');
Insert into [School] (CountyId, Name) values (8, 'Just Us Kids Inc (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Kendall Demonstration ES (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Kenilworth ES');
Insert into [School] (CountyId, Name) values (8, 'Kennedy Institute (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Ketcham ES');
Insert into [School] (CountyId, Name) values (8, 'Key ES');
Insert into [School] (CountyId, Name) values (8, 'Kimball ES');
Insert into [School] (CountyId, Name) values (8, 'King ES');
Insert into [School] (CountyId, Name) values (8, 'Kuumba Learning Center (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Lafayette ES');
Insert into [School] (CountyId, Name) values (8, 'Langdon ES');
Insert into [School] (CountyId, Name) values (8, 'Lasalle ES');
Insert into [School] (CountyId, Name) values (8, 'Latin American Montessori Bilingual PCS');
Insert into [School] (CountyId, Name) values (8, 'Leckie ES');
Insert into [School] (CountyId, Name) values (8, 'Lee, Mamie');
Insert into [School] (CountyId, Name) values (8, 'Lowell School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Ludlow-Taylor ES');
Insert into [School] (CountyId, Name) values (8, 'Malcolm X ES');
Insert into [School] (CountyId, Name) values (8, 'Mann ES');
Insert into [School] (CountyId, Name) values (8, 'Maret School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Marshall Thurgood ES');
Insert into [School] (CountyId, Name) values (8, 'Mary Mcleod Bethune Day Academy PCS');
Insert into [School] (CountyId, Name) values (8, 'Maury ES');
Insert into [School] (CountyId, Name) values (8, 'Mcgogney ES');
Insert into [School] (CountyId, Name) values (8, 'Meridian PCS');
Insert into [School] (CountyId, Name) values (8, 'Merritt ES');
Insert into [School] (CountyId, Name) values (8, 'Metropolitan Day School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Meyer ES');
Insert into [School] (CountyId, Name) values (8, 'Miner ES');
Insert into [School] (CountyId, Name) values (8, 'Montessori School Of Chevy Chase (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Montgomery ES');
Insert into [School] (CountyId, Name) values (8, 'Moten Center Special Education');
Insert into [School] (CountyId, Name) values (8, 'Murch ES');
Insert into [School] (CountyId, Name) values (8, 'Nalle ES');
Insert into [School] (CountyId, Name) values (8, 'Nannie Helen Burroughs School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'National Cathedral School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'National Childrens Center (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'National Presbyterian School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Nativity Catholic Academy (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Naylor Road School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Next Step PCS');
Insert into [School] (CountyId, Name) values (8, 'Noyes ES');
Insert into [School] (CountyId, Name) values (8, 'Orr ES');
Insert into [School] (CountyId, Name) values (8, 'Our Lady Of Perpetual Help (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Our Lady Of Victory School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Oyster ES');
Insert into [School] (CountyId, Name) values (8, 'Park View ES');
Insert into [School] (CountyId, Name) values (8, 'Patterson ES');
Insert into [School] (CountyId, Name) values (8, 'Payne ES');
Insert into [School] (CountyId, Name) values (8, 'Peabody ES');
Insert into [School] (CountyId, Name) values (8, 'Plummer ES');
Insert into [School] (CountyId, Name) values (8, 'Potomac Lighthouse PCS');
Insert into [School] (CountyId, Name) values (8, 'Powell ES');
Insert into [School] (CountyId, Name) values (8, 'Pre-K Incentive Program');
Insert into [School] (CountyId, Name) values (8, 'Preparatory School Of The District Of Columbia (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Prospect Learning Center');
Insert into [School] (CountyId, Name) values (8, 'Randall Hyland (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Randle Highlands ES');
Insert into [School] (CountyId, Name) values (8, 'Raymond ES');
Insert into [School] (CountyId, Name) values (8, 'Reed ES');
Insert into [School] (CountyId, Name) values (8, 'River School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'River Terrace ES');
Insert into [School] (CountyId, Name) values (8, 'Robeson, Paul');
Insert into [School] (CountyId, Name) values (8, 'Rock Creek International School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Roosevelt Stay');
Insert into [School] (CountyId, Name) values (8, 'Roots Activity Learning Center (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Roots PCS');
Insert into [School] (CountyId, Name) values (8, 'Rose School');
Insert into [School] (CountyId, Name) values (8, 'Ross ES');
Insert into [School] (CountyId, Name) values (8, 'Rudolph ES');
Insert into [School] (CountyId, Name) values (8, 'Sacred Heart School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Sankofa Fie (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Savoy ES');
Insert into [School] (CountyId, Name) values (8, 'School For Arts In Learning PCS');
Insert into [School] (CountyId, Name) values (8, 'Seaton ES');
Insert into [School] (CountyId, Name) values (8, 'Shadd ES');
Insert into [School] (CountyId, Name) values (8, 'Shaed ES');
Insert into [School] (CountyId, Name) values (8, 'Sharpe Health School');
Insert into [School] (CountyId, Name) values (8, 'Shepherd ES');
Insert into [School] (CountyId, Name) values (8, 'Sheridan School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Sidwell Friends School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Simon ES');
Insert into [School] (CountyId, Name) values (8, 'Slowe ES');
Insert into [School] (CountyId, Name) values (8, 'Smithsonian Early Enrichment Center (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Smothers ES');
Insert into [School] (CountyId, Name) values (8, 'Spingarn Center');
Insert into [School] (CountyId, Name) values (8, 'Spingarn Stay');
Insert into [School] (CountyId, Name) values (8, 'St Anns Academy (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'St Augustine School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'St Benedict The Moor School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'St Francis De Sales School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'St Francis Xavier Catholic School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'St Gabriel School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'St Johns Community Services (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'St Patrick S Episcopal Day School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'St Peters Interparish School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Stanton ES');
Insert into [School] (CountyId, Name) values (8, 'Stevens ES');
Insert into [School] (CountyId, Name) values (8, 'Stoddert ES');
Insert into [School] (CountyId, Name) values (8, 'Sunrise Academy (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Taft Special Education Center');
Insert into [School] (CountyId, Name) values (8, 'Takoma ES');
Insert into [School] (CountyId, Name) values (8, 'Terrell ES');
Insert into [School] (CountyId, Name) values (8, 'The Ambassador Baptist Church Christian (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'The Bridges Academy (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'The Childrens House Of Washington (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'The Episcopal Center For Children (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'The Lab School Of Washington (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Thomas ES');
Insert into [School] (CountyId, Name) values (8, 'Thomson ES');
Insert into [School] (CountyId, Name) values (8, 'Tots Developmental School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Tree Of Life PCS');
Insert into [School] (CountyId, Name) values (8, 'Tri-community PCS');
Insert into [School] (CountyId, Name) values (8, 'Truesdell ES');
Insert into [School] (CountyId, Name) values (8, 'Tubman ES');
Insert into [School] (CountyId, Name) values (8, 'Tuition Grant');
Insert into [School] (CountyId, Name) values (8, 'Turner ES');
Insert into [School] (CountyId, Name) values (8, 'Two Rivers PCS');
Insert into [School] (CountyId, Name) values (8, 'Tyler ES');
Insert into [School] (CountyId, Name) values (8, 'Ujamaa School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Van Ness ES');
Insert into [School] (CountyId, Name) values (8, 'Walker-Jones ES');
Insert into [School] (CountyId, Name) values (8, 'Washington Academy PCS');
Insert into [School] (CountyId, Name) values (8, 'Washington International School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Washington Middle School (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Washington Science And Technology Academy (Pvt)');
Insert into [School] (CountyId, Name) values (8, 'Watkins ES');
Insert into [School] (CountyId, Name) values (8, 'Webb ES');
Insert into [School] (CountyId, Name) values (8, 'West ES');
Insert into [School] (CountyId, Name) values (8, 'Wheatley ES');
Insert into [School] (CountyId, Name) values (8, 'Whittier ES');
Insert into [School] (CountyId, Name) values (8, 'Wilkinson ES');
Insert into [School] (CountyId, Name) values (8, 'William E. Doar PCS');
Insert into [School] (CountyId, Name) values (8, 'Wilson ES');
Insert into [School] (CountyId, Name) values (8, 'Winston ES');
Insert into [School] (CountyId, Name) values (8, 'Young ES');

-- Erie, NY
Insert into [School] (CountyId, Name) values (9, 'Maple West ES');

-- Fairfax, VA
Insert into [School] (CountyId, Name) values (10, 'Academy of Christian Education (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Ad Fontes (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Agape Christian (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Al Fatih (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Aldrin ES');
Insert into [School] (CountyId, Name) values (10, 'Alexandria Country Day (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Ambleside (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Angelus (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Annandale Terrace ES');
Insert into [School] (CountyId, Name) values (10, 'Apple Tree (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Aquinas Montessori (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Armstrong ES');
Insert into [School] (CountyId, Name) values (10, 'Baileys ES');
Insert into [School] (CountyId, Name) values (10, 'Beech Tree ES');
Insert into [School] (CountyId, Name) values (10, 'Belle View ES');
Insert into [School] (CountyId, Name) values (10, 'Belvedere ES');
Insert into [School] (CountyId, Name) values (10, 'Blessed Sacrament');
Insert into [School] (CountyId, Name) values (10, 'Bonnie Brae ES');
Insert into [School] (CountyId, Name) values (10, 'Braddock ES');
Insert into [School] (CountyId, Name) values (10, 'Bren Mar Park ES');
Insert into [School] (CountyId, Name) values (10, 'Brentwood (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Brookfield ES');
Insert into [School] (CountyId, Name) values (10, 'Brooksfield (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Browne (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Bucknell ES');
Insert into [School] (CountyId, Name) values (10, 'Bull Run ES');
Insert into [School] (CountyId, Name) values (10, 'Burgundy Farm Country Day (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Bush Hill ES');
Insert into [School] (CountyId, Name) values (10, 'Calvary Road Christian (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Camelot ES');
Insert into [School] (CountyId, Name) values (10, 'Cameron ES');
Insert into [School] (CountyId, Name) values (10, 'Canterbury Woods ES');
Insert into [School] (CountyId, Name) values (10, 'Cardinal Forest ES');
Insert into [School] (CountyId, Name) values (10, 'Centre Ridge ES');
Insert into [School] (CountyId, Name) values (10, 'Centreville ES');
Insert into [School] (CountyId, Name) values (10, 'Cherry Run ES');
Insert into [School] (CountyId, Name) values (10, 'Chesapeake Academy (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Chesterbrook (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Chesterbrook ES');
Insert into [School] (CountyId, Name) values (10, 'Childrens House Montessori (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Christian Center School, Inc (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Churchill Road ES');
Insert into [School] (CountyId, Name) values (10, 'Clearview ES');
Insert into [School] (CountyId, Name) values (10, 'Clermont ES');
Insert into [School] (CountyId, Name) values (10, 'Clifton ES');
Insert into [School] (CountyId, Name) values (10, 'Colin Powell ES');
Insert into [School] (CountyId, Name) values (10, 'Columbia ES');
Insert into [School] (CountyId, Name) values (10, 'Colvin Run ES');
Insert into [School] (CountyId, Name) values (10, 'Community Montessori (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Congressional School of Virginia (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Corpus Christi (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Corpus Christi Early Childhood Center (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Crestwood ES');
Insert into [School] (CountyId, Name) values (10, 'Crossfield ES');
Insert into [School] (CountyId, Name) values (10, 'Cub Run ES');
Insert into [School] (CountyId, Name) values (10, 'Cunningham Park ES');
Insert into [School] (CountyId, Name) values (10, 'Daniels Run ES');
Insert into [School] (CountyId, Name) values (10, 'Deer Park ES');
Insert into [School] (CountyId, Name) values (10, 'Dogwood ES');
Insert into [School] (CountyId, Name) values (10, 'Dominion Christian (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Dranesville ES');
Insert into [School] (CountyId, Name) values (10, 'Dulles Montessori School (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Eagle View ES');
Insert into [School] (CountyId, Name) values (10, 'Early Years Montessori (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Edlin (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Elfland (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Engleside Christian (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Fairfax Baptist Temple (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Fairfax Christian (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Fairfax Villa ES');
Insert into [School] (CountyId, Name) values (10, 'Fairhill ES');
Insert into [School] (CountyId, Name) values (10, 'Fairview ES');
Insert into [School] (CountyId, Name) values (10, 'Flint Hill (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Flint Hill ES');
Insert into [School] (CountyId, Name) values (10, 'Floris ES');
Insert into [School] (CountyId, Name) values (10, 'Forest Edge ES');
Insert into [School] (CountyId, Name) values (10, 'Forestdale ES');
Insert into [School] (CountyId, Name) values (10, 'Forestville ES');
Insert into [School] (CountyId, Name) values (10, 'Fort Belvoir ES');
Insert into [School] (CountyId, Name) values (10, 'Fort Hunt ES');
Insert into [School] (CountyId, Name) values (10, 'Fox Mill ES');
Insert into [School] (CountyId, Name) values (10, 'Franconia ES');
Insert into [School] (CountyId, Name) values (10, 'Franklin Sherman ES');
Insert into [School] (CountyId, Name) values (10, 'Freedom Hill ES');
Insert into [School] (CountyId, Name) values (10, 'Garfield ES');
Insert into [School] (CountyId, Name) values (10, 'Gesher Jewish Day');
Insert into [School] (CountyId, Name) values (10, 'Glen Forest ES');
Insert into [School] (CountyId, Name) values (10, 'Good Beginnings (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Grace Episcopal (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Grace Lutheran (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Graham Road ES');
Insert into [School] (CountyId, Name) values (10, 'Grasshopper Green/ Kenwood (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Great Falls ES');
Insert into [School] (CountyId, Name) values (10, 'Green Hedges (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Greenbriar East ES');
Insert into [School] (CountyId, Name) values (10, 'Greenbriar West ES');
Insert into [School] (CountyId, Name) values (10, 'Groveton ES');
Insert into [School] (CountyId, Name) values (10, 'Gunston ES');
Insert into [School] (CountyId, Name) values (10, 'Halley ES');
Insert into [School] (CountyId, Name) values (10, 'Haycock ES');
Insert into [School] (CountyId, Name) values (10, 'Hayfield ES');
Insert into [School] (CountyId, Name) values (10, 'Herndon ES');
Insert into [School] (CountyId, Name) values (10, 'Hollin Meadows ES');
Insert into [School] (CountyId, Name) values (10, 'Holy Spirit Catholic (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Hope Montessori (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Hunt Valley ES');
Insert into [School] (CountyId, Name) values (10, 'Hunters Woods ES');
Insert into [School] (CountyId, Name) values (10, 'Hutchison ES');
Insert into [School] (CountyId, Name) values (10, 'Hybla Valley ES');
Insert into [School] (CountyId, Name) values (10, 'Immanuel Christian (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Islamic Saudi (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Island Creek ES');
Insert into [School] (CountyId, Name) values (10, 'Keene Mill ES');
Insert into [School] (CountyId, Name) values (10, 'Kent Gardens ES');
Insert into [School] (CountyId, Name) values (10, 'Kings Glen ES');
Insert into [School] (CountyId, Name) values (10, 'Kings Park ES');
Insert into [School] (CountyId, Name) values (10, 'Lake Anne ES');
Insert into [School] (CountyId, Name) values (10, 'Lake Anne Nursery Kindergarten');
Insert into [School] (CountyId, Name) values (10, 'Lane ES');
Insert into [School] (CountyId, Name) values (10, 'Langley (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Laurel Ridge ES');
Insert into [School] (CountyId, Name) values (10, 'Lees Corner ES');
Insert into [School] (CountyId, Name) values (10, 'Lemon Road ES');
Insert into [School] (CountyId, Name) values (10, 'Little Flock Christian (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Little Run ES');
Insert into [School] (CountyId, Name) values (10, 'London Towne ES');
Insert into [School] (CountyId, Name) values (10, 'Lorien Wood (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Lorton Station ES');
Insert into [School] (CountyId, Name) values (10, 'Louise Archer ES');
Insert into [School] (CountyId, Name) values (10, 'Lutie Lewis Coates ES');
Insert into [School] (CountyId, Name) values (10, 'Lynbrook ES');
Insert into [School] (CountyId, Name) values (10, 'Mantua ES');
Insert into [School] (CountyId, Name) values (10, 'Marshall Road ES');
Insert into [School] (CountyId, Name) values (10, 'McNair ES');
Insert into [School] (CountyId, Name) values (10, 'Merritt (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Montessori Country (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Montessori School of Alexandria (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Montessori School of Fairfax (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Montessori School of Herndon (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Montessori School of Holmes Run (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Montessori School of McLean (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Montessori School of Northern Virginia (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Mosby Woods ES');
Insert into [School] (CountyId, Name) values (10, 'Mount Eagle ES');
Insert into [School] (CountyId, Name) values (10, 'Mount Pleasant Baptist Church Christian (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Mount Vernon Woods ES');
Insert into [School] (CountyId, Name) values (10, 'Nativity (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Navy ES');
Insert into [School] (CountyId, Name) values (10, 'New School of Northern Virginia (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Newington Forest ES');
Insert into [School] (CountyId, Name) values (10, 'North Springfield ES');
Insert into [School] (CountyId, Name) values (10, 'Northern Virginia Christian (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Nysmith School for the Gifted (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Oak Hill Christian (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Oak Hill ES');
Insert into [School] (CountyId, Name) values (10, 'Oak View ES');
Insert into [School] (CountyId, Name) values (10, 'Oakton ES');
Insert into [School] (CountyId, Name) values (10, 'Oakwood (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Olde Creek ES');
Insert into [School] (CountyId, Name) values (10, 'Orange Hunt ES');
Insert into [School] (CountyId, Name) values (10, 'Our Lady of Good Counsel (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Our Savior Lutheran (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Parklawn ES');
Insert into [School] (CountyId, Name) values (10, 'Pine Spring ES');
Insert into [School] (CountyId, Name) values (10, 'Pinecrest (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Pinnacle Academy (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Poplar Tree ES');
Insert into [School] (CountyId, Name) values (10, 'Powell ES');
Insert into [School] (CountyId, Name) values (10, 'Prince of Peace Lutheran (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Providence ES');
Insert into [School] (CountyId, Name) values (10, 'Queen of Apostles (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Ravensworth ES');
Insert into [School] (CountyId, Name) values (10, 'Reston Day School (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Reston Montessori (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Ridgemont Montessori (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Riverside ES');
Insert into [School] (CountyId, Name) values (10, 'Rolling Valley ES');
Insert into [School] (CountyId, Name) values (10, 'Rose Hill ES');
Insert into [School] (CountyId, Name) values (10, 'Saint Agnes (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Saint Timothy (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Sangster ES');
Insert into [School] (CountyId, Name) values (10, 'Saratoga ES');
Insert into [School] (CountyId, Name) values (10, 'Shrevewood ES');
Insert into [School] (CountyId, Name) values (10, 'Silverbrook ES');
Insert into [School] (CountyId, Name) values (10, 'Sleepy Hollow ES');
Insert into [School] (CountyId, Name) values (10, 'Spring Hill ES');
Insert into [School] (CountyId, Name) values (10, 'Springfield (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Springfield Estates ES');
Insert into [School] (CountyId, Name) values (10, 'Springs Montessori (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Ambrose (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Andrew the Apostle Catholic (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Ann (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Bernadette (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Charles (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. James Catholic (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. John (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Joseph (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Joseph Parish (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Josephs Day (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Leos Catholic Church (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Louis (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Luke (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Mark (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Marks Lutheran Church Montessori (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Marys Catholic (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Michaels Catholic (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Rita Catholic (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Stephens & St. Agnes (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Thomas More Cathedral (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'St. Veronica Catholic (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Stenwood ES');
Insert into [School] (CountyId, Name) values (10, 'Stratford Landing ES');
Insert into [School] (CountyId, Name) values (10, 'Summit Christian (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Sunrise Valley ES');
Insert into [School] (CountyId, Name) values (10, 'Sunset Hills Montessori Childrens House (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Sydenstricker (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Temple Baptist (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Terra Centre ES');
Insert into [School] (CountyId, Name) values (10, 'Terraset ES');
Insert into [School] (CountyId, Name) values (10, 'The Academy of Christian Education (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'The Boyd (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'The Embassy (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'The Leary School (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'The Potomac (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Timber Lane ES');
Insert into [School] (CountyId, Name) values (10, 'Town & Country (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Trinity Christian Church (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Union Mill ES');
Insert into [School] (CountyId, Name) values (10, 'Vienna ES');
Insert into [School] (CountyId, Name) values (10, 'Virginia Run ES');
Insert into [School] (CountyId, Name) values (10, 'Wakefield Forest ES');
Insert into [School] (CountyId, Name) values (10, 'Waples Mill ES');
Insert into [School] (CountyId, Name) values (10, 'Washington Islamic (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Washington Mill ES');
Insert into [School] (CountyId, Name) values (10, 'Way of Faith (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Way of Faith Christian Academy (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Waynewood ES');
Insert into [School] (CountyId, Name) values (10, 'West Springfield ES');
Insert into [School] (CountyId, Name) values (10, 'Westbriar ES');
Insert into [School] (CountyId, Name) values (10, 'Westgate ES');
Insert into [School] (CountyId, Name) values (10, 'Westlawn ES');
Insert into [School] (CountyId, Name) values (10, 'Westminster (Pvt)');
Insert into [School] (CountyId, Name) values (10, 'Weyanoke ES');
Insert into [School] (CountyId, Name) values (10, 'White Oaks ES');
Insert into [School] (CountyId, Name) values (10, 'Willow Springs ES');
Insert into [School] (CountyId, Name) values (10, 'Wolftrap ES');
Insert into [School] (CountyId, Name) values (10, 'Woodburn ES');
Insert into [School] (CountyId, Name) values (10, 'Woodlawn ES');
Insert into [School] (CountyId, Name) values (10, 'Woodley Hills ES');
Insert into [School] (CountyId, Name) values (10, 'Word of Life Christian (Pvt)');

-- Fairfield, CT
Insert into [School] (CountyId, Name) values (11, 'Akron MS');
Insert into [School] (CountyId, Name) values (11, 'Alden MS');
Insert into [School] (CountyId, Name) values (11, 'Ben Franklin ES');
Insert into [School] (CountyId, Name) values (11, 'Clarence Center ES');
Insert into [School] (CountyId, Name) values (11, 'Community CS');
Insert into [School] (CountyId, Name) values (11, 'Depew MS');
Insert into [School] (CountyId, Name) values (11, 'Discovery School');
Insert into [School] (CountyId, Name) values (11, 'East Aurora MS');
Insert into [School] (CountyId, Name) values (11, 'Elizabeth Shelton ES');
Insert into [School] (CountyId, Name) values (11, 'Enterprise CS');
Insert into [School] (CountyId, Name) values (11, 'Glendale ES');
Insert into [School] (CountyId, Name) values (11, 'Global Concepts CS');
Insert into [School] (CountyId, Name) values (11, 'Grabiarz School Of Excellence');
Insert into [School] (CountyId, Name) values (11, 'Hamburg MS');
Insert into [School] (CountyId, Name) values (11, 'Harvey Austin School #97');
Insert into [School] (CountyId, Name) values (11, 'Kenmore MS');
Insert into [School] (CountyId, Name) values (11, 'Martin Road ES');
Insert into [School] (CountyId, Name) values (11, 'Maryvale Intermediate School');
Insert into [School] (CountyId, Name) values (11, 'Native American Magnet ES');
Insert into [School] (CountyId, Name) values (11, 'Parkdale ES');
Insert into [School] (CountyId, Name) values (11, 'Potters Road School');
Insert into [School] (CountyId, Name) values (11, 'Riverview ES');
Insert into [School] (CountyId, Name) values (11, 'South Buffalo CS');
Insert into [School] (CountyId, Name) values (11, 'Tapestry CS');
Insert into [School] (CountyId, Name) values (11, 'Theodore Roosevelt School');
Insert into [School] (CountyId, Name) values (11, 'William Street School');
Insert into [School] (CountyId, Name) values (11, 'William T Hoag ES');

-- FallsChurchCity, VA
Insert into [School] (CountyId, Name) values (12, 'Mt. Daniel ES');
Insert into [School] (CountyId, Name) values (12, 'Thomas Jefferson ES');

-- Fauquier, VA
Insert into [School] (CountyId, Name) values (13, 'Fresta Valley Christian School (Pvt)');

-- FortBend, TX
Insert into [School] (CountyId, Name) values (14, 'Bonnie Holland ES');

-- Frederic, MD
Insert into [School] (CountyId, Name) values (15, 'Urbana ES');

-- GarnetValley, PA
Insert into [School] (CountyId, Name) values (16, 'Concord ES');
Insert into [School] (CountyId, Name) values (16, 'Hanover Academy School (Pvt)');

-- Hanover, VA
Insert into [School] (CountyId, Name) values (17, 'Concord ES');

-- Henrico, PA
Insert into [School] (CountyId, Name) values (18, 'Colonial Trail ES');
Insert into [School] (CountyId, Name) values (18, 'Richmond Montessori (Pvt)');
Insert into [School] (CountyId, Name) values (18, 'Rivers Edge ES');
Insert into [School] (CountyId, Name) values (18, 'Shady Grove ES');
Insert into [School] (CountyId, Name) values (18, 'Three Chopt ES');

-- Hillsborough, FL
Insert into [School] (CountyId, Name) values (19, 'Brandon Academy (Pvt)');

-- Johnson, KS
Insert into [School] (CountyId, Name) values (20, 'Lakewood ES');
Insert into [School] (CountyId, Name) values (20, 'Morse ES');
Insert into [School] (CountyId, Name) values (20, 'Regency Place ES');

-- Loudoun, VA
Insert into [School] (CountyId, Name) values (21, 'Academy of Christian Education (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Aldie ES');
Insert into [School] (CountyId, Name) values (21, 'Algonkian ES');
Insert into [School] (CountyId, Name) values (21, 'Arcola ES');
Insert into [School] (CountyId, Name) values (21, 'Ashburn ES');
Insert into [School] (CountyId, Name) values (21, 'Balls Bluff ES');
Insert into [School] (CountyId, Name) values (21, 'Banneker ES');
Insert into [School] (CountyId, Name) values (21, 'BeanTree Learning-Ashburn (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'BeanTree Learning-Chantilly (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Belmont Station ES');
Insert into [School] (CountyId, Name) values (21, 'Buffalo Trail ES');
Insert into [School] (CountyId, Name) values (21, 'Catoctin ES');
Insert into [School] (CountyId, Name) values (21, 'Cedar Lane ES');
Insert into [School] (CountyId, Name) values (21, 'Chesterbrook Academy (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Chesterbrook Academy ES(Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Christian Faith & Fellowship (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Cool Spring ES');
Insert into [School] (CountyId, Name) values (21, 'Countryside ES');
Insert into [School] (CountyId, Name) values (21, 'Creightons Corner ES');
Insert into [School] (CountyId, Name) values (21, 'Creme De La Creme (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Dominion Academy (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Dominion Trail ES');
Insert into [School] (CountyId, Name) values (21, 'Emerick ES');
Insert into [School] (CountyId, Name) values (21, 'Evergreen Mill ES');
Insert into [School] (CountyId, Name) values (21, 'Faith Christian (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Forest Grove ES');
Insert into [School] (CountyId, Name) values (21, 'Frances Hazel Reid ES');
Insert into [School] (CountyId, Name) values (21, 'Good Beginnings School (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Graydon Manor (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Guilford ES');
Insert into [School] (CountyId, Name) values (21, 'Hamilton ES');
Insert into [School] (CountyId, Name) values (21, 'Hillsboro ES');
Insert into [School] (CountyId, Name) values (21, 'Hillside ES');
Insert into [School] (CountyId, Name) values (21, 'Horizon ES');
Insert into [School] (CountyId, Name) values (21, 'Hutchison Farm ES');
Insert into [School] (CountyId, Name) values (21, 'John W. Tolbert Jr. ES');
Insert into [School] (CountyId, Name) values (21, 'Leesburg Christian (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Leesburg ES');
Insert into [School] (CountyId, Name) values (21, 'Legacy ES');
Insert into [School] (CountyId, Name) values (21, 'Liberty ES');
Insert into [School] (CountyId, Name) values (21, 'Lincoln ES');
Insert into [School] (CountyId, Name) values (21, 'Little River ES');
Insert into [School] (CountyId, Name) values (21, 'Loudoun Country Day (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Lovettsville ES');
Insert into [School] (CountyId, Name) values (21, 'Lowes Island ES');
Insert into [School] (CountyId, Name) values (21, 'Lucketts ES');
Insert into [School] (CountyId, Name) values (21, 'Meadowland ES');
Insert into [School] (CountyId, Name) values (21, 'Middleburg ES');
Insert into [School] (CountyId, Name) values (21, 'Mill Run ES');
Insert into [School] (CountyId, Name) values (21, 'Montessori Childrens House of Loudoun (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Montessori School of Leesburg (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Mountain View ES');
Insert into [School] (CountyId, Name) values (21, 'Munger Academy (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Newton-Lee ES');
Insert into [School] (CountyId, Name) values (21, 'Our Lady of Hope Catholic School (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Pinebrook ES');
Insert into [School] (CountyId, Name) values (21, 'Potowmack ES');
Insert into [School] (CountyId, Name) values (21, 'Rolling Ridge ES');
Insert into [School] (CountyId, Name) values (21, 'Rosa Lee Carter ES');
Insert into [School] (CountyId, Name) values (21, 'Round Hill ES');
Insert into [School] (CountyId, Name) values (21, 'Sanders Corner ES');
Insert into [School] (CountyId, Name) values (21, 'Seldens Landing ES');
Insert into [School] (CountyId, Name) values (21, 'South Riding Montessori Center (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'St. Theresa School (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Sterling ES');
Insert into [School] (CountyId, Name) values (21, 'Steuart Weller ES');
Insert into [School] (CountyId, Name) values (21, 'Sugarland ES');
Insert into [School] (CountyId, Name) values (21, 'Sully ES');
Insert into [School] (CountyId, Name) values (21, 'Sycolin Creek ES');
Insert into [School] (CountyId, Name) values (21, 'The Boyd School (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'The Hill School (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Virginia Academy-Ashburn (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Virginia Academy-Sterling (Pvt)');
Insert into [School] (CountyId, Name) values (21, 'Waterford ES');

-- ManassasParkCity, VA
Insert into [School] (CountyId, Name) values (22, 'ConcManassas Parkord ES');

-- Mecklenburg, NC
Insert into [School] (CountyId, Name) values (23, 'Barringer Academic Center');

-- Mercer, NJ
Insert into [School] (CountyId, Name) values (24, 'Maurice Hawk ES');

-- Middlesex, NJ
Insert into [School] (CountyId, Name) values (25, 'Menlo Park ES');
Insert into [School] (CountyId, Name) values (25, 'Randolphville ES');

-- Montgomery, MD
Insert into [School] (CountyId, Name) values (26, 'College Gardens ES'); 
Insert into [School] (CountyId, Name) values (26, 'Diamond ES'); 
Insert into [School] (CountyId, Name) values (26, 'Garrett Park ES'); 
Insert into [School] (CountyId, Name) values (26, 'Glen Heaven ES'); 
Insert into [School] (CountyId, Name) values (26, 'Kensington Parkwood ES'); 
Insert into [School] (CountyId, Name) values (26, 'Takoma Park ES'); 
Insert into [School] (CountyId, Name) values (26, 'Wayside ES'); 
Insert into [School] (CountyId, Name) values (26, 'Westbrook ES'); 
Insert into [School] (CountyId, Name) values (26, 'William B. Gibbs Jr. ES');

-- Morris, NJ
Insert into [School] (CountyId, Name) values (27, 'Littleton ES');

-- Nassau, NJ
Insert into [School] (CountyId, Name) values (28, 'Denton Avenue ES');

-- PrinceWilliam, VA
Insert into [School] (CountyId, Name) values (29, 'Academy (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'All Saints Catholic (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Alvey ES');
Insert into [School] (CountyId, Name) values (29, 'Antietam ES');
Insert into [School] (CountyId, Name) values (29, 'Aquinas (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Ashland ES');
Insert into [School] (CountyId, Name) values (29, 'Bel Air ES');
Insert into [School] (CountyId, Name) values (29, 'Belmont ES');
Insert into [School] (CountyId, Name) values (29, 'Bennett ES');
Insert into [School] (CountyId, Name) values (29, 'Bristow Run ES');
Insert into [School] (CountyId, Name) values (29, 'Buckland Mills ES');
Insert into [School] (CountyId, Name) values (29, 'Cardinal Montessori (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Cedar Point ES');
Insert into [School] (CountyId, Name) values (29, 'Christ Chapel Academy (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Clairmont (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Cloverdale (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Coles ES');
Insert into [School] (CountyId, Name) values (29, 'Dale City Christian Church (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Dale City ES');
Insert into [School] (CountyId, Name) values (29, 'Dumfries ES');
Insert into [School] (CountyId, Name) values (29, 'Ellis ES');
Insert into [School] (CountyId, Name) values (29, 'Emmanuel Christian (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Enterprise ES');
Insert into [School] (CountyId, Name) values (29, 'Evangel Christian (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Fairmont Christian Preparatory (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Featherstone ES');
Insert into [School] (CountyId, Name) values (29, 'First Steps (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Fitzgerald ES');
Insert into [School] (CountyId, Name) values (29, 'Glenkirk ES');
Insert into [School] (CountyId, Name) values (29, 'Gravely ES');
Insert into [School] (CountyId, Name) values (29, 'Henderson ES');
Insert into [School] (CountyId, Name) values (29, 'Heritage Christian (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Highland School (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Jewels And Jems (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Kerrydale ES');
Insert into [School] (CountyId, Name) values (29, 'Kilby ES');
Insert into [School] (CountyId, Name) values (29, 'King ES');
Insert into [School] (CountyId, Name) values (29, 'La Petite Academy (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Lake Ridge ES');
Insert into [School] (CountyId, Name) values (29, 'Leesylvania ES');
Insert into [School] (CountyId, Name) values (29, 'Linton Hall (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Loch Lomond ES');
Insert into [School] (CountyId, Name) values (29, 'Marshall ES');
Insert into [School] (CountyId, Name) values (29, 'Marumsco Hills ES');
Insert into [School] (CountyId, Name) values (29, 'McAuliffe ES');
Insert into [School] (CountyId, Name) values (29, 'Minnieland (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Minnieville ES');
Insert into [School] (CountyId, Name) values (29, 'Montclair ES');
Insert into [School] (CountyId, Name) values (29, 'Mountain View ES');
Insert into [School] (CountyId, Name) values (29, 'Mullen ES');
Insert into [School] (CountyId, Name) values (29, 'Neabsco ES');
Insert into [School] (CountyId, Name) values (29, 'Nokesville ES');
Insert into [School] (CountyId, Name) values (29, 'Occoquan ES');
Insert into [School] (CountyId, Name) values (29, 'Old Bridge ES');
Insert into [School] (CountyId, Name) values (29, 'Pattie ES');
Insert into [School] (CountyId, Name) values (29, 'Penn ES');
Insert into [School] (CountyId, Name) values (29, 'Pennington ES');
Insert into [School] (CountyId, Name) values (29, 'Potomac View ES');
Insert into [School] (CountyId, Name) values (29, 'Prince William Academy (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'River Oaks ES');
Insert into [School] (CountyId, Name) values (29, 'Rockledge ES');
Insert into [School] (CountyId, Name) values (29, 'Rosa Parks ES');
Insert into [School] (CountyId, Name) values (29, 'Sancta Lucia (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Signal Hill ES');
Insert into [School] (CountyId, Name) values (29, 'Sinclair ES');
Insert into [School] (CountyId, Name) values (29, 'Springwoods ES');
Insert into [School] (CountyId, Name) values (29, 'St Francis (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'St Pauls School (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Sudley ES');
Insert into [School] (CountyId, Name) values (29, 'Swans Creek ES');
Insert into [School] (CountyId, Name) values (29, 'Tabernacle Baptist Academy (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Triangle ES');
Insert into [School] (CountyId, Name) values (29, 'Trinity Temple (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Tyler ES');
Insert into [School] (CountyId, Name) values (29, 'Vaughan ES');
Insert into [School] (CountyId, Name) values (29, 'Victory ES');
Insert into [School] (CountyId, Name) values (29, 'West Gate ES');
Insert into [School] (CountyId, Name) values (29, 'Westridge ES');
Insert into [School] (CountyId, Name) values (29, 'Williams ES');
Insert into [School] (CountyId, Name) values (29, 'Wonderland (Pvt)');
Insert into [School] (CountyId, Name) values (29, 'Yorkshire ES');

-- Richmond, VA
Insert into [School] (CountyId, Name) values (30, 'St Bridget School (Pvt)');

-- Wake, NC
Insert into [School] (CountyId, Name) values (31, 'Laurel Park ES');
Insert into [School] (CountyId, Name) values (31, 'Thales Academy (Pvt)');

-- Will, IL
Insert into [School] (CountyId, Name) values (32, 'Indian Trail ES');


Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('GRADE_K', 'GradeType', 'K-Level', 1)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('GRADE_1', 'GradeType', '1st Grade', 2)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('GRADE_2', 'GradeType', '2nd Grade', 2)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('GRADE_3', 'GradeType', '3rd Grade', 3)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('GRADE_4', 'GradeType', '4th Grade', 4)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('GRADE_5', 'GradeType', '5th Grade', 5)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('GRADE_6', 'GradeType', '6th Grade', 6)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('GRADE_7', 'GradeType', '7th Grade', 7)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('GRADE_8', 'GradeType', '8th Grade', 8)


Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('JrBeeSpeller', 'PatronType', 'Speller-Jr.Bee (K-Gr4)', 1)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('OlympiadSpeller', 'PatronType', 'Speller-Olympiad(Gr5-Gr8)', 2)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('Parent', 'PatronType', 'Parent/Guardian', 3)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('Infant', 'PatronType', 'Infant', 4)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('KidSpectator', 'PatronType', 'Kid Spectator', 5)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('OtherSpectator', 'PatronType', 'Other Spectator', 6)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('Adult', 'PatronType', 'Adult', 1)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('Youth', 'PatronType', 'Youth (11 - 17 Years)', 2)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('Child', 'PatronType', 'Child (2-10 Years)', 3)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('Infant-1', 'PatronType', 'Infant (below 3 Years)', 4)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_0_2', 'AgeType', '0-2 Years', 1)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_3', 'AgeType', '3 Years', 1)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_4', 'AgeType', '4 Years', 2)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_5', 'AgeType', '5 Years', 3)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_6', 'AgeType', '6 Years', 4)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_7', 'AgeType', '7 Years', 5)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_8', 'AgeType', '8 Years', 6)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_9', 'AgeType', '9 Years', 7)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_10', 'AgeType', '10 Years', 8)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_11', 'AgeType', '11 Years', 9)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_12', 'AgeType', '12 Years', 10)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_13', 'AgeType', '13 Years', 11)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_14', 'AgeType', '14 Years', 12)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_15', 'AgeType', '15 Years', 13)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_16', 'AgeType', '16 Years', 14)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_17', 'AgeType', '17 Years', 15)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_18', 'AgeType', '18 Years', 16)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_18_PLUS', 'AgeType', '>=18 Years', 17)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('AGE_1', 'AgeType', '<=2 Years', 18)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('REGINIT', 'RegistrationStatus', 'Initiated', 1)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('REGCOMPLETE', 'RegistrationStatus', 'Completed', 2)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('REGCANCELLED', 'RegistrationStatus', 'Cancelled', 3)

Insert into [ReferenceCode]
(Code, Category, Description, DisplayOrder)
values 
('REGREFUNDED', 'RegistrationStatus', 'Refunded', 4)



Insert into [EventType]
(EventTypeCode, Name, Description)
values('MASTISPELL', 'MastiSpell', 'Junior Spelling Bee Competition')

Insert into [EventType]
(EventTypeCode, Name, Description)
values('MASTIFEST', 'MastiFest', 'Fun filled cultural dance event')


Insert into [EventTypePatronType]
(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
values
(1, 'MASTISPELL', 'JrBeeSpeller', 1)

--Insert into [EventTypePatronType]
--(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
--values
--(2, 'MASTISPELL', 'OlympiadSpeller', 2)

Insert into [EventTypePatronType]
(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
values
(3, 'MASTISPELL', 'Parent', 3)

Insert into [EventTypePatronType]
(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
values
(4, 'MASTISPELL', 'Infant', 4)

Insert into [EventTypePatronType]
(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
values
(5, 'MASTISPELL', 'KidSpectator', 5)

Insert into [EventTypePatronType]
(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
values
(6, 'MASTISPELL', 'OtherSpectator', 6)

Insert into [EventTypePatronType]
(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
values
(7, 'MASTIFEST', 'Adult', 1)

Insert into [EventTypePatronType]
(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
values
(8, 'MASTIFEST', 'Youth', 2)

Insert into [EventTypePatronType]
(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
values
(9, 'MASTIFEST', 'Child', 3)

Insert into [EventTypePatronType]
(EventTypePatronTypeId, EventTypeCode, PatronTypeCode, DisplayOrder)
values
(10, 'MASTIFEST', 'Infant', 4)


Insert into [PatronTypeGradeType]
(PatronTypeGradeTypeId, PatronTypeCode, GradeTypeCode, DisplayOrder)
values
(1, 'JrBeeSpeller', 'GRADE_K', 1)

Insert into [PatronTypeGradeType]
(PatronTypeGradeTypeId, PatronTypeCode, GradeTypeCode, DisplayOrder)
values
(2, 'JrBeeSpeller', 'GRADE_1', 2)

Insert into [PatronTypeGradeType]
(PatronTypeGradeTypeId, PatronTypeCode, GradeTypeCode, DisplayOrder)
values
(3, 'JrBeeSpeller', 'GRADE_2', 3)

Insert into [PatronTypeGradeType]
(PatronTypeGradeTypeId, PatronTypeCode, GradeTypeCode, DisplayOrder)
values
(4, 'JrBeeSpeller', 'GRADE_3', 4)

Insert into [PatronTypeGradeType]
(PatronTypeGradeTypeId, PatronTypeCode, GradeTypeCode, DisplayOrder)
values
(5, 'JrBeeSpeller', 'GRADE_4', 5)

Insert into [PatronTypeGradeType]
(PatronTypeGradeTypeId, PatronTypeCode, GradeTypeCode, DisplayOrder)
values
(6, 'OlympiadSpeller', 'GRADE_5', 1)

Insert into [PatronTypeGradeType]
(PatronTypeGradeTypeId, PatronTypeCode, GradeTypeCode, DisplayOrder)
values
(7, 'OlympiadSpeller', 'GRADE_6', 2)

Insert into [PatronTypeGradeType]
(PatronTypeGradeTypeId, PatronTypeCode, GradeTypeCode, DisplayOrder)
values
(8, 'OlympiadSpeller', 'GRADE_7', 3)

Insert into [PatronTypeGradeType]
(PatronTypeGradeTypeId, PatronTypeCode, GradeTypeCode, DisplayOrder)
values
(9, 'OlympiadSpeller', 'GRADE_8', 4)


/**** PatronTypeAgeType **************/
Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(1, 'JrBeeSpeller', 'AGE_4', 1)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(2, 'JrBeeSpeller', 'AGE_5', 2)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(3, 'JrBeeSpeller', 'AGE_6', 3)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(4, 'JrBeeSpeller', 'AGE_7', 4)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(5, 'JrBeeSpeller', 'AGE_8', 5)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(6, 'JrBeeSpeller', 'AGE_9', 6)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(7, 'JrBeeSpeller', 'AGE_10', 7)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(8, 'JrBeeSpeller', 'AGE_11', 8)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(9, 'OlympiadSpeller', 'AGE_8', 1)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(10, 'OlympiadSpeller', 'AGE_9', 2)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(11, 'OlympiadSpeller', 'AGE_10', 3)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(12, 'OlympiadSpeller', 'AGE_11', 4)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(13, 'OlympiadSpeller', 'AGE_12', 5)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(14, 'OlympiadSpeller', 'AGE_13', 6)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(15, 'OlympiadSpeller', 'AGE_14', 7)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(16, 'Parent', 'AGE_18_PLUS', 1)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(17, 'Infant', 'AGE_0_2', 1)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(18, 'KidSpectator', 'AGE_3', 1)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(19, 'KidSpectator', 'AGE_4', 2)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(20, 'KidSpectator', 'AGE_5', 3)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(21, 'KidSpectator', 'AGE_6', 4)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(22, 'KidSpectator', 'AGE_7', 5)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(23, 'KidSpectator', 'AGE_8', 6)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(24, 'KidSpectator', 'AGE_9', 7)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(25, 'KidSpectator', 'AGE_10', 8)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(26, 'KidSpectator', 'AGE_11', 9)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(27, 'KidSpectator', 'AGE_12', 10)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(28, 'KidSpectator', 'AGE_13', 11)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(29, 'KidSpectator', 'AGE_14', 12)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(30, 'KidSpectator', 'AGE_15', 13)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(31, 'KidSpectator', 'AGE_16', 14)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(32, 'KidSpectator', 'AGE_17', 15)

Insert into [PatronTypeAgeType]
(PatronTypeAgeTypeId, PatronTypeCode, AgeTypeCode, DisplayOrder)
values
(33, 'OtherSpectator', 'AGE_18_PLUS', 1)

/******************************************************************************/



/******************************************************************************/
/************** Event Data ******************/
/******************************************************************************/


SET IDENTITY_INSERT [Event] ON
Insert into [Event]
(EventId, Name, Description, EventTypeCode, EventDate, RegistrationStartDate, RegistrationEndDate, VenueName, VenueAddress, EarlyBirdOn)
values
(1, 'MastiSpell 2015', 'MastiSpell event for 2015', 'MASTISPELL', '05/27/2014', '01/01/2014', '05/15/2014', 'Stone Bridge High School', '123 Main Street, Ashburn VA, 20148', 1)

INSERT [dbo].[Event] ([EventId], [Name], [Description], [EventTypeCode], [EventDate], 
[RegistrationStartDate], [RegistrationEndDate], [VenueName], [VenueAddress], [EarlyBirdOn]) 
VALUES (2, N'MastiSpell 2016', N'MastiSpell Event for 2016', N'MASTISPELL', CAST(N'2015-05-27 00:00:00.000' AS DateTime), 
CAST(N'2015-01-01 00:00:00.000' AS DateTime), CAST(N'2015-05-15 00:00:00.000' AS DateTime), 
N'Stone Bridge High School', N'123 Main Street, Ashburn VA, 20148', 1)

INSERT [dbo].[Event] ([EventId], [Name], [Description], [EventTypeCode], [EventDate], 
[RegistrationStartDate], [RegistrationEndDate], [VenueName], [VenueAddress], [EarlyBirdOn]) 
VALUES (3, N'MastiFest 2016', N'MastiFest Event for 2016', N'MASTIFEST', CAST(N'2016-10-15 00:00:00.000' AS DateTime), 
CAST(N'2016-10-15 00:00:00.000' AS DateTime), CAST(N'2016-10-15 00:00:00.000' AS DateTime), 
N'Stone Bridge High School', N'123 Main Street, Ashburn VA, 20148', 1)

SET IDENTITY_INSERT [Event] OFF

SET IDENTITY_INSERT [RegistrationPrice] ON
INSERT INTO [dbo].[RegistrationPrice]
([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate])
VALUES
(1, 1, 'Parent', 25.00, 40.00, '10/01/2014', '05/01/2015')

INSERT INTO [dbo].[RegistrationPrice]
([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate])
VALUES
(2, 1, 'JrBeeSpeller', 100.00, 120.00, '10/01/2014', '05/01/2015')

INSERT INTO [dbo].[RegistrationPrice]
([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate])
VALUES
(3, 1, 'KidSpectator', 10.00, 10.00, '10/01/2014', '05/01/2015')

INSERT INTO [dbo].[RegistrationPrice]
([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate])
VALUES
(4, 1, 'OtherSpectator', 25.00, 40.00, '10/01/2014', '05/01/2015')

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (5, 2, N'Parent', 10.0000, 40.0000, CAST(N'2015-10-01 00:00:00.000' AS DateTime), CAST(N'2016-05-01 00:00:00.000' AS DateTime))

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (6, 2, N'JrBeeSpeller', 65.0000, 100.0000, CAST(N'2015-10-01 00:00:00.000' AS DateTime), CAST(N'2016-05-01 00:00:00.000' AS DateTime))

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (7, 2, N'KidSpectator', 5.0000, 20.0000, CAST(N'2015-10-01 00:00:00.000' AS DateTime), CAST(N'2016-05-01 00:00:00.000' AS DateTime))

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (8, 2, N'OtherSpectator', 10.0000, 40.0000, CAST(N'2015-10-01 00:00:00.000' AS DateTime), CAST(N'2016-05-01 00:00:00.000' AS DateTime))


INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (9, 3, N'Adult', 10.0000, 40.0000, CAST(N'2015-10-01 00:00:00.000' AS DateTime), CAST(N'2016-05-01 00:00:00.000' AS DateTime))

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (10, 3, N'Youth', 10.0000, 40.0000, CAST(N'2015-10-01 00:00:00.000' AS DateTime), CAST(N'2016-05-01 00:00:00.000' AS DateTime))

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (11, 3, N'Child', 10.0000, 40.0000, CAST(N'2015-10-01 00:00:00.000' AS DateTime), CAST(N'2016-05-01 00:00:00.000' AS DateTime))

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (12, 3, N'Infant-1', 0.0000, 0.0000, CAST(N'2015-10-01 00:00:00.000' AS DateTime), CAST(N'2016-05-01 00:00:00.000' AS DateTime))

SET IDENTITY_INSERT [RegistrationPrice] OFF

/**** MastiFest 2016 Event *****/
INSERT [dbo].[Event] ([EventId], [Name], [Description], [EventTypeCode], [EventDate], 
[RegistrationStartDate], [RegistrationEndDate], [VenueName], [VenueAddress], [EarlyBirdOn],
[MainMessage], [HomePageURL]) 
VALUES (3, N'MastiFest 2016', N'MastiFest Event for 2016', N'MASTIFEST', CAST(N'2016-10-22 00:00:00.000' AS DateTime), 
CAST(N'2016-09-19 00:00:00.000' AS DateTime), CAST(N'2016-10-22 00:00:00.000' AS DateTime), 
N'Tuscarora High School', N'801 N King St, Leesburg, VA 20176', 1,
'Limited Early Bird Prices!! -- Offered for first 100 registrants only!! Buy Online to save $$$.',
'http://www.mastifest.com')

SET IDENTITY_INSERT [Event] OFF

SET IDENTITY_INSERT [RegistrationPrice] ON

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (9, 3, N'Adult', 22.0000, 25.0000, CAST(N'2016-09-01 00:00:00.000' AS DateTime), CAST(N'2016-10-22 00:00:00.000' AS DateTime))

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (10, 3, N'Youth', 22.0000, 25.0000, CAST(N'2016-09-01 00:00:00.000' AS DateTime), CAST(N'2016-10-22 00:00:00.000' AS DateTime))

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (11, 3, N'Child', 14.0000, 18.0000, CAST(N'2016-09-01 00:00:00.000' AS DateTime), CAST(N'2016-10-22 00:00:00.000' AS DateTime))

INSERT [dbo].[RegistrationPrice] ([RegistrationPriceId], [EventId], [PatronTypeCode], [DiscountedAmount], [Amount], [StartDate], [EndDate]) 
VALUES (12, 3, N'Infant-1', 0.0000, 0.0000, CAST(N'2016-09-01 00:00:00.000' AS DateTime), CAST(N'2016-10-22 00:00:00.000' AS DateTime))

SET IDENTITY_INSERT [RegistrationPrice] OFF

