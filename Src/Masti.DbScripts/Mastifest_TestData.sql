﻿SET IDENTITY_INSERT [Registration] ON
Insert Into [Registration]
(RegistrationId, InvoiceNumber, EventId, Amount, MastiPaymentId, PmtVendorOrderid, PmtVendorCode, PaymentInitiationDate, PaymentCompleteDate, RegisteredDate, StatusCode, RefundedDate, AttendingSpellingBootcamp, ReferredBy)
Values
(1, NEWID(), 1, 100.00, 1, '123456ABC', 'GWALLET', '10/01/2014 06:00:00 AM', '10/01/2014 06:01:00 AM', '10/01/2014 06:01:00 AM', 'REGCOMPLETE', null, 1, 'Committee President')
SET IDENTITY_INSERT [Registration] OFF

SET IDENTITY_INSERT [Patron] ON
Insert into [Patron]
(PatronId, FirstName, MiddleName, LastName, Email, PatronTypeCode, AgeTypeCode,
SchoolId, GradeTypeCode, OtherSchool, Amount, RegistrationId)
values
(1, 'Isha', 'K', 'Kolhatkar', 'ishakolhatkar@gmail.com', 'JrBeeSpeller', 'AGE_10',  1, 'GRADE_1', NULL, 100.00, 1)

Insert into [Patron]
(PatronId, FirstName, MiddleName, LastName, Email, PatronTypeCode, AgeTypeCode,
SchoolId, GradeTypeCode, OtherSchool, Amount, RegistrationId)
values
(2, 'Kedar', 'H', 'Kolhatkar', 'kedar.kolhatkar@gmail.com', 'Parent', 'AGE_18_PLUS', NULL, NULL, NULL, 35.00, 1)
SET IDENTITY_INSERT [Patron] OFF






