IF OBJECT_ID('dbo.Logging', 'U') IS NOT NULL
	DROP TABLE [dbo].[Logging]
GO

IF OBJECT_ID('dbo.Patron', 'U') IS NOT NULL
	DROP TABLE [dbo].[Patron]
GO

IF OBJECT_ID('dbo.RegistrationPrice', 'U') IS NOT NULL
	DROP TABLE [dbo].[RegistrationPrice]
GO

IF OBJECT_ID('dbo.Registration', 'U') IS NOT NULL
	DROP TABLE [dbo].[Registration]
GO

IF OBJECT_ID('dbo.Event', 'U') IS NOT NULL
	DROP TABLE [dbo].[Event]
GO

IF OBJECT_ID('dbo.School', 'U') IS NOT NULL
	DROP TABLE [dbo].[School]
GO

IF OBJECT_ID('dbo.County', 'U') IS NOT NULL
	DROP TABLE [dbo].[County]
GO

IF OBJECT_ID('dbo.State', 'U') IS NOT NULL
	DROP TABLE [dbo].[State]
GO

IF OBJECT_ID('dbo.EventTypePatronType', 'U') IS NOT NULL
	DROP TABLE [dbo].[EventTypePatronType]
GO

IF OBJECT_ID('dbo.PatronTypeGradeType', 'U') IS NOT NULL
	DROP TABLE [dbo].[PatronTypeGradeType]
GO

IF OBJECT_ID('dbo.PatronTypeAgeType', 'U') IS NOT NULL
	DROP TABLE [dbo].[PatronTypeAgeType]
GO

IF OBJECT_ID('dbo.EventType', 'U') IS NOT NULL
	DROP TABLE [dbo].[EventType]
GO

IF OBJECT_ID('dbo.ReferenceCode', 'U') IS NOT NULL
	DROP TABLE [dbo].[ReferenceCode]
GO



/****** Object:  Table [dbo].[ReferenceCode]    Script Date: 10/20/2014 9:25:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReferenceCode](
	[Code] [nvarchar](20) NOT NULL,
	[Category] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[DisplayOrder] [int] NOT NULL,
 CONSTRAINT [PK_ReferenceCode] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[State]    Script Date: 10/20/2014 9:25:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[State](
	[StateCode] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED 
(
	[StateCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[County]    Script Date: 10/20/2014 9:25:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[County](
	[CountyId] [int] NOT NULL IDENTITY(1,1),
	[Name] [nvarchar](100) NOT NULL,
	[StateCode] [nvarchar](2) NULL,
 CONSTRAINT [PK_County] PRIMARY KEY CLUSTERED 
(
	[CountyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EventType]    Script Date: 10/20/2014 9:25:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventType](
	[EventTypeCode] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
 CONSTRAINT [PK_EventType] PRIMARY KEY CLUSTERED 
(
	[EventTypeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[EventTypePatronType]    Script Date: 10/20/2014 9:25:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventTypePatronType](
	[EventTypePatronTypeId] int NOT NULL,
	[EventTypeCode] [nvarchar](20) NOT NULL,
	[PatronTypeCode] [nvarchar](20) NOT NULL,
	[DisplayOrder] [int] NOT NULL,
 CONSTRAINT [PK_EventTypePatronType] PRIMARY KEY CLUSTERED 
(
	[EventTypePatronTypeId]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[PatronTypeGradeType]    Script Date: 10/20/2014 9:25:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PatronTypeGradeType](
	[PatronTypeGradeTypeId] int NOT NULL,
	[PatronTypeCode] [nvarchar](20) NOT NULL,
	[GradeTypeCode] [nvarchar](20) NOT NULL,
	[DisplayOrder] [int] NOT NULL,
 CONSTRAINT [PK_PatronTypeGradeType] PRIMARY KEY CLUSTERED 
(
	[PatronTypeGradeTypeId]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[PatronTypeAgeType]    Script Date: 10/20/2014 9:25:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PatronTypeAgeType](
	[PatronTypeAgeTypeId] int NOT NULL,
	[PatronTypeCode] [nvarchar](20) NOT NULL,
	[AgeTypeCode] [nvarchar](20) NOT NULL,
	[DisplayOrder] [int] NOT NULL,
 CONSTRAINT [PK_PatronTypeAgeType] PRIMARY KEY CLUSTERED 
(
	[PatronTypeAgeTypeId]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Event]    Script Date: 10/20/2014 9:25:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event](
	[EventId] [int] NOT NULL IDENTITY(1,1),
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NOT NULL,
	[EventTypeCode] [nvarchar](20) NOT NULL,
	[EventDate] [datetime] NULL,
	[RegistrationStartDate] [datetime] NULL,
	[RegistrationEndDate] [datetime] NULL,
	[VenueName] [nvarchar](100) NULL,
	[VenueAddress] [nvarchar](500) NULL,
	[EarlyBirdOn] [bit] NOT NULL,
	MainMessage varchar(250) NULL,
	HomePageURL varchar(250) NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/****** Object:  Table [dbo].[Registration]    Script Date: 10/20/2014 9:25:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Registration](
	[RegistrationId] [int] NOT NULL IDENTITY(1,1),
	[InvoiceNumber] [uniqueidentifier] NOT NULL,
	[EventId] [int] NOT NULL,
	[Amount] [money] NOT NULL,
	[MastiPaymentId] [int] NULL,
	[PmtVendorOrderId] [nvarchar](100) NULL,
	[PmtVendorCode] [nvarchar](20) NULL,
	[PaymentInitiationDate] [datetime] NOT NULL,
	[PaymentCompleteDate] [datetime] NULL,
	[RegisteredDate] [datetime] NOT NULL,
	[PaymentCancelledDate] [datetime] NULL,
	[StatusCode] [nvarchar](20) NOT NULL,
	[RefundedDate] [datetime] NULL,
	[AttendingSpellingBootcamp] [bit] NOT NULL,
	[ReferredBy] [nvarchar] (200) NULL,
 CONSTRAINT [PK_Registration] PRIMARY KEY CLUSTERED 
(
	[RegistrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RegistrationPrice]    Script Date: 10/20/2014 9:25:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RegistrationPrice](
	[RegistrationPriceId] [int] NOT NULL IDENTITY(1,1),
	[EventId] [int] NOT NULL,
	[PatronTypeCode] [nvarchar](20) NOT NULL,
	[DiscountedAmount] [money] NOT NULL,
	[Amount] [money] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
 CONSTRAINT [PK_RegistrationPrice] PRIMARY KEY CLUSTERED 
(
	[RegistrationPriceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[School]    Script Date: 10/20/2014 9:25:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[School](
	[SchoolId] [int] NOT NULL IDENTITY(1,1),
	[Name] [nvarchar](100) NOT NULL,
	[CountyId] [int] NOT NULL,
 CONSTRAINT [PK_School] PRIMARY KEY CLUSTERED 
(
	[SchoolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


/****** Object:  Table [dbo].[Patron]    Script Date: 10/20/2014 9:25:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patron](
	[PatronId] [int] NOT NULL IDENTITY(1,1),
	[FirstName] [nvarchar](100) NOT NULL,
	[MiddleName] [nvarchar](100),
	[LastName] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](100) NULL,
	[PatronTypeCode] [nvarchar](20) NOT NULL,
	[AgeTypeCode] [nvarchar](20) NOT NULL,
	[SchoolId] [int],
	[GradeTypeCode] [nvarchar](20),
	[OtherSchool] [nvarchar](100),
	[Amount] [money] NOT NULL,
	[Registrationid] [int] NOT NULL,
 CONSTRAINT [PK_Patron] PRIMARY KEY CLUSTERED 
(
	[PatronId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Logging](
	[LoggingId] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[Date] [datetime] NULL,
	[Application] [varchar](200) NULL,
	[Level] [varchar](100) NULL,
	[Logger] [varchar](8000) NULL,
	[Message] [varchar](8000) NULL,
	[MachineName] [varchar](8000) NULL,
	[UserName] [varchar](8000) NULL,
	[CallSite] [varchar](8000) NULL,
	[Thread] [varchar](100) NULL,
	[Exception] [varchar](8000) NULL,
	[Stacktrace] [varchar](8000) NULL,
 CONSTRAINT [PK_logging] PRIMARY KEY CLUSTERED 
(
	[LoggingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[County]  WITH CHECK ADD  CONSTRAINT [FK_County_State] FOREIGN KEY([StateCode])
REFERENCES [dbo].[State] ([StateCode])
GO
ALTER TABLE [dbo].[County] CHECK CONSTRAINT [FK_County_State]
GO

GO
ALTER TABLE [dbo].[Event]  WITH CHECK ADD  CONSTRAINT [FK_Event_EventType] FOREIGN KEY([EventTypeCode])
REFERENCES [dbo].[EventType] ([EventTypeCode])
GO
ALTER TABLE [dbo].[Event] CHECK CONSTRAINT [FK_Event_EventType]
GO
ALTER TABLE [dbo].[Registration]  WITH CHECK ADD  CONSTRAINT [FK_Registration_Event] FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([EventId])
GO
ALTER TABLE [dbo].[Registration] CHECK CONSTRAINT [FK_Registration_Event]
GO

ALTER TABLE [dbo].[Registration]  WITH CHECK ADD  CONSTRAINT [FK_Registration_Status] FOREIGN KEY([StatusCode])
REFERENCES [dbo].[ReferenceCode] ([Code])
GO
ALTER TABLE [dbo].[Registration] CHECK CONSTRAINT [FK_Registration_Status]
GO

ALTER TABLE [dbo].[RegistrationPrice]  WITH CHECK ADD  CONSTRAINT [FK_RegistrationPrice_Event] FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([EventId])
GO
ALTER TABLE [dbo].[RegistrationPrice] CHECK CONSTRAINT [FK_RegistrationPrice_Event]
GO

ALTER TABLE [dbo].[RegistrationPrice]  WITH CHECK ADD  CONSTRAINT [FK_RegistrationPrice_ReferenceCode] FOREIGN KEY([PatronTypeCode])
REFERENCES [dbo].[ReferenceCode] ([Code])
GO
ALTER TABLE [dbo].[RegistrationPrice] CHECK CONSTRAINT [FK_RegistrationPrice_ReferenceCode]
GO

ALTER TABLE [dbo].[Patron]  WITH CHECK ADD  CONSTRAINT [FK_Patron_ReferenceCode_PatronType] FOREIGN KEY([PatronTypeCode])
REFERENCES [dbo].[ReferenceCode] ([Code])
GO
ALTER TABLE [dbo].[Patron] CHECK CONSTRAINT [FK_Patron_ReferenceCode_PatronType]
GO

ALTER TABLE [dbo].[Patron]  WITH CHECK ADD  CONSTRAINT [FK_Patron_ReferenceCode_AgeType] FOREIGN KEY([AgeTypeCode])
REFERENCES [dbo].[ReferenceCode] ([Code])
GO
ALTER TABLE [dbo].[Patron] CHECK CONSTRAINT [FK_Patron_ReferenceCode_AgeType]
GO

ALTER TABLE [dbo].[Patron]  WITH CHECK ADD  CONSTRAINT [FK_Patron_School] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[School] ([SchoolId])
GO
ALTER TABLE [dbo].[Patron] CHECK CONSTRAINT [FK_Patron_School]
GO

ALTER TABLE [dbo].[Patron]  WITH CHECK ADD  CONSTRAINT [FK_Patron_ReferenceCode_GradeType] FOREIGN KEY([GradeTypeCode])
REFERENCES [dbo].[ReferenceCode] ([Code])
GO
ALTER TABLE [dbo].[Patron] CHECK CONSTRAINT [FK_Patron_ReferenceCode_GradeType]
GO

ALTER TABLE [dbo].[Patron]  WITH CHECK ADD  CONSTRAINT [FK_Patron_Registration] FOREIGN KEY([RegistrationId])
REFERENCES [dbo].[Registration] ([RegistrationId])
GO
ALTER TABLE [dbo].[Patron] CHECK CONSTRAINT [FK_Patron_Registration]
GO

ALTER TABLE [dbo].[School]  WITH CHECK ADD  CONSTRAINT [FK_School_County] FOREIGN KEY([CountyId])
REFERENCES [dbo].[County] ([CountyId])
GO
ALTER TABLE [dbo].[School] CHECK CONSTRAINT [FK_School_County]
GO


ALTER TABLE [dbo].[Logging] ADD  CONSTRAINT [DF_Logging_LoggingId]  DEFAULT (newid()) FOR [LoggingId]
GO

ALTER TABLE [dbo].[Logging] ADD  CONSTRAINT [DF_Logging_date]  DEFAULT (getdate()) FOR [Date]
GO