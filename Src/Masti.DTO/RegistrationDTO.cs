using System;
using System.Collections.Generic;

namespace Masti.DTO
{
    public class RegistrationDTO
    {
        public Guid InvoiceNumber { get; set; }

        public decimal Amount { get; set; }

        public string PmtVendorOrderId { get; set; }

        public bool AttendingSpellingBootcamp { get; set; }

        public string ReferredBy { get; set; }

        public string EventTypeCode { get; set; }

        public string EventName { get; set; }

        public int EventId { get; set; }

        public IList<PatronDTO> Patrons { get; set; }

        public int MastiPaymentId { get; set; }
    }
}
