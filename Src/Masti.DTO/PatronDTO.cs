namespace Masti.DTO
{
    public class PatronDTO
    {
        public int PatronId { get; set; }

        public string PatronTypeCode { get; set; }

        public string PatronTypeDescription { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }
        
        public string LastName { get; set; }
        
        public string Email { get; set; }
        
        public decimal Amount { get; set; }
        
        public string AgeTypeCode { get; set; }

        public string AgeTypeDescription { get; set; }

        public string GradeTypeCode { get; set; }

        public string GradeTypeDescription { get; set; }

        public string StateCode { get; set; }

        public int? CountyId { get; set; }

        public string CountyName { get; set; }

        public int? SchoolId { get; set; }

        public string SchoolName { get; set; }

        public bool HasOtherSchool { get; set; }

        public string OtherSchool { get; set; }

        public string FullName()
        {
            return string.IsNullOrEmpty(MiddleName)
                ? string.Format("{0} {1}", FirstName, LastName)
                : string.Format("{0} {1} {2}", FirstName, MiddleName, LastName);
        }
    }
}